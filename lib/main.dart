import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart'
    as fl;
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'constants/appNames.dart';
import 'helper/dataConnectionChecker.dart';

import 'helper/routes.dart';
import 'helper/utils.dart';

import 'model/message.dart';
import 'page/inApp/commandes/orderDetails.dart';
import 'page/inApp/homePage.dart';
import 'page/inApp/messages/orderConversation.dart';
import 'page/inApp/serviceClient/serviceClient.dart';
import 'page/inApp/notifications/actualites.dart';

import 'services/localDatabase.dart';
import 'services/notificationService.dart';
import 'services/pusher_service.dart';

import 'state/appState.dart';
import 'state/articleState.dart';
import 'state/authState.dart';
import 'state/insertFromPanierState.dart';
import 'state/messageState.dart';
import 'state/newOrderState.dart';
import 'state/notificationState.dart';
import 'state/orderState.dart';
import 'state/panierState.dart';
import 'state/transactionState.dart';

const String portName = "ConnectingIsolate";
const String portName1 = "intentIsolate";

PusherService pusherService = PusherService();

final navKey = GlobalKey<NavigatorState>();

final BehaviorSubject<String> selectNotificationSubject =
    BehaviorSubject<String>();

///THIS FUNCTION CALLS THE API IN BACKGROUND TO RETRIEVE PUSH NOTIFICATIONS
streamMessages() {
  DataConnectionChecker().hasConnection.then(
    (isConnected) {
      tellPusherLastConnection();
    },
  );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ///I FIXED THE ORRIENTATION TO AVOID DEALING WITH UNRESPONSIVE BEHAVIOURS
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  ///I USE THE LOCAL NOTIFICATIONS PLUGIN CAUSE
  ///THE COMPANY USES PUSHER AND THE PLUGINS AVAILABLE FOR PUSHER BEAMS ARE NOT THAT GOOD
  const fl.AndroidInitializationSettings initializationSettingsAndroid =
      fl.AndroidInitializationSettings('app_icon');

  final fl.InitializationSettings initializationSettings =
      fl.InitializationSettings(
    android: initializationSettingsAndroid,
  );
  final fl.NotificationAppLaunchDetails notificationAppLaunchDetails =
      await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  await flutterLocalNotificationsPlugin.initialize(
    initializationSettings,
    onSelectNotification: (String payload) async {
      if (payload != null) {
        /* debugPrint('notification payload: $payload'); */
      }
      selectNotificationSubject.add(payload);
    },
  );
  if (notificationAppLaunchDetails.didNotificationLaunchApp) {
    if (notificationAppLaunchDetails.payload != null) {
      /* debugPrint(
          'notification payload: ${notificationAppLaunchDetails.payload}'); */
    }
    selectNotificationSubject.add(notificationAppLaunchDetails.payload);
  }

  ///I USE SQFLITE PLUGIN FOR THE CART FEATURES
  await LocalDatabaseManager.initialize();

  ///FIREBASE IS ONLY USED FOR PHONE VERIFICATION
  try {
    await Firebase.initializeApp();
  } catch (e) {
    //print("Exception lors de l'initialisation de Firebase: $e");
  }

  ///TO MAKE THE PUSH NOTIFICATIONS IN THE BACKGROUND
  final int helloAlarmID = 0;
  await AndroidAlarmManager.initialize();
  await AndroidAlarmManager.periodic(
    const Duration(minutes: 5),
    helloAlarmID,
    streamMessages,
    rescheduleOnReboot: true,
    exact: true,
  );

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(false);
  }

  runApp(MyApp());

  ///WHEN THE APP IS IN THE BACKGROUND
  ReceivePort rcPort = new ReceivePort();
  IsolateNameServer.registerPortWithName(rcPort.sendPort, portName);
  rcPort.listen((c) {
    MessageState mState;
    if (navKey.currentContext != null)
      mState = Provider.of<MessageState>(navKey.currentContext, listen: false);
    if (c["event-source"] == AppNames.eventNewChat) {
      if (mState != null &&
          ("${c["commande_id"] as String}" == mState.commandeWhichFetchingId)) {
        mState.messagesAdd(
          Message(
            id: null,
            date: mState.formatDate("${c["date"]}"),
            contenu: "${c["contenu"]}",
            isAdmin: "${c["is_admin"]}" == "true" ? '1' : '0',
            commandeId: "${c["commande_id"]}",
            createdAt: null,
            updatedAt: null,
            isReadByAdmin: "1",
            type: null,
            image: "${c["image"]}",
          ),
        );
      }
    }
    if (c["event-source"] == AppNames.eventNewChatClient) {
      if (mState != null)
        mState.messagesAddSAV(
          Message(
            id: null,
            date: mState.formatDate("${c["date"]}"),
            contenu: "${c["contenu"]}",
            isAdmin: "${c["is_admin"]}" == "true" ? '1' : '0',
            createdAt: null,
            updatedAt: null,
            isReadByAdmin: "1",
            type: null,
            image: "${c["image"]}",
          ),
        );
    }
  });
  tellPusherLastConnection();

  ///THE APP IS MADE TO RECEIVE SHARING INTENTS FROM APPS AS ALIEXPRESS, AMAZON,...
  ///
  StreamSubscription _intentDataStreamSubscription;
  // For sharing or opening urls/text coming from outside the app while the app is in the memory
  _intentDataStreamSubscription =
      ReceiveSharingIntent.getTextStream().listen((String value) {
    if (value != null) {
      SharedPreferences.getInstance().then((yuPrefs) {
        if (yuPrefs.containsKey("isLoggedIn")) {
          yuPrefs.setString("intent", value);
        }
      });
    }
  }, onError: (err) {
    //print("getLinkStream error: $err");
  });

  // For sharing or opening urls/text coming from outside the app while the app is closed
  ReceiveSharingIntent.getInitialText().then((String value) {
    if (value != null) {
      SharedPreferences.getInstance().then((yuPrefs) {
        if (yuPrefs.containsKey("isLoggedIn")) {
          yuPrefs.setString("intent", value);
        }
      });
    }
  });
  _intentDataStreamSubscription.cancel();
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    _configureSelectNotificationSubject();
    super.initState();
  }

  void _configureSelectNotificationSubject() {
    ///USER CAN TAP ON A PUSH NOTIFICATION,
    ///THEN THE APP SHOULD REDIRECT TO A PAGE RELATIVE
    ///TO THE NOTIFICATION'S TYPE.
    ///SO PAYLOADS OF THE NOTIFICATIONS CONTAIN
    ///CODES TELLING WHERE TO REDIRECT
    selectNotificationSubject.stream.listen(
      (String payload) async {
        if (payload != null) {
          Widget hPage;
          //SAV--->Service Client
          //CMD--->Changement d'état d'une commande
          //COLIS---->Nouvelle position d'un colis
          //PRM---->Promotion, Divers
          //MSG--->Message en lien à une commande
          //RCG--->Recharge Portefeuille
          //PAY--->Paiement d'une commande
          //NTG--->Do nothing
          String mode = payload.substring(0, 3);
          if (mode == "SAV") {
            hPage = ServiceClient();
          }
          if (mode == "CMD") {
            if (payload.substring(
                  3,
                  payload.indexOf('/'),
                ) ==
                "null") {
              hPage = Actualites();
            } else {
              hPage = OrderDetails(
                id: payload.substring(
                  3,
                  payload.indexOf('/'),
                ),
              );
            }
          }
          if (mode == "COL") {
            hPage = OrderDetails(
              id: payload.substring(
                3,
                payload.indexOf('/'),
              ),
              tabPosition: 3,
            );
          }
          if (mode == "MSG") {
            hPage = OrderConversation(
              id: payload.substring(3, payload.indexOf('/')),
              reference: payload.substring(payload.indexOf('/') + 1),
            );
          }
          if (mode == "RCG") {
            //hPage = Compte();
            if (navKey.currentState != null) {
              while (navKey.currentState.canPop()) {
                navKey.currentState.pop();
              }
              navKey.currentState.pushReplacement(
                MaterialPageRoute(
                  builder: (context) => HomePage(
                    index: 3,
                  ),
                ),
              );
            }
          }
          if (mode == "PAY") {
            hPage = OrderDetails(
              id: payload.substring(3, payload.indexOf('/')),
            );
          }
          if (mode == "PRM") {
            hPage = Actualites(
              type: payload.substring(3) == "PROMO" ? 1 : 2,
            );
          }
          if (mode == "NTG") {
            hPage = null;
          }
          if (mode == "NTF") {
            hPage = Actualites();
          }
          if (navKey.currentState != null) {
            if (hPage == null) {
            } else {
              if (navKey.currentWidget == hPage) {
                navKey.currentState.pushReplacement(
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => hPage),
                );
              } else {
                navKey.currentState.push(
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => hPage),
                );
              }
            }
          }
        }
      },
    );
  }

  @override
  void dispose() {
    selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Color(0xFFf39c12),
        //statusBarIconBrightness: Brightness.light,
        /* systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light, */
      ),
    );
    //precacheImage(AssetImage(AppAssets.splash), context);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppState>(create: (_) => AppState()),
        ChangeNotifierProvider<AuthState>(create: (_) => AuthState()),
        ChangeNotifierProvider<NewOrderState>(create: (_) => NewOrderState()),
        ChangeNotifierProvider<OrderState>(create: (_) => OrderState()),
        ChangeNotifierProvider<PanierState>(create: (_) => PanierState()),
        ChangeNotifierProvider<InsertFromPanierState>(
            create: (_) => InsertFromPanierState()),
        ChangeNotifierProvider<MessageState>(create: (_) => MessageState()),
        ChangeNotifierProvider<NotificationState>(
          create: (_) => NotificationState(),
        ),
        ChangeNotifierProvider<TransactionState>(
          create: (_) => TransactionState(),
        ),
        ChangeNotifierProvider<ArticleState>(
          create: (_) => ArticleState(),
        ),
      ],
      child: MaterialApp(
        navigatorKey: navKey,
        locale: Locale('fr', 'FR'),
        title: 'TarzanExpress',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.white,
          appBarTheme: AppBarTheme(
            brightness: Brightness.dark,
          ),
          //scaffoldBackgroundColor: Colors.white,
          fontFamily: 'Poppins',
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        routes: Routes.route(),
        onGenerateRoute: (settings) => Routes.onGenerateRoute(settings),
        onUnknownRoute: (settings) => Routes.onUnknownRoute(settings),
      ),
    );
  }
}
