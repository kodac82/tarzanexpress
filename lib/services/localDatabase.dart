import 'package:path/path.dart' as p;
import 'package:sqflite/sqflite.dart';
import '../constants/appNames.dart';
import '../model/adresse.dart';
import '../model/product.dart';

class LocalDatabaseManager {
  static Database database;

  static Future<void> initialize() async {
    String databasePath = await getDatabasesPath();
    database = await openDatabase(
      p.join(databasePath, AppNames.LOCAL_DATABASE),
      onCreate: (db, version) async {
        await db.execute(
          "CREATE TABLE panier(id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, lien TEXT, description TEXT, quantite TEXT, image TEXT)",
        );
        await db.execute(
          "CREATE TABLE adresse(id INTEGER PRIMARY KEY AUTOINCREMENT, pays TEXT, ville TEXT, quartier TEXT, adresse1 TEXT, adresse2 TEXT, idFromServer TEXT)",
        );
      },
      version: 1,
    );
  }

  static Future<void> insertProduct(Product product) async {
    final Database db = database;
    await db.insert(
      'panier',
      product.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<Product>> getPanier() async {
    final Database db = database;
    final List<Map<String, dynamic>> maps = await db.query('panier');
    return maps.isEmpty
        ? []
        : List.generate(
            maps.length,
            (i) {
              return Product(
                id: maps[i]['id'],
                nom: maps[i]['nom'],
                lien: maps[i]['lien'],
                description: maps[i]['description'],
                quantite: int.parse(maps[i]['quantite']),
                image: maps[i]['image'].toString(),
              );
            },
          );
  }

  static Future<void> updateProduct(Product product) async {
    await database.update(
      'panier',
      product.toMap(),
      where: "id = ?",
      whereArgs: [product.id],
    );
  }

  static Future<void> deleteProduct(int id) async {
    await database.delete(
      'panier',
      where: "id = ?",
      whereArgs: [id],
    );
  }

  static Future<void> insertAdresse(Adresse adresse) async {
    final Database db = database;
    await db.insert(
      'adresse',
      adresse.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<Adresse>> getAdresse() async {
    final List<Map<String, dynamic>> maps = await database.query('adresse');
    return maps.isEmpty
        ? []
        : List.generate(
            maps.length,
            (i) {
              return Adresse(
                id: int.parse(maps[i]['id'].toString()),
                pays: maps[i]['pays'],
                ville: maps[i]['ville'],
                quartier: maps[i]['quartier'],
                adresse1: maps[i]['adresse1'],
                adresse2: maps[i]['adresse2'],
                idFromServer: maps[i]['idFromServer'],
              );
            },
          );
  }

  static Future<void> updateAdresse(Adresse adresse) async {
    await database.update(
      'adresse',
      adresse.toMap(),
      where: "id = ?",
      whereArgs: [adresse.id],
    );
  }

  static Future<void> deleteAdresse(int id) async {
    await database.delete(
      'adresse',
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
