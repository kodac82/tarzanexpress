import 'dart:async';

import 'package:flutter/services.dart';
import 'package:pusher_websocket_flutter/pusher.dart';

class PusherService {
  Event lastEvent;
  String lastConnectionState;
  String currentConnectionState;
  Channel channel;

  Future<void> initPusher() async {
    try {
      await Pusher.init(
        "c51562f8a93f082bf97c" /* APP_KEY */,
        PusherOptions(
          cluster: "mt1",
          /* PUSHER_CLUSTER */
          activityTimeout: 30000,
        ),
      );
    } on PlatformException catch (/* e */ _) {
      //print(e.message);
    }
  }

  void connectPusher() {
    Pusher.connect(
        onConnectionStateChange: (ConnectionStateChange connectionState) async {
      lastConnectionState = connectionState.previousState;
      currentConnectionState = connectionState.currentState;
      //print("PUSHER PREVIOUS STATE == ${connectionState.previousState}");
      //print("PUSHER CURRENT STATE == ${connectionState.currentState}");
    }, onError: (ConnectionError e) {
      //print("Error: ${e.toJson().toString()}");
    });
  }

  Future<void> subscribePusher(String channelName) async {
    channel = await Pusher.subscribe(channelName);
  }

  void unSubscribePusher(String channelName) {
    Pusher.unsubscribe(channelName);
  }

  StreamController<String> _eventData = StreamController<String>();
  Sink get _inEventData => _eventData.sink;
  Stream get eventStream => _eventData.stream;

  void bindEvent(String eventName) {
    channel.bind(eventName, (last) {
      final String data = last.data;
      _inEventData.add(data);
    });
  }

  void unbindEvent(String eventName) {
    channel.unbind(eventName);
    _eventData.close();
  }

  Future<void> firePusher(
    String channelName,
    List<String> eventNames,
  ) async {
    await initPusher();
    connectPusher();
    await subscribePusher(channelName);
    for (var i = 0; i < eventNames.length; i++) {
      bindEvent(eventNames[i]);
    }
  }
}
