import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../helper/utils.dart';
import '../services/notificationService.dart';

import '../constants/appNames.dart';
import '../main.dart';
import '../model/message.dart';
import '../services/pusher_service.dart';

class MessageState extends ChangeNotifier {
  PusherService pusherService = PusherService();

  bool _isSendingSAV = false;

  bool get isSendingSAV => _isSendingSAV;

  set isSendingSAV(bool value) {
    _isSendingSAV = value;
    notifyListeners();
  }

  List<Map<String, dynamic>> _receivedSAV;

  List<Map<String, dynamic>> get receivedSAV => _receivedSAV;

  set receivedSAV(List<Map<String, dynamic>> value) {
    _receivedSAV = value;
    notifyListeners();
  }

  List<Message> _messagesSAV;

  List<Message> get messagesSAV => _messagesSAV;

  set messagesSAV(List<Message> value) {
    _messagesSAV = value;
    notifyListeners();
  }

  messagesAddSAV(Message message) {
    if (_messagesSAV == null) {
      _messagesSAV = [];
    }
    _messagesSAV.add(message);
    notifyListeners();
  }

  messagesAddAllSAV(List list) {
    _messagesSAV.addAll([...list]);
    notifyListeners();
  }

  clearSAV() {
    isSendingSAV = false;
    messagesSAV = null;
    receivedSAV = null;
  }

  Future<bool> getMessagesSAV({
    @required BuildContext context,
  }) async {
    try {
      ///CREATING REQUEST VARIABLE
      //print("CREATING REQUEST VARIABLE");
      var uri = Uri.parse(AppNames.hostUrl + AppNames.serviceClientGet);
      var request = new http.MultipartRequest("GET", uri);

      ///ADDING HEADERS
      //print("ADDING HEADERS");
      final prefs = await SharedPreferences.getInstance();
      await getNewToken(context: context);
      final String token = prefs.getString("accessToken");
      Map<String, String> headers = {
        'Authorization': "Bearer $token",
        "Content-Type": "application/json",
        "Accept": "application/json"
      };
      request.headers.addAll(headers);

      ///SENDING THE REQUEST
      //print("SENDING THE REQUEST");
      var response = await request.send();

      ///READING THE RESPONSE
      //print("READING THE RESPONSE");
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      //print("REPONSE EST : ${response.statusCode}");
      //print("BODY IS : $responseString");
      var a = jsonDecode(responseString);
      if (response.statusCode.toString().startsWith("20")) {
        List<Map<String, dynamic>> c = [
          for (var i = 0; i < a.length; i++) new Map<String, dynamic>.from(a[i])
        ];
        _messagesSAV = [];
        _messagesSAV.clear();
        messagesAddAllSAV([
          for (var i = 0; i < c.length; i++)
            Message(
              id: "${c[i]["id"]}",
              date: "${c[i]["date"]}",
              contenu: "${c[i]["contenu"]}",
              isAdmin: "${c[i]["is_admin"]}",
              commandeId: "${c[i]["commande_id"]}",
              createdAt: "${c[i]["created_at"]}",
              updatedAt: "${c[i]["updated_at"]}",
              isReadByAdmin: "${c[i]["is_read_by_admin"]}",
              type: "${c[i]["type"]}",
              image: c[i]["image"] as String,
            ),
        ]);
        return true;
      }
    } catch (e) {
      //print("Exception lors du getMessagesServiceClient(): $e");
    }
    return false;
  }

  Future<bool> postMessageSAV({
    @required BuildContext context,
    @required String contenu,
    @required File image,
  }) async {
    var response;
    isSendingSAV = true;
    try {
      final prefs = await SharedPreferences.getInstance();
      await getNewToken(context: context);
      final String token = prefs.getString("accessToken");
      final String userId = prefs.getInt("id").toString();
      var uri1 =
          Uri.parse(AppNames.hostUrl + AppNames.serviceClientPost + userId);
      var uri2 =
          Uri.parse(AppNames.hostUrl + AppNames.serviceClientPostImg + userId);

      if (contenu != null) {
        response = await http.post(
          uri1,
          headers: {
            'Authorization': "Bearer $token",
            "Accept": "application/json"
          },
          body: {"contenu": contenu},
        );
        //print('Response status: ${response.statusCode}');
        //print('Response body: ${response.body}');
        if (response.statusCode.toString().startsWith("20")) {
          isSendingSAV = false;
          Map a = Map.from(Map.from(jsonDecode(response.body))['data']);
          messagesAddSAV(
            Message(
              id: null,
              date: "${a["date"]}",
              contenu: "${a["contenu"]}",
              isAdmin: '0',
              commandeId: null,
              createdAt: null,
              updatedAt: null,
              isReadByAdmin: "0",
              type: null,
            ),
          );
          return true;
        }
      } else {
        ///contenu being null means image is not null
        var request = new http.MultipartRequest("POST", uri2);
        request.files
            .add(await http.MultipartFile.fromPath("cmd_pictures", image.path));
        Map<String, String> headers = {
          'Authorization': "Bearer $token",
        };
        request.headers.addAll(headers);
        var response = await request.send();
        var responseData = await response.stream.toBytes();
        var responseString = String.fromCharCodes(responseData);
        //print("REPONSE EST : ${response.statusCode}");
        //print("BODY IS : $responseString");
        if (response.statusCode.toString().startsWith('20')) {
          isSendingSAV = false;
          Map a = Map.from((Map.from(jsonDecode(responseString)))['data']);
          messagesAddSAV(
            Message(
              id: null,
              date: "${a["date"]}",
              image: "${a["image"]}",
              isAdmin: '0',
              commandeId: null,
              createdAt: null,
              updatedAt: null,
              isReadByAdmin: "0",
              type: null,
            ),
          );
          return true;
        }
      }
    } catch (e) {
      //print("Exception lors du postMessageServiceClient(): $e");
    }
    isSendingSAV = false;
    return false;
  }

  String formatDateSAV(String date) {
    String secondPart = date.substring(date.indexOf(' '));
    int dayIndex = date.indexOf('/');
    String day = date.substring(0, dayIndex);
    if (day.length < 2) day = '0' + day;
    date = date.substring(dayIndex + 1);
    int monthIndex = date.indexOf('/');
    String month = date.substring(0, monthIndex);
    if (month.length < 2) month = '0' + month;
    date = date.substring(monthIndex + 1);
    int yearIndex = date.indexOf(' ');
    String year = date.substring(0, yearIndex);
    if (year.length < 2) year = '0' + year;
    return year + '-' + month + '-' + day + secondPart;
  }

  Map<String, int> _unreadMap = {};

  Map<String, int> get unreadMap => _unreadMap;

  set unreadMap(Map<String, int> value) {
    _unreadMap = value;
    notifyListeners();
  }

  bool _isSending = false;

  bool get isSending => _isSending;

  set isSending(bool value) {
    _isSending = value;
    notifyListeners();
  }

  String _commandeWhichFetchingId;

  String get commandeWhichFetchingId => _commandeWhichFetchingId;

  set commandeWhichFetchingId(String value) {
    _commandeWhichFetchingId = value;
    notifyListeners();
  }

  List<Map<String, dynamic>> _received;

  List<Map<String, dynamic>> get received => _received;

  set received(List<Map<String, dynamic>> value) {
    _received = value;
    notifyListeners();
  }

  List<Message> _messages;

  List<Message> get messages => _messages;

  set messages(List<Message> value) {
    _messages = value;
    notifyListeners();
  }

  messagesAdd(Message message) {
    if (_messages == null) {
      _messages = [];
    }
    _messages.add(message);
    notifyListeners();
  }

  messagesAddAll(List list) {
    _messages.addAll([...list]);
    notifyListeners();
  }

  clear() {
    isSending = false;
    messages = null;
    commandeWhichFetchingId = null;
    received = null;
  }

  checkPusherAlive({@required BuildContext context}) async {
    if (pusherService.currentConnectionState == 'DISCONNECTED') {
      await pusherService.firePusher(
        'tarzan-express',
        [
          'chat',
          'push',
          'chat_client',
          'portefeuille_recharge',
          'confirmation_paiement',
          'position_colis'
        ],
      );
    }
    //await Future.delayed(Duration(seconds: 1 /*, milliseconds: 500*/));
  }

  Future<bool> getMessages({
    @required BuildContext context,
    @required String id,
  }) async {
    _commandeWhichFetchingId = id;
    checkPusherAlive(context: context);
    try {
      ///CREATING REQUEST VARIABLE
      //print("CREATING REQUEST VARIABLE");
      var uri = Uri.parse(AppNames.hostUrl + AppNames.getMessages + id);
      var request = new http.MultipartRequest("GET", uri);

      ///ADDING HEADERS
      //print("ADDING HEADERS");
      final prefs = await SharedPreferences.getInstance();
      await getNewToken(context: context);
      final String token = prefs.getString("accessToken");
      Map<String, String> headers = {
        'Authorization': "Bearer $token",
        "Content-Type": "application/json",
        "Accept": "application/json"
      };
      request.headers.addAll(headers);

      ///SENDING THE REQUEST
      //print("SENDING THE REQUEST");
      var response = await request.send();

      ///READING THE RESPONSE
      //print("READING THE RESPONSE");
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      //print("REPONSE EST : ${response.statusCode}");
      //print("BODY IS : $responseString");
      var a = jsonDecode(responseString);
      List<Map<String, dynamic>> c = [
        for (var i = 0; i < a.length; i++) new Map<String, dynamic>.from(a[i])
      ];
      await updateReadStatus(orderId: id, token: token);
      _messages = [];
      _messages.clear();
      messagesAddAll([
        for (var i = 0; i < c.length; i++)
          Message(
            id: "${c[i]["id"]}",
            date: "${c[i]["date"]}",
            contenu: "${c[i]["contenu"]}",
            isAdmin: "${c[i]["is_admin"]}",
            commandeId: "${c[i]["commande_id"]}",
            createdAt: "${c[i]["created_at"]}",
            updatedAt: "${c[i]["updated_at"]}",
            isReadByAdmin: "${c[i]["is_read_by_admin"]}",
            type: "${c[i]["type"]}",
            image: c[i]["image"] as String,
          ),
      ]);

      return true;
    } catch (e) {
      //print("Exception lors du getMessages(): $e");
      _commandeWhichFetchingId = null;
    }
    isSending = false;
    return false;
  }

  updateReadStatus({
    @required String token,
    @required String orderId,
  }) async {
    try {
      var uri = Uri.parse(AppNames.hostUrl +
          "api/messages/commandes/$orderId/update_messages_states");
      /* print("UPDATING " +
          "api/messages/commandes/$orderId/update_messages_states"); */
      /* var response =  */ await http.put(
        uri,
        headers: {
          'Authorization': "Bearer $token",
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: json.encode({}),
      );
      //print('Response status: ${response.statusCode}');
      //print('Response body: ${response.body}');
      /*if (response.statusCode.toString().startsWith('20')) {
        await prefs.setString('password', newPassword);
        return true;
      }*/
    } catch (e) {
      //print("Exception lors du updateReadStatus(): $e");
    }
    return false;
  }

  Future<bool> postMessage({
    @required BuildContext context,
    @required String id,
    @required String contenu,
    @required File image,
  }) async {
    var response;
    isSending = true;
    checkPusherAlive(context: context);
    try {
      final prefs = await SharedPreferences.getInstance();
      await getNewToken(context: context);
      final String token = prefs.getString("accessToken");
      var uri1 = Uri.parse(AppNames.hostUrl + AppNames.postMessage + id);
      var uri2 = Uri.parse(AppNames.hostUrl + AppNames.postMessageImage + id);
      if (contenu != null) {
        response = await http.post(
          uri1,
          headers: {
            'Authorization': "Bearer $token",
            "Accept": "application/json"
          },
          body: {
            "contenu": contenu,
          },
        );
        //print('Response status: ${response.statusCode}');
        //print('Response body: ${response.body}');
        if (response.statusCode.toString().startsWith("20")) {
          isSending = false;
          Map a = Map.from((Map.from(jsonDecode(response.body)))['data']);
          messagesAdd(
            Message(
              id: null,
              date: "${a["date"]}",
              contenu: "${a["contenu"]}",
              isAdmin: '0',
              commandeId: "${a["commande_id"]}",
              createdAt: null,
              updatedAt: null,
              isReadByAdmin: "0",
              type: null,
            ),
          );
          return true;
        }
      } else {
        ///contenu being null means image is not null
        var request = new http.MultipartRequest("POST", uri2);
        request.files
            .add(await http.MultipartFile.fromPath("cmd_pictures", image.path));
        Map<String, String> headers = {
          'Authorization': "Bearer $token",
        };
        request.headers.addAll(headers);
        var response = await request.send();
        var responseData = await response.stream.toBytes();
        var responseString = String.fromCharCodes(responseData);
        //print("REPONSE EST : ${response.statusCode}");
        //print("BODY IS : $responseString");
        if (response.statusCode.toString().startsWith('20')) {
          isSending = false;
          Map a = Map.from((Map.from(jsonDecode(responseString)))['data']);
          messagesAdd(
            Message(
              id: null,
              date: "${a["date"]}",
              image: "${a["image"]}",
              isAdmin: '0',
              commandeId: "${a["commande_id"]}",
              createdAt: null,
              updatedAt: null,
              isReadByAdmin: "0",
              type: null,
            ),
          );
          return true;
        }
      }
    } catch (e) {
      //print("Exception lors du postMessage(): $e");
    }
    isSending = false;
    return false;
  }

  Future<List> unreadMessages({
    @required BuildContext context,
  }) async {
    checkPusherAlive(context: context);
    try {
      final prefs = await SharedPreferences.getInstance();
      await getNewToken(context: context);
      final String token = prefs.getString("accessToken");
      var uri = Uri.parse(AppNames.hostUrl + AppNames.unreadMessages);
      var response = await http.get(
        uri,
        headers: {
          'Authorization': "Bearer $token",
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
      );
      //print('Response status: ${response.statusCode}');
      // print('Response body: ${response.body}');
      if (response.statusCode.toString().startsWith('20')) {
        List gList = jsonDecode(response.body);
        final int gLength = gList.length;
        unreadMap.clear();
        for (int i = 0; i < gList.length; i++) {
          Map xMap = Map.from(gList[i]);
          unreadMap[xMap["commande_id"].toString()] = xMap["messages_non_lus"];
        }
        return [true, gLength];
      }
    } catch (e) {
      //print("Exception lors du unreadMessages(): $e");
    }
    return [false];
  }

  String formatDate(
    String date,
  ) {
    String secondPart = date.substring(date.indexOf(' '));
    int dayIndex = date.indexOf('/');
    String day = date.substring(0, dayIndex);
    if (day.length < 2) day = '0' + day;
    date = date.substring(dayIndex + 1);
    int monthIndex = date.indexOf('/');
    String month = date.substring(0, monthIndex);
    if (month.length < 2) month = '0' + month;
    date = date.substring(monthIndex + 1);
    int yearIndex = date.indexOf(' ');
    String year = date.substring(0, yearIndex);
    if (year.length < 2) year = '0' + year;
    return year + '-' + month + '-' + day + secondPart;
  }

  streamMessages() {
    if (!["CONNECTING", "CONNECTED"]
        .contains(pusherService.currentConnectionState.toString())) {
      try {
        pusherService.firePusher(
          'tarzan-express',
          [
            'tarzan-push',
          ],
        ).then(
          (_) {
            pusherService.eventStream.listen(
              (event) {
                //print("NEW EVENT !!!!!!!!!!!!!!!!!!!!!"+event.toString());
                Map<String, dynamic> c =
                    Map<String, dynamic>.from(jsonDecode(event));
                MessageState mState;
                if (navKey.currentContext != null)
                  mState = Provider.of<MessageState>(navKey.currentContext,
                      listen: false);
                int userId;
                SharedPreferences.getInstance().then(
                  (value) {
                    try {
                      if ([
                        "PROMO",
                        "ACTUALITE",
                        "INFO",
                        "DIVERS",
                      ].contains(c["event-source"])) {
                        //print("PUSHER RESPONSE NOTIFICATION : $c");
                        showNotification(
                          body: "${c['message']}",
                          /* "Une nouvelle actualité pour vous." */
                          helper: ("PRM" + "${c["event-source"]}"),
                        );
                      } else {
                        value.reload().then(
                          (_) {
                            if (value.containsKey('id')) {
                              userId = value.getInt("id");
                              if ("${c["user_id"]}" == "$userId") {
                                if (c["event-source"] == AppNames.eventNewCmd) {
                                  showNotification(
                                    body: "${c['message']}",
                                    helper: ("CMD" +
                                        "${c["commande_id"]}" +
                                        '/' +
                                        "${c["reference"]}"),
                                  );
                                }
                                if (c["event-source"] ==
                                    AppNames.eventNewChat) {
                                  ///MESSAGE
                                  if ("${c["is_admin"]}" == "true") {
                                    if (!(value.containsKey(
                                            "isInside${c["commande_id"]}") &&
                                        value.getBool(
                                            "isInside${c["commande_id"]}"))) {
                                      showNotification(
                                        body: "${c['message']}",
                                        helper: ("MSG" +
                                            "${c["commande_id"]}" +
                                            '/' +
                                            "${c["reference"]}"),
                                      );
                                    }
                                    if (mState != null &&
                                        ("${c["commande_id"] as String}" ==
                                            mState.commandeWhichFetchingId)) {
                                      mState.messagesAdd(
                                        Message(
                                          id: null,
                                          date:
                                              mState.formatDate("${c["date"]}"),
                                          contenu: "${c["contenu"]}",
                                          isAdmin: '1',
                                          commandeId: "${c["commande_id"]}",
                                          createdAt: null,
                                          updatedAt: null,
                                          isReadByAdmin: "1",
                                          type: null,
                                          image: "${c["image"]}",
                                        ),
                                      );
                                    }
                                  }
                                }
                                if (c["event-source"] ==
                                    AppNames.eventNewChatClient) {
                                  if ("${c["is_admin"]}" == "true") {
                                    if (!value.containsKey('isInsideSAV')) {
                                      showNotification(
                                        body: "${c['message']}",
                                        helper: ("SAV"),
                                      );
                                    }
                                    if (mState != null)
                                      mState.messagesAddSAV(
                                        Message(
                                            id: null,
                                            date: mState
                                                .formatDate("${c["date"]}"),
                                            contenu: "${c["contenu"]}",
                                            isAdmin: '1',
                                            createdAt: null,
                                            updatedAt: null,
                                            isReadByAdmin: "1",
                                            type: null,
                                            image: "${c["image"]}"),
                                      );
                                  }
                                }
                                if (c["event-source"] ==
                                    AppNames.eventRecharge) {
                                  showNotification(
                                    body: "${c['message']}",
                                    helper: ("RCG"),
                                  );
                                }
                                if (c["event-source"] ==
                                    AppNames.eventPaiementCmd) {
                                  showNotification(
                                    body: "${c['message']}",
                                    helper: ("PAY" +
                                        "${c["commande_id"]}" +
                                        '/' +
                                        "${c["reference"]}"),
                                  );
                                }
                                if (c["event-source"] ==
                                    AppNames.eventPositionColis) {
                                  //print("PUSHER RESPONSE NOTIFICATION : $c");
                                  showNotification(
                                    body: "${c['message']}",
                                    helper: ("COL" +
                                        "${c["commande_id"]}" +
                                        '/' +
                                        "${c["commande_reference"]}"),
                                  );
                                }
                                if (c["event-source"] ==
                                    AppNames.eventChangementEtat) {
                                  showNotification(
                                    body: "${c['message']}",
                                    helper: ("CMD" +
                                        "${c["commande_id"]}" +
                                        '/' +
                                        "${c["reference"]}"),
                                  );
                                }
                              }
                            }
                          },
                        );
                      }
                    } catch (e) {
                      //print("Error occured in streamMessages() : $e");
                    }
                  },
                );
              },
            );
          },
        );
      } catch (e) {
        //print("Error occured in streamMessages : $e");
      }
    }
  }
}
