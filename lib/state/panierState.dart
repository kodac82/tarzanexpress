import 'package:flutter/material.dart';
import '../services/localDatabase.dart';
import '../model/product.dart';

class PanierState extends ChangeNotifier {
  int _checked = 0;
  int get checked => _checked;
  set checked(int value) {
    _checked = value;
    notifyListeners();
  }

  List<bool> _selected = [];
  List<bool> get selected => _selected;
  set selected(List<bool> value) {
    _selected = value;
    notifyListeners();
  }

  List<Product> _contentPanier = [];
  List<Product> get contentPanier => _contentPanier;
  set contentPanier(List<Product> value) {
    _contentPanier = value;
    notifyListeners();
  }

  clear() {
    checked = 0;
    selected = [];
    contentPanier = [];
  }

  modifySelectedElement({
    @required int index,
    @required bool value,
  }) {
    _selected[index] = value;
    notifyListeners();
  }

  selectAll() {
    bool val = !selected.every((element) => element == true);
    for (var i = 0; i < selected.length; i++) {
      _selected[i] = val;
    }
    notifyListeners();
  }

  Future<void> contentPanierAdd(Product product) async {
    await LocalDatabaseManager.insertProduct(product);
    await getProducts();
  }

  Future<void> modifyContentPanierElement({
    @required int index,
    @required String nom,
    @required String description,
    @required int quantite,
    @required image,
  }) async {
    await LocalDatabaseManager.updateProduct(
      Product(
        id: contentPanier[index].id,
        nom: nom,
        description: description,
        quantite: quantite,
        image: image,
      ),
    );
    await getProducts();
    /* contentPanier[index] = _contentPanier[index].copyWith(
      description: description,
      quantite: quantite,
    );
    notifyListeners(); */
  }

  Future<void> deleteContentPanierElement({
    @required int id,
  }) async {
    await LocalDatabaseManager.deleteProduct(id);
    for (var i = 0; i < contentPanier.length; i++) {
      if (contentPanier[i].id == id) {
        _contentPanier.removeAt(i);
        i = contentPanier.length + 1;
      }
    }
    for (var i = 0; i < selected.length; i++) {
      if (selected[i] == true) {
        selected.removeAt(i);
        i = selected.length + 1;
      }
    }
    notifyListeners();
  }

  Future<void> getProducts() async {
    contentPanier = await LocalDatabaseManager.getPanier();
    selected = contentPanier.isEmpty
        ? []
        : List.generate(
            contentPanier.length,
            (i) {
              return false;
            },
          );
  }
}
