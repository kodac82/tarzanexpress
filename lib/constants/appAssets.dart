abstract class AppAssets {
  static const intro0 = "assets/images/intro0.png";
  static const intro1 = "assets/images/intro1.png";
  static const intro2 = "assets/images/intro2.png";
  static const aliexpress = "assets/images/aliexpress.png";
  static const cartAdd = "assets/images/cart-add.png";
  static const logo = "assets/images/logo.png";
  static const logoWithoutNameWhite = "assets/images/logoWithoutNameWhite.png";
  static const orderEmpty = "assets/images/order-empty.png";
  static const alibaba = "assets/images/alibaba.png";
  static const amazon = "assets/images/amazon.png";
  static const shein = "assets/images/shein.png";
  static const logoFlooz = "assets/images/logoFlooz.png";
  static const logoTMoney = "assets/images/logoTMoney.png";
  static const mobileMoney = "assets/images/mobileMoney.png";
  static const especes = "assets/images/especes.png";
  static const bankCheck = "assets/images/bankCheck.png";
  static const bankTransfer = "assets/images/bankTransfer.png";
  static const detailsLivraison = "assets/images/detailsLivraison.png";
  //ICON
  static const homeIcon = "assets/icon/Home.png";
  static const messageIcon = "assets/icon/Message.png";
  static const cartIcon = "assets/icon/Buy.png";
  static const profileIcon = "assets/icon/Profile.png";
  static const walletIcon = "assets/icon/Wallet.png";
  static const notifIcon = "assets/icon/Notification.png";
  static const promoIcon = "assets/icon/Discount.png";
  static const diversIcon = "assets/icon/InfoCircle.png";
  //POPULAR
  static const popular1 = "assets/images/popular/1.jpg";
  static const popular2 = "assets/images/popular/2.jpg";
  static const popular3 = "assets/images/popular/3.jpg";
}
