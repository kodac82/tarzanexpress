import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../state/messageState.dart';
import '../../widgets/customLoader.dart';

import '../../page/intro/intro.dart';
import '../../state/appState.dart';
import '../../state/authState.dart';
import '../../state/panierState.dart';
import '../auth/welcomePage.dart';
import '../inApp/homePage.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool isFreshlyInstalled = true;
  bool isLoggedIn = false;
  bool dataLoaded = false;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      timer();
    });
    super.initState();
  }

  void timer() async {
    Future.delayed(Duration(seconds: 2)).then((_) {
      final state = Provider.of<AppState>(context, listen: false);
      Provider.of<PanierState>(context, listen: false).getProducts();
      Provider.of<MessageState>(context, listen: false).streamMessages();
      state.loadPreferences().then((_) async {
        bool value = state.prefs.containsKey("isFreshlyInstalled");
        if (!value) {
          await state.prefs.setBool("isLoggedIn", false);
        } else {
          bool value1 = state.prefs.getBool("isLoggedIn");
          if (!value1) {
            Provider.of<AuthState>(context, listen: false)
                .getCountriesAvailable();
          }
          mounted
              ? setState(() {
                  isLoggedIn = value1;
                })
              : isLoggedIn = value1;
        }
        if (mounted) {
          setState(() {
            ///IF TRUE, IT MEANS IT WAS CREATED BEFORE, SO USER ALREADY GONE TROUGH INTRO
            isFreshlyInstalled = !value;
            dataLoaded = true;
          });
        } else {
          isFreshlyInstalled = !value;
          dataLoaded = true;
        }
      });
      //state.getCurrentUser();
    });
  }

  @override
  Widget build(BuildContext context) {
    return dataLoaded
        ? isFreshlyInstalled
            ? Intro()
            : (isLoggedIn)
                ? HomePage()
                : WelcomePage()
        : /* Scaffold(
            body: SafeArea(
              child: Container(
                height: fullHeight(context),
                width: fullWidth(context),
                child: Center(
                  child: Image.asset(
                    AppAssets.splashModern,
                    /* height: fullHeight(context),
                    width: fullWidth(context), */
                    fit: BoxFit.fill,
                    //color: AppColors.yellowAppbar,
                  ),
                ),
              ),
            ),
          ); */
        CustomScreenLoader();
  }
}
