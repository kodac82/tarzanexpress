import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../widgets/customWidgets.dart';

import '../../constants/appAssets.dart';
import '../../page/common/splash.dart';

class Intro extends StatefulWidget {
  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  final PageController _pageController = PageController(initialPage: 0);

  int currentPage = 0;

  skipFunction() async {
    (await SharedPreferences.getInstance())
        .setBool("isFreshlyInstalled", false);
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => SplashPage(),
      ),
    );
  }

  Widget buildPage(
    Size dSize,
    int index,
  ) {
    return Container(
      height: fullHeight(context),
      width: fullWidth(context),
      padding: EdgeInsets.only(
          top: dSize.height * 0.01,
          bottom: (dSize.height * 0.03) + (dSize.width * 0.14),
          left: dSize.width * 0.03,
          right: dSize.width * 0.03),
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage(introData[index]),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  List introData = [
    AppAssets.intro0,
    AppAssets.intro1,
    AppAssets.intro2,
  ];

  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            PageView(
              physics: ClampingScrollPhysics(),
              controller: _pageController,
              onPageChanged: (int page) {
                setState(() {
                  currentPage = page;
                });
              },
              children: [
                buildPage(dSize, 0),
                buildPage(dSize, 1),
                buildPage(dSize, 2),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: dSize.height * 0.018,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ElevatedButton(
                          onPressed: () => skipFunction(),
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                              Colors.black87.withOpacity(0.2),
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                          ),
                          child: Text(
                            'Passer',
                            style: TextStyle(
                              //color: Colors.black,
                              fontSize: dSize.width * 0.045,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () async {
                            if (currentPage != 2) {
                              _pageController.nextPage(
                                duration: Duration(milliseconds: 500),
                                curve: Curves.ease,
                              );
                            } else {
                              await skipFunction();
                            }
                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                              Colors.green,
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Icon(
                              (currentPage != 2)
                                  ? Icons.arrow_forward_ios
                                  : Icons.check_outlined,
                              color: Colors.white,
                              size: dSize.width * 0.06,
                            ),
                          ),
                        ),
                        /* GestureDetector(
                          onTap: () async {
                            if (currentPage != 2) {
                              _pageController.nextPage(
                                duration: Duration(milliseconds: 500),
                                curve: Curves.ease,
                              );
                            } else {
                              await skipFunction();
                            }
                          },
                          child: Container(
                            height: dSize.width * 0.14,
                            width: dSize.width * 0.14,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.green,
                            ),
                            child: Icon(
                                (currentPage != 2)
                                    ? Icons.arrow_forward_ios
                                    : Icons.check_outlined,
                                color: Colors.white,
                                size: dSize.width * 0.08,),
                          ),
                        ), */
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
