import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../../constants/appColors.dart';
import '../../helper/dataConnectionChecker.dart';
import '../../helper/phoneInput/utils/phone_number.dart';
import '../../helper/phoneInput/utils/selector_config.dart';
import '../../helper/phoneInput/widgets/input_widget.dart';
import '../../state/authState.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  bool showSpinner = false;
  final _formKey = GlobalKey<FormState>();
  final _formKey1 = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  bool codeSent = false;
  String codeValidation;
  String dVerificationId;
  bool triedVerification = false;
  bool validationDone = false;
  String newPassword = "";
  bool showPassword = false;
  bool passwordChanged = false;

  @override
  Widget build(BuildContext context) {
    AuthState authState = Provider.of<AuthState>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text("Récupération"),
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 35, 10, 35),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Visibility(
                    visible: (!codeSent && !passwordChanged),
                    child: Column(
                      children: [
                        Text(
                          "Veuillez entrer le numéro de télephone associé à votre compte",
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(height: 15),
                        InternationalPhoneNumberInput(
                          errorMessage: "Numéro invalide",
                          countries: authState.countriesAvailable ??
                              [
                                'TG',
                                'BJ',
                              ],
                          onInputChanged: (PhoneNumber number) {
                            authState.recuperationPhoneNumber = number;
                          },
                          selectorConfig: SelectorConfig(
                            selectorType: PhoneInputSelectorType.DIALOG,
                            backgroundColor: Colors.black,
                            countryComparator:
                                authState.countriesAvailable == null
                                    ? null
                                    : (a, b) {
                                        if (authState.countriesAvailable
                                                .indexOf(a.alpha2Code) <
                                            authState.countriesAvailable
                                                .indexOf(b.alpha2Code)) {
                                          return -1;
                                        }
                                        return 1;
                                      },
                          ),
                          ignoreBlank: false,
                          hintText: "Insérer le numéro",
                          locale: "fr",
                          autoValidateMode: AutovalidateMode.onUserInteraction,
                          selectorTextStyle: TextStyle(color: Colors.black),
                          onSaved: (String value) {
                            authState.telephoneForRecuperation = value;
                          },
                        ),
                        SizedBox(height: 15),
                        Center(
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.green),
                            ),
                            onPressed: () async {
                              if (await DataConnectionChecker().hasConnection) {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  final String authPhoneNumber = authState
                                      .recuperationPhoneNumber.phoneNumber;
                                  setState(() {
                                    showSpinner = true;
                                  });
                                  List reponse = await authState.checkExistence(
                                      numero: authState.recuperationPhoneNumber
                                          .parseNumber(),
                                      alpha2Code: authState
                                          .recuperationPhoneNumber.isoCode);
                                  setState(() {
                                    showSpinner = false;
                                  });
                                  if (reponse[0]) {
                                    if (reponse[1]) {
                                      bool doSend = false;
                                      await showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (_) => AlertDialog(
                                          title: Text(
                                            "CONFIRMATION",
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                          content: Text(
                                            "Afin de poursuivre, nous allons envoyer un code de validation pour s'assurer que vous êtes le détenteur de ce compte.Ce numéro est correct ?\n$authPhoneNumber",
                                            style: TextStyle(fontSize: 17),
                                          ),
                                          actions: [
                                            TextButton(
                                              child: Text("NON"),
                                              onPressed: () {
                                                doSend = false;
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                            TextButton(
                                              child: Text("OUI"),
                                              onPressed: () {
                                                doSend = true;
                                                Navigator.of(context).pop();
                                              },
                                            )
                                          ],
                                        ),
                                      );
                                      if (doSend) {
                                        setState(() {
                                          showSpinner = true;
                                        });
                                        final bool hasConnection =
                                            await DataConnectionChecker()
                                                .hasConnection;
                                        if (hasConnection) {
                                          auth.setLanguageCode('fr');
                                          try {
                                            await FirebaseAuth.instance
                                                .verifyPhoneNumber(
                                              phoneNumber: authState
                                                  .recuperationPhoneNumber
                                                  .phoneNumber,
                                              verificationCompleted:
                                                  (PhoneAuthCredential
                                                      credential) {
                                                //print("AUTH COMPLETED");
                                              },
                                              verificationFailed:
                                                  (FirebaseAuthException e) {
                                                //print("AUTH FAILED : $e");
                                                setState(
                                                  () {
                                                    showSpinner = false;
                                                  },
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(
                                                  SnackBar(
                                                    content: Text(
                                                      "Nous avons rencontré un problème, veuillez réessayer svp !",
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    behavior: SnackBarBehavior
                                                        .floating,
                                                    margin: EdgeInsets.fromLTRB(
                                                        15, 0, 15, 30),
                                                  ),
                                                );
                                              },
                                              codeSent: (String verificationId,
                                                  int resendToken) {
                                                //print("CODE SENT");
                                                setState(() {
                                                  dVerificationId =
                                                      verificationId;
                                                  codeSent = true;
                                                  showSpinner = false;
                                                });
                                              },
                                              codeAutoRetrievalTimeout:
                                                  (String verificationId) {},
                                            );
                                          } catch (e) {
                                            //print(e.toString());
                                            setState(() {
                                              showSpinner = false;
                                            });
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              SnackBar(
                                                content: Text(
                                                  "Nous avons rencontré un problème, veuillez réessayer svp !",
                                                  textAlign: TextAlign.center,
                                                ),
                                                behavior:
                                                    SnackBarBehavior.floating,
                                                margin: EdgeInsets.fromLTRB(
                                                    15, 0, 15, 30),
                                              ),
                                            );
                                          }
                                        } else {
                                          setState(() {
                                            showSpinner = false;
                                          });
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(
                                              content: Text(
                                                "Connexion Internet faible ou inexistante !",
                                                textAlign: TextAlign.center,
                                              ),
                                              behavior:
                                                  SnackBarBehavior.floating,
                                              margin: EdgeInsets.fromLTRB(
                                                  15, 0, 15, 30),
                                            ),
                                          );
                                        }
                                      }
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          content: Text(
                                            "Il semble qu'il n'existe pas de compte associé à ce numéro.\nPeut-être voudriez vous vous inscrire plutôt.",
                                            textAlign: TextAlign.center,
                                          ),
                                          behavior: SnackBarBehavior.floating,
                                          margin: EdgeInsets.fromLTRB(
                                              15, 0, 15, 30),
                                        ),
                                      );
                                    }
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                          reponse[1],
                                          textAlign: TextAlign.center,
                                        ),
                                        behavior: SnackBarBehavior.floating,
                                        margin:
                                            EdgeInsets.fromLTRB(15, 0, 15, 30),
                                      ),
                                    );
                                  }
                                }
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      "Assurez vous d'avoir une bonne connexion et réessayez",
                                      textAlign: TextAlign.center,
                                    ),
                                    behavior: SnackBarBehavior.floating,
                                    margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                  ),
                                );
                              }
                            },
                            child: Text(
                              "Soumettre",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    visible:
                        codeSent && (!triedVerification) && !passwordChanged,
                    child: Form(
                      key: _formKey1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            "Entrez le code reçu :",
                            style: TextStyle(color: AppColors.indigo),
                          ),
                          Theme(
                            data: new ThemeData(
                              primaryColor: AppColors.indigo,
                            ),
                            child: TextFormField(
                              keyboardType: TextInputType.phone,
                              maxLines: 1,
                              textInputAction: TextInputAction.done,
                              decoration: InputDecoration(
                                hintText: "Code reçu par SMS",
                                hintStyle: TextStyle(
                                  fontFamily: "Poppins",
                                  fontSize: 18,
                                  //fontWeight: FontWeight.w600,
                                ),
                              ),
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return "Entrez un code valide svp!";
                                }
                                return null;
                              },
                              onSaved: (val) {
                                codeValidation = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15),
                          Center(
                            child: ElevatedButton(
                              onPressed: () async {
                                if (_formKey1.currentState.validate()) {
                                  _formKey1.currentState.save();
                                  setState(() {
                                    showSpinner = true;
                                  });
                                  final bool hasConnection =
                                      await DataConnectionChecker()
                                          .hasConnection;
                                  if (hasConnection) {
                                    UserCredential userCredential;
                                    try {
                                      userCredential =
                                          await auth.signInWithCredential(
                                        PhoneAuthProvider.credential(
                                            verificationId: dVerificationId,
                                            smsCode: codeValidation),
                                      );
                                    } catch (e) {
                                      /* print(
                                          "ERREUR LORS DU SIGNIN WITH CREDENTIALS $e"); */
                                      setState(() {
                                        showSpinner = false;
                                      });
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          duration: Duration(seconds: 3),
                                          backgroundColor: Colors.black,
                                          content: Text(
                                            "Nous avons rencontré un problème. Veuillez réessayer.",
                                            textAlign: TextAlign.center,
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          behavior: SnackBarBehavior.floating,
                                          margin: EdgeInsets.fromLTRB(
                                              15, 0, 15, 30),
                                        ),
                                      );
                                    }
                                    setState(() {
                                      if (showSpinner) {
                                        showSpinner = false;
                                      }
                                      if (userCredential != null) {
                                        triedVerification = true;
                                        validationDone = true;
                                      } else {
                                        triedVerification = true;
                                        validationDone = false;
                                      }
                                    });
                                  } else {
                                    setState(() {
                                      showSpinner = false;
                                    });
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        duration: Duration(seconds: 3),
                                        backgroundColor: Colors.black,
                                        content: Text(
                                          "Connexion faible ou inexistante!",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        behavior: SnackBarBehavior.floating,
                                        margin:
                                            EdgeInsets.fromLTRB(15, 0, 15, 30),
                                      ),
                                    );
                                  }
                                }
                              },
                              child: Text(
                                "Vérifier le code",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: triedVerification && !passwordChanged,
                    child: Center(
                      child: validationDone
                          ? Form(
                              key: _formKey2,
                              child: Column(
                                children: [
                                  Text(
                                    "Veuillez entrer votre nouveau mot de passe (8 caractères minimum) :",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(height: 15),
                                  TextFormField(
                                    keyboardType: TextInputType.visiblePassword,
                                    textInputAction: TextInputAction.done,
                                    obscureText: !showPassword,
                                    decoration: InputDecoration(
                                      hintText: "Nouveau mot de passe",
                                      hintStyle: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 18,
                                        //fontWeight: FontWeight.w600,
                                      ),
                                      suffixIcon: IconButton(
                                          icon: Icon(
                                            showPassword
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: Colors.brown,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              showPassword = !showPassword;
                                            });
                                          }),
                                    ),
                                    validator: (val) {
                                      if (val == null ||
                                          val.isEmpty ||
                                          val.length < 8) {
                                        return "";
                                      }
                                      return null;
                                    },
                                    onSaved: (val) {
                                      setState(() {
                                        newPassword = val;
                                      });
                                    },
                                  ),
                                  SizedBox(height: 15),
                                  Center(
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Colors.green),
                                      ),
                                      onPressed: () async {
                                        if (await DataConnectionChecker()
                                            .hasConnection) {
                                          if (_formKey2.currentState
                                              .validate()) {
                                            _formKey2.currentState.save();

                                            setState(() {
                                              showSpinner = true;
                                            });
                                            List reponse =
                                                await authState.recoverPassword(
                                              numero: authState
                                                  .recuperationPhoneNumber
                                                  .parseNumber(),
                                              alpha2Code: authState
                                                  .recuperationPhoneNumber
                                                  .isoCode,
                                              password: newPassword,
                                            );
                                            setState(() {
                                              showSpinner = false;
                                            });
                                            if (reponse[0]) {
                                              setState(() {
                                                passwordChanged = true;
                                              });
                                              await Future.delayed(
                                                  Duration(seconds: 2));
                                              Navigator.pop(context);
                                            } else {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                SnackBar(
                                                  content: Text(
                                                    reponse[1],
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  behavior:
                                                      SnackBarBehavior.floating,
                                                  margin: EdgeInsets.fromLTRB(
                                                      15, 0, 15, 30),
                                                ),
                                              );
                                            }
                                          }
                                        } else {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(
                                              content: Text(
                                                "Assurez vous d'avoir une bonne connexion et réessayez",
                                                textAlign: TextAlign.center,
                                              ),
                                              behavior:
                                                  SnackBarBehavior.floating,
                                              margin: EdgeInsets.fromLTRB(
                                                  15, 0, 15, 30),
                                            ),
                                          );
                                        }
                                      },
                                      child: Text("Soumettre"),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Column(
                              children: [
                                Icon(
                                  Icons.cancel_outlined,
                                  color: Colors.redAccent,
                                  size: 30,
                                ),
                                SizedBox(height: 15),
                                Text(
                                  "Validation échouée: Code invalide !",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.redAccent,
                                    fontSize: 18,
                                  ),
                                ),
                                SizedBox(height: 15),
                                Center(
                                  child: ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        codeSent = false;
                                        codeValidation = null;
                                        dVerificationId = null;
                                        triedVerification = false;
                                        validationDone = false;
                                        newPassword = "";
                                        showPassword = false;
                                      });
                                    },
                                    child: Text(
                                      "RÉESSAYER",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                    ),
                  ),
                  Visibility(
                    visible: passwordChanged,
                    child: Center(
                      child: Column(
                        children: [
                          Icon(
                            Icons.check_outlined,
                            color: Colors.green,
                            size: 30,
                          ),
                          SizedBox(height: 15),
                          Text(
                            "Récupération effectuée !",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.green,
                              fontSize: 18,
                            ),
                          ),
                          SizedBox(height: 15),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
