import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../constants/appColors.dart';
import '../../helper/dataConnectionChecker.dart';
import '../../helper/enum.dart';
import '../../helper/phoneInput/utils/phone_number.dart';
import '../../helper/phoneInput/utils/selector_config.dart';
import '../../helper/phoneInput/widgets/input_widget.dart';
import '../../page/auth/customInput.dart';
import '../../page/auth/inOrUp.dart';
import '../../page/inApp/homePage.dart';
import '../../page/auth/forgotPassword.dart';
import '../../state/authState.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();
  bool showSpinner = false;
  String indicatif = "";
  @override
  Widget build(BuildContext context) {
    final Size dSize = MediaQuery.of(context).size;
    AuthState authState = Provider.of<AuthState>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.orange),
        centerTitle: true,
        title: Text(
          "CONNEXION",
          style: TextStyle(color: Colors.orange, fontSize: dSize.width * 0.05),
        ),
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Builder(
          builder: (context) => Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(dSize.width * 0.020),
              child: Column(
                children: [
                  InOrUp(
                    signIn: true,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: 10, horizontal: dSize.width * 0.020),
                    child: Text(
                      "Veuillez renseigner vos données de connexion.",
                      style: TextStyle(
                        fontSize: dSize.width * 0.047,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Numéro de téléphone",
                          style: TextStyle(color: AppColors.indigo),
                        ),
                        SizedBox(height: 15),
                        InternationalPhoneNumberInput(
                          errorMessage: "Numéro invalide",
                          initialValue: authState.phoneNumber,
                          countries: authState.countriesAvailable ??
                              [
                                'TG',
                                'BJ',
                              ],
                          onInputChanged: (PhoneNumber number) {},
                          selectorConfig: SelectorConfig(
                            selectorType: PhoneInputSelectorType.DIALOG,
                            backgroundColor: Colors.black,
                            countryComparator:
                                authState.countriesAvailable == null
                                    ? null
                                    : (a, b) {
                                        if (authState.countriesAvailable
                                                .indexOf(a.alpha2Code) <
                                            authState.countriesAvailable
                                                .indexOf(b.alpha2Code)) {
                                          return -1;
                                        }
                                        return 1;
                                      },
                          ),
                          ignoreBlank: false,
                          hintText: "Insérer le numéro",
                          locale: "fr",
                          autoValidateMode: AutovalidateMode.onUserInteraction,
                          selectorTextStyle: TextStyle(color: Colors.black),
                          onSaved: (String value) {
                            authState.telephone = value;
                          },
                        ),
                        SizedBox(height: 15),
                        CustomInput(
                          label: "Mot de passe",
                          keyboardType: TextInputType.visiblePassword,
                          input: Inputs.Password,
                          hintText: "Votre mot de passe",
                          textInputAction: TextInputAction.done,
                          onSavedType: OnSaved.Password,
                          maxLine: 1,
                          maxLength: null,
                          passwordShowable: true,
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 25),

                  ///VALIDATION BUTTON
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () async {
                            if (await DataConnectionChecker().hasConnection) {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                setState(() {
                                  showSpinner = true;
                                });
                                List reponse = await authState.signIn(
                                    yPhoneNumber: null, yPassword: null);
                                setState(() {
                                  showSpinner = false;
                                });
                                if (reponse[0]) {
                                  NavigatorState navigator =
                                      Navigator.of(context);
                                  Route route = ModalRoute.of(context);
                                  while (navigator.canPop())
                                    navigator.removeRouteBelow(route);
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => HomePage(),
                                    ),
                                  );
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        reponse[1],
                                        textAlign: TextAlign.center,
                                      ),
                                      behavior: SnackBarBehavior.floating,
                                      margin:
                                          EdgeInsets.fromLTRB(15, 0, 15, 30),
                                    ),
                                  );
                                }
                              }
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                    "Assurez vous d'avoir une bonne connexion et réessayez",
                                    textAlign: TextAlign.center,
                                  ),
                                  behavior: SnackBarBehavior.floating,
                                  margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                ),
                              );
                            }
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) =>
                                  Colors.green.withOpacity(0.8),
                            ),
                          ),
                          child: Text(
                            "SE CONNECTER",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 12),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ForgotPassword(),
                        ),
                      );
                    },
                    child: Text(
                      "Mot de passe oublié ?",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
