import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../constants/appColors.dart';
import '../../helper/coolStepper/cool_stepper.dart';
import '../../helper/dataConnectionChecker.dart';
import '../../helper/enum.dart';
import '../../helper/phoneInput/utils/phone_number.dart';
import '../../helper/phoneInput/utils/selector_config.dart';
import '../../helper/phoneInput/widgets/input_widget.dart';
import '../../helper/utils.dart';
import '../../page/inApp/homePage.dart';
import '../../page/auth/signin.dart';
import '../../state/authState.dart';
import '../inApp/about/termAndConditions.dart';
import 'customInput.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  final _formKey1 = GlobalKey<FormState>();
  bool hasCode = false;
  bool acceptConditions = true;
  bool showSpinner = false;
  bool codeSent = false;
  String codeValidation;
  String dVerificationId;
  bool triedVerification = false;
  bool validationDone = false;

  @override
  Widget build(BuildContext context) {
    final Size dSize = MediaQuery.of(context).size;
    AuthState authState = Provider.of<AuthState>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Colors.orange),
        centerTitle: true,
        title: Text(
          "INSCRIPTION",
          style: TextStyle(color: Colors.orange, fontSize: dSize.width * 0.05),
        ),
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Form(
          key: _formKey,
          child: Builder(
            builder: (context) => Container(
              child: CoolStepper(
                onCompleted: () async {
                  if (await DataConnectionChecker().hasConnection) {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      setState(() {
                        showSpinner = true;
                      });
                      List reponse = await authState.signUp();
                      setState(() {
                        showSpinner = false;
                      });
                      if (reponse[0]) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            duration: Duration(seconds: 1),
                            backgroundColor: AppColors.green,
                            content: Text(
                              "Succès! Vous avez été enregistré!",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                            behavior: SnackBarBehavior.floating,
                            margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                          ),
                        );
                        await Future.delayed(Duration(seconds: 1));
                        setState(() {
                          showSpinner = true;
                        });
                        List reponse1 = await authState.signIn(
                            yPhoneNumber: null, yPassword: null);
                        setState(() {
                          showSpinner = false;
                        });
                        if (reponse1[0]) {
                          NavigatorState navigator = Navigator.of(context);
                          Route route = ModalRoute.of(context);
                          while (navigator.canPop())
                            navigator.removeRouteBelow(route);
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HomePage(),
                            ),
                          );
                        } else {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SignIn(),
                            ),
                          );
                        }
                      } else {
                        if (reponse[1] ==
                            "Le numéro de téléphone utilisé est déjà enregistré!") {
                          setState(() {
                            codeSent = false;
                            codeValidation = null;
                            dVerificationId = null;
                            triedVerification = false;
                            validationDone = false;
                          });
                        }
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              reponse[1] ??
                                  "Nous avons rencontré un problème. Veuillez réessayer plutard svp !",
                              textAlign: TextAlign.center,
                            ),
                            behavior: SnackBarBehavior.floating,
                            margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                          ),
                        );
                      }
                    } else {
                      noConnectionSnackbar(context: context);
                    }
                  }
                },
                steps: [
                  CoolStep(
                    title: "INFORMATIONS PERSONNELLES",
                    subtitle: null,
                    content: Column(
                      children: [
                        CustomInput(
                          label: "Nom *",
                          keyboardType: TextInputType.name,
                          input: Inputs.Nom,
                          hintText: "Insérer votre nom",
                          textInputAction: TextInputAction.done,
                          onSavedType: OnSaved.Nom,
                          maxLine: 1,
                          maxLength: null,
                        ),
                        CustomInput(
                          label: "Prénoms *",
                          keyboardType: TextInputType.name,
                          input: Inputs.Prenoms,
                          hintText: "Insérer vos prénoms",
                          textInputAction: TextInputAction.done,
                          onSavedType: OnSaved.Prenoms,
                          maxLine: 1,
                          maxLength: null,
                        ),
                        CustomInput(
                          label: "E-mail (Facultatif)",
                          keyboardType: TextInputType.emailAddress,
                          input: Inputs.Email,
                          hintText: "Insérer votre e-mail",
                          textInputAction: TextInputAction.done,
                          onSavedType: OnSaved.Email,
                          maxLine: 1,
                          maxLength: null,
                        ),
                      ],
                    ),
                    validation: () {
                      if (!_formKey.currentState.validate()) {
                        return "Une erreur est survenue";
                      }
                      _formKey.currentState.save();
                      return null;
                    },
                  ),
                  CoolStep(
                    title: "INFORMATIONS DU COMPTE",
                    subtitle: "",
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Visibility(
                          visible: !codeSent,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Numéro de téléphone",
                                style: TextStyle(color: AppColors.indigo),
                              ),
                              SizedBox(height: 15),
                              InternationalPhoneNumberInput(
                                errorMessage: "Numéro invalide",
                                countries: authState.countriesAvailable ??
                                    [
                                      'TG',
                                      'BJ',
                                    ],
                                onInputChanged: (PhoneNumber number) {
                                  authState.alpha2Code = number.isoCode;
                                  authState.phoneNumber = number;
                                },
                                selectorConfig: SelectorConfig(
                                  selectorType: PhoneInputSelectorType.DIALOG,
                                  backgroundColor: Colors.black,
                                  countryComparator:
                                      authState.countriesAvailable == null
                                          ? null
                                          : (a, b) {
                                              if (authState.countriesAvailable
                                                      .indexOf(a.alpha2Code) <
                                                  authState.countriesAvailable
                                                      .indexOf(b.alpha2Code)) {
                                                return -1;
                                              }
                                              return 1;
                                            },
                                ),
                                initialValue: authState.phoneNumber,
                                ignoreBlank: false,
                                hintText: "Insérer le numéro",
                                locale: "fr",
                                autoValidateMode:
                                    AutovalidateMode.onUserInteraction,
                                selectorTextStyle:
                                    TextStyle(color: Colors.black),
                                onSaved: (String value) {
                                  authState.telephone = value;
                                },
                              ),
                              SizedBox(height: 15),
                              Center(
                                child: ElevatedButton(
                                  onPressed: () async {
                                    if (_formKey.currentState.validate()) {
                                      /* print("PHONE NUMBER IS" +
                                          authState.phoneNumber.phoneNumber); */
                                      final String authPhoneNumber =
                                          authState.phoneNumber.phoneNumber;
                                      _formKey.currentState.save();
                                      auth.setLanguageCode('fr');
                                      final bool hasConnection =
                                          await DataConnectionChecker()
                                              .hasConnection;
                                      if (hasConnection) {
                                        bool doSend = false;
                                        bool numberDoesNotExists = false;
                                        //
                                        setState(() {
                                          showSpinner = true;
                                        });
                                        List reponseExistence =
                                            await authState.checkExistence(
                                                numero: authState.phoneNumber
                                                    .parseNumber(),
                                                alpha2Code: authState
                                                    .phoneNumber.isoCode);
                                        setState(() {
                                          showSpinner = false;
                                        });
                                        if (reponseExistence[0]) {
                                          if (!reponseExistence[1]) {
                                            numberDoesNotExists = true;
                                          } else {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              SnackBar(
                                                content: Text(
                                                  "Ce numéro existe déjà et ne peut plus être utilisé. Connectez-vous plutôt.",
                                                  textAlign: TextAlign.center,
                                                ),
                                                behavior:
                                                    SnackBarBehavior.floating,
                                                margin: EdgeInsets.fromLTRB(
                                                    15, 0, 15, 30),
                                              ),
                                            );
                                          }
                                        } else {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(
                                              content: Text(
                                                reponseExistence[1].toString(),
                                                textAlign: TextAlign.center,
                                              ),
                                              behavior:
                                                  SnackBarBehavior.floating,
                                              margin: EdgeInsets.fromLTRB(
                                                  15, 0, 15, 30),
                                            ),
                                          );
                                        }
                                        //
                                        if (numberDoesNotExists) {
                                          await showDialog(
                                            barrierDismissible: false,
                                            context: context,
                                            builder: (_) => AlertDialog(
                                              title: Text(
                                                "CONFIRMATION",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                ),
                                              ),
                                              content: Text(
                                                "Ce numéro est correct ?\n$authPhoneNumber",
                                                style: TextStyle(fontSize: 17),
                                              ),
                                              actions: [
                                                TextButton(
                                                  child: Text("NON"),
                                                  onPressed: () {
                                                    doSend = false;
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                                TextButton(
                                                  child: Text("OUI"),
                                                  onPressed: () {
                                                    doSend = true;
                                                    Navigator.of(context).pop();
                                                  },
                                                )
                                              ],
                                            ),
                                          );
                                          if (doSend) {
                                            setState(() {
                                              showSpinner = true;
                                            });
                                            await FirebaseAuth.instance
                                                .verifyPhoneNumber(
                                              phoneNumber: authState
                                                  .phoneNumber.phoneNumber,
                                              verificationCompleted:
                                                  (PhoneAuthCredential
                                                      credential) {
                                                //print("AUTH COMPLETED");
                                              },
                                              verificationFailed:
                                                  (FirebaseAuthException e) {
                                                //print("AUTH FAILED");
                                                setState(() {
                                                  showSpinner = false;
                                                });
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(
                                                  SnackBar(
                                                    content: Text(
                                                      "Nous avons rencontré un problème, veuillez réessayer svp !",
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    behavior: SnackBarBehavior
                                                        .floating,
                                                    margin: EdgeInsets.fromLTRB(
                                                        15, 0, 15, 30),
                                                  ),
                                                );
                                              },
                                              codeSent: (String verificationId,
                                                  int resendToken) {
                                                //print("CODE SENT");
                                                setState(() {
                                                  dVerificationId =
                                                      verificationId;
                                                  codeSent = true;
                                                  showSpinner = false;
                                                });
                                              },
                                              codeAutoRetrievalTimeout:
                                                  (String verificationId) {},
                                            );
                                            /* setState(() {
                                            showSpinner = false;
                                          }); */
                                          }
                                        }
                                      } else {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          SnackBar(
                                            duration: Duration(seconds: 3),
                                            backgroundColor: Colors.black,
                                            content: Text(
                                              "Connexion faible ou inexistante!",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            behavior: SnackBarBehavior.floating,
                                            margin: EdgeInsets.fromLTRB(
                                                15, 0, 15, 30),
                                          ),
                                        );
                                      }
                                    }
                                  },
                                  child: Text(
                                    "Envoyer un code de validation",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: codeSent && (!triedVerification),
                          child: Form(
                            key: _formKey1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Text(
                                  "Validation numéro",
                                  style: TextStyle(color: AppColors.indigo),
                                ),
                                Theme(
                                  data: new ThemeData(
                                    primaryColor: AppColors.indigo,
                                  ),
                                  child: TextFormField(
                                    keyboardType: TextInputType.phone,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    decoration: InputDecoration(
                                      hintText: "Code reçu par SMS",
                                      hintStyle: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 18,
                                        //fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    validator: (val) {
                                      if (val == null || val.isEmpty) {
                                        return "Entrez un code valide svp!";
                                      }
                                      return null;
                                    },
                                    onSaved: (val) {
                                      codeValidation = val;
                                    },
                                  ),
                                ),
                                SizedBox(height: 15),
                                Center(
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      if (_formKey1.currentState.validate()) {
                                        _formKey1.currentState.save();
                                        final bool hasConnection =
                                            await DataConnectionChecker()
                                                .hasConnection;
                                        if (hasConnection) {
                                          setState(() {
                                            showSpinner = true;
                                          });
                                          UserCredential userCredential;
                                          try {
                                            userCredential =
                                                await auth.signInWithCredential(
                                              PhoneAuthProvider.credential(
                                                  verificationId:
                                                      dVerificationId,
                                                  smsCode: codeValidation),
                                            );
                                          } catch (e) {
                                            /* print(
                                                "ERREUR LORS DU SIGNIN WITH CREDENTIALS $e"); */
                                          }
                                          setState(() {
                                            showSpinner = false;
                                            if (userCredential != null) {
                                              triedVerification = true;
                                              validationDone = true;
                                            } else {
                                              triedVerification = true;
                                              validationDone = false;
                                            }
                                          });
                                        } else {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(
                                              duration: Duration(seconds: 3),
                                              backgroundColor: Colors.black,
                                              content: Text(
                                                "Connexion faible ou inexistante!",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              behavior:
                                                  SnackBarBehavior.floating,
                                              margin: EdgeInsets.fromLTRB(
                                                  15, 0, 15, 30),
                                            ),
                                          );
                                        }
                                      }
                                    },
                                    child: Text(
                                      "Vérifier le code",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: triedVerification && validationDone,
                          child: Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.check_circle_outline_outlined,
                                  color: Colors.green,
                                  size: 30,
                                ),
                                SizedBox(height: 15),
                                Text(
                                  "Code validé avec succès !",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: (triedVerification && (!validationDone)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.cancel_outlined,
                                color: Colors.redAccent,
                                size: 30,
                              ),
                              SizedBox(height: 15),
                              Text(
                                "Validation échouée: Code invalide !",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.redAccent,
                                  fontSize: 18,
                                ),
                              ),
                              SizedBox(height: 15),
                              Center(
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      codeSent = false;
                                      codeValidation = null;
                                      dVerificationId = null;
                                      triedVerification = false;
                                      validationDone = false;
                                    });
                                  },
                                  child: Text(
                                    "RÉESSAYER",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    validation: () {
                      if (!codeSent) {
                        if (_formKey.currentState.validate()) {
                          return "Vérifiez votre numéro de télephone pour continuer !";
                        } else {
                          return "Une erreur est survenue";
                        }
                      }
                      if (codeSent) {
                        if (triedVerification) {
                          if (validationDone) {
                            return null;
                          } else {
                            return "Vérification échouée: Code invalide !";
                          }
                        } else {
                          if (_formKey1.currentState.validate()) {
                            return "Vérifiez votre numéro de télephone pour continuer !";
                          } else {
                            return "Une erreur est survenue";
                          }
                        }
                      }
                      return null;
                    },
                  ),
                  CoolStep(
                    title: "SÉCURITÉ",
                    subtitle: "",
                    content: Column(
                      children: [
                        CustomInput(
                          label: "Mot de passe *",
                          keyboardType: TextInputType.visiblePassword,
                          input: Inputs.Password,
                          hintText: "Votre mot de passe",
                          textInputAction: TextInputAction.done,
                          onSavedType: OnSaved.Password,
                          maxLine: 1,
                          maxLength: null,
                        ),
                        CustomInput(
                          label: "Confirmation Mot de passe *",
                          keyboardType: TextInputType.visiblePassword,
                          input: Inputs.ConfirmPassword,
                          hintText: "Répetez le mot de passe",
                          textInputAction: TextInputAction.done,
                          onSavedType: OnSaved.ConfirmPassword,
                          maxLine: 1,
                          maxLength: null,
                        ),
                      ],
                    ),
                    validation: () {
                      if (!_formKey.currentState.validate()) {
                        return "Une erreur est survenue";
                      }
                      _formKey.currentState.save();
                      return null;
                    },
                  ),
                  CoolStep(
                    title: "CODE DE PARRAINAGE",
                    subtitle: "",
                    content: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                          margin: EdgeInsets.only(bottom: 10),
                          decoration: BoxDecoration(
                            color: AppColors.bgWarning,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: Text(
                            "Cette étape est optionnelle, insérez si vous en avez, un code parrainage ou passez à l'étape suivante! Ce code sert à récompenser la personne qui vous a suggeré notre application.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        CheckboxListTile(
                          checkColor: Colors.white,
                          activeColor: AppColors.indigo,
                          isThreeLine: false,
                          contentPadding: EdgeInsets.all(0),
                          dense: true,
                          title: Text(
                            "J'ai un code de parrainage",
                            style: TextStyle(fontSize: 18),
                          ),
                          controlAffinity: ListTileControlAffinity.leading,
                          value: authState.hasCode,
                          onChanged: (bool val) {
                            setState(() {
                              authState.hasCode = val;
                            });
                          },
                        ),
                        Visibility(
                          visible: authState.hasCode,
                          child: CustomInput(
                            label: "Code de parrainage",
                            keyboardType: TextInputType.name,
                            input: Inputs.CodeParrainage,
                            hintText: "Insérer le code",
                            textInputAction: TextInputAction.done,
                            onSavedType: OnSaved.CodeParrainage,
                            maxLine: 1,
                            maxLength: null,
                          ),
                        ),
                        CheckboxListTile(
                          checkColor: Colors.white,
                          activeColor: AppColors.indigo,
                          isThreeLine: false,
                          contentPadding: EdgeInsets.all(0),
                          dense: true,
                          title: Wrap(
                            children: [
                              Text(
                                "J'accepte ",
                                style: TextStyle(fontSize: 18),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          TermsAndConditions(),
                                    ),
                                  );
                                },
                                child: Text(
                                  "Termes et conditions *",
                                  style: TextStyle(
                                      fontSize: 18, color: AppColors.indigo),
                                ),
                              )
                            ],
                          ),
                          controlAffinity: ListTileControlAffinity.leading,
                          value: acceptConditions,
                          onChanged: (bool val) {
                            setState(() {
                              acceptConditions = val;
                            });
                          },
                        ),
                      ],
                    ),
                    validation: () {
                      if (hasCode) {
                        if (!_formKey.currentState.validate()) {
                          return "Une erreur est survenue";
                        } else {
                          if (!acceptConditions) {
                            return "Pour continuer veuillez accepter les conditions";
                          }
                          _formKey.currentState.save();
                        }
                      }
                      if (!acceptConditions) {
                        return "Pour continuer veuillez accepter les conditions";
                      }
                      return null;
                    },
                  ),
                ],
                showErrorSnackbar: true,
                config: CoolStepperConfig(
                  headerColor: Colors.white,
                  titleTextStyle: TextStyle(
                    color: AppColors.yellowAppbar,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins',
                  ),
                  subtitleTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontFamily: 'Poppins',
                  ),
                  backText: "RETOUR",
                  nextText: "SUIVANT",
                  finalText: "SUIVANT",
                  stepText: "ÉTAPE ",
                  ofText: "/",
                  icon: null,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
