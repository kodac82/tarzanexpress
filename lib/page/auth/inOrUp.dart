import 'package:flutter/material.dart';

import 'signin.dart';
import 'signup.dart';

class InOrUp extends StatelessWidget {
  final bool signIn;
  InOrUp({this.signIn = true});
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => signIn ? SignUp() : SignIn(),
          ),
        );
      },
      child: Wrap(
        children: [
          Text(
            signIn ? "Pas de compte?" : "Déjà un compte?",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 17, color: Colors.black54),
          ),
          Text(
            signIn
                ? " S'Inscrire"
                : "Déjà un compte? Cliquez ici pour vous connecter",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 17, color: Colors.orange),
          ),
        ],
      ),
    );
  }
}
