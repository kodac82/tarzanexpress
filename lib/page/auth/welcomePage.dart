import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:tarzanexpress/main.dart';
import '../../helper/dataConnectionChecker.dart';
import '../../state/authState.dart';
import '../../constants/appColors.dart';
import 'signin.dart';
import 'signup.dart';

/*TODO: PROGRAMMERS, PLEASE READ THIS
  I FOUND A PLUGIN ON PUB.DEV FOR SHOWING COUNTRIES, BUT IT WAS LACKING OF CUSTOMISATION POSSIBILITIES.
  SO INSTEAD OF ADDING THE PLUGIN I COPIED ITS SOURCE CODE TO CUSTOMISE IT.
  IT IS IN THE LIB/HELPER/PHONEINPUT DIRECTORY
  I DID THE SAME THING TO SOME PLUGINS TOO. THEY'RE ALL IN THE HELPER DIRECTORY
*/

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  bool _showSpinner = false;

  @override
  Widget build(BuildContext context) {
    final screenDetails = MediaQuery.of(context);
    final Size dSize = screenDetails.size;
    /* double littleText = screenDetails.orientation == Orientation.portrait
        ? screenDetails.size.width / 18
        : screenDetails.size.width / 30; */

    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: _showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Builder(
          builder: (context) => Container(
            padding: EdgeInsets.symmetric(
              horizontal: dSize.width * 0.05,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Spacer(),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text:
                        "Commander partout dans le monde\ndevient simple et facile avec ",
                    style: TextStyle(
                      fontSize: dSize.width * 0.05,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'TarzanExpress',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Colors.orange,
                          fontSize: dSize.width * 0.07,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: dSize.height * 0.03,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: dSize.height * 0.02),
                  width: MediaQuery.of(context).size.width,
                  child: TextButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                        EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      ),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30),
                        ),
                      ),
                      backgroundColor: MaterialStateProperty.all(Colors.orange),
                    ),
                    onPressed: () async {
                      bool answer = false;
                      if (await DataConnectionChecker().hasConnection) {
                        setState(() {
                          _showSpinner = true;
                        });
                        answer =
                            await Provider.of<AuthState>(context, listen: false)
                                .getCountriesAvailable();
                        setState(() {
                          _showSpinner = false;
                        });
                        if (answer) {
                          Navigator.push(
                            navKey.currentContext,
                            MaterialPageRoute(
                              builder: (context) => SignUp(),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text(
                                "Une erreur est survenue. Veuillez réessayer plutard.",
                                textAlign: TextAlign.center,
                              ),
                              behavior: SnackBarBehavior.floating,
                              margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                            ),
                          );
                        }
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              "Connection internet faible ou inexistante.",
                              textAlign: TextAlign.center,
                            ),
                            behavior: SnackBarBehavior.floating,
                            margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                          ),
                        );
                      }
                    },
                    child: Text(
                      "S'inscrire",
                      style: TextStyle(
                        color: Colors.white,
                        //fontSize: littleText,
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Wrap(
                    children: <Widget>[
                      Text(
                        'Déjà inscrit? ',
                        textAlign: TextAlign.center,
                        /* style: TextStyle(
                          fontSize: littleText,
                        ), */
                      ),
                      GestureDetector(
                        onTap: () async {
                          bool answer = false;
                          if (await DataConnectionChecker().hasConnection) {
                            setState(() {
                              _showSpinner = true;
                            });
                            answer = await Provider.of<AuthState>(context,
                                    listen: false)
                                .getCountriesAvailable();
                            setState(() {
                              _showSpinner = false;
                            });
                            if (answer) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SignIn(),
                                ),
                              );
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                    "Une erreur est survenue. Veuillez réessayer plutard.",
                                    textAlign: TextAlign.center,
                                  ),
                                  behavior: SnackBarBehavior.floating,
                                  margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                ),
                              );
                            }
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                  "Connection internet faible ou inexistante.",
                                  textAlign: TextAlign.center,
                                ),
                                behavior: SnackBarBehavior.floating,
                                margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                              ),
                            );
                          }
                        },
                        child: Text(
                          'Connectez-vous',
                          style: TextStyle(
                            //fontSize: littleText,
                            color: Colors.orange,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
