import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../page/inApp/notifications/articleDetails.dart';
import '../../../state/articleState.dart';

class Divers extends StatefulWidget {
  @override
  _DiversState createState() => _DiversState();
}

class _DiversState extends State<Divers> {
  @override
  Widget build(BuildContext context) {
    var a = Provider.of<ArticleState>(context, listen: false);
    return Scaffold(
      body: FutureBuilder<Object>(
          future: DataConnectionChecker().hasConnection,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data) {
                return FutureBuilder(
                  future: SharedPreferences.getInstance(),
                  builder: (context, snapY) {
                    if (snapY.connectionState == ConnectionState.done) {
                      if (snapY.data != null) {
                        SharedPreferences prefs = snapY.data;
                        List<String> articlesRead = [];
                        if (prefs.containsKey('articlesRead')) {
                          articlesRead = prefs.getStringList('articlesRead');
                        } else {
                          prefs.setStringList('articlesRead', []);
                        }
                        return FutureBuilder(
                          future: a.getArticles(context: context),
                          builder: (context, snap) {
                            if (snap.connectionState == ConnectionState.done) {
                              if (a.articles == null) {
                                return Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.link_off),
                                        Text(
                                          "Une erreur s'est produite. Notre équipe est en train de résoudre le problème.Veuillez réessayer plutard.",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              } else {
                                if (a.articles.length != 0) {
                                  List elements = [
                                    for (var i = 0; i < a.articles.length; i++)
                                      if (a.articles[i].type['param1'] !=
                                          "PROMO")
                                        Card(
                                          elevation: 3,
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 10.0, vertical: 3.5),
                                          color: /* a.articles[i].etatLecture.toString() */ !articlesRead
                                                  .contains(a.articles[i].id
                                                      .toString())
                                              ? Colors.grey.shade200
                                              : Colors.white,
                                          child: ListTile(
                                            onTap: () async {
                                              /* await Provider.of<ArticleState>(context,
                                          listen: false)
                                      .articleIsRead(
                                          context: context,
                                          notificationId: a.articles[i].id); */
                                              try {
                                                if (prefs.containsKey(
                                                    'articlesRead')) {
                                                  articlesRead =
                                                      prefs.getStringList(
                                                          'articlesRead');
                                                } else {
                                                  articlesRead = [];
                                                }
                                                articlesRead.add(a
                                                    .articles[i].id
                                                    .toString());
                                                prefs.setStringList(
                                                  'articlesRead',
                                                  articlesRead,
                                                );
                                              } catch (e) {}
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ArticleDetails(
                                                    article: a.articles[i],
                                                    isPromo: false,
                                                  ),
                                                ),
                                              );
                                            },
                                            leading:
                                                /* a.articles[i].etatLecture.toString() */ !articlesRead
                                                        .contains(a
                                                            .articles[i].id
                                                            .toString())
                                                    ? Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color:
                                                              Colors.blueAccent,
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                        height: 10.0,
                                                        width: 10.0,
                                                      )
                                                    : Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color: Colors
                                                              .grey.shade400,
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                        height: 10.0,
                                                        width: 10.0,
                                                      ),
                                            title: Text(
                                              a.articles[i].titre,
                                              style: TextStyle(fontSize: 18.0),
                                            ),
                                            subtitle: Text(
                                              a.articles[i].contenu,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 2,
                                              style: TextStyle(fontSize: 16.0),
                                            ),
                                          ),
                                        )
                                  ];
                                  return SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: elements.isNotEmpty
                                          ? [...elements]
                                          : [
                                              Center(
                                                child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(vertical: 20),
                                                  child: Text(
                                                    "AUCUN DIVERS DISPONIBLE",
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                    ),
                                  );
                                } else {
                                  return Center(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 20),
                                      child: Text(
                                        "AUCUN DIVERS DISPONIBLE ",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              }
                            }
                            return Center(
                              child: Container(
                                padding: EdgeInsets.all(10),
                                color: Colors.white,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    CircularProgressIndicator(),
                                    SizedBox(width: 10),
                                    Text("Récupération des données ..")
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      }
                      return Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.broken_image_outlined,
                                color: Colors.red,
                              ),
                              Text(
                                "L'application a rencontré un problème. Veuillez réessayer.",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              ),
                              ElevatedButton(
                                onPressed: () => setState(() {}),
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Colors.blue,
                                  ),
                                ),
                                child: Text(
                                  "RÉESSAYER",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                    return Center(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        color: Colors.white,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CircularProgressIndicator(),
                            SizedBox(width: 10),
                            Text("Initialisation..")
                          ],
                        ),
                      ),
                    );
                  },
                );
              }
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.wifi_off),
                      Text(
                        "Connection internet faible ou inexistante.",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                      ElevatedButton(
                        onPressed: () => setState(() {}),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Colors.blue,
                          ),
                        ),
                        child: Text(
                          "RÉESSAYER",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
            return Center(
              child: Container(
                padding: EdgeInsets.all(10),
                color: Colors.white,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(width: 10),
                    Text("Récupération des données..")
                  ],
                ),
              ),
            );
          }),
    );
  }
}
