import 'package:flutter/material.dart';
import '../../../page/inApp/notifications/divers.dart';
import 'notifications.dart';
import '../../../helper/utils.dart';
import '../../../constants/appAssets.dart';
import 'promo.dart';

class Actualites extends StatefulWidget {
  final int type;
  Actualites({this.type});
  @override
  _ActualitesState createState() => _ActualitesState();
}

class _ActualitesState extends State<Actualites>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    if (widget.type != null) {
      _tabController.animateTo(widget.type);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: yellowAppbar(title: "Notifications"),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.fromLTRB(0, 14, 0, 0),
        child: Column(
          children: [
            TabBar(
                controller: _tabController,
                indicatorColor: Colors.amber,
                indicatorWeight: 4,
                labelColor: Colors.black,
                labelStyle:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                isScrollable: true,
                //indicatorPadding: EdgeInsets.only(right: 15.0),
                tabs: [
                  Tab(
                    child: Row(
                      children: [
                        ImageIcon(
                          AssetImage(AppAssets.notifIcon),
                          color: Colors.red.shade400,
                        ),
                        SizedBox(width: 4),
                        Text("INFO"),
                      ],
                    ),
                  ),
                  Tab(
                    child: Row(
                      children: [
                        ImageIcon(
                          AssetImage(AppAssets.promoIcon),
                          color: Colors.lightGreen.shade400,
                        ),
                        SizedBox(width: 4),
                        Text("PROMO"),
                      ],
                    ),
                  ),
                  Tab(
                    child: Row(
                      children: [
                        ImageIcon(
                          AssetImage(AppAssets.diversIcon),
                          color: Colors.blueAccent,
                        ),
                        SizedBox(width: 4),
                        Text("DIVERS"),
                      ],
                    ),
                  )
                ]),
            Flexible(
              child: Container(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    Notifications(),
                    Promo(),
                    Divers(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
