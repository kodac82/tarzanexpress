import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../../model/article.dart';
import '../../../widgets/customWidgets.dart';

class ArticleDetails extends StatefulWidget {
  final bool isPromo;
  final ArticleModel article;
  ArticleDetails({@required this.isPromo, @required this.article});
  @override
  _ArticleDetailsState createState() => _ArticleDetailsState();
}

class _ArticleDetailsState extends State<ArticleDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.orange),
        centerTitle: true,
        title: Text(
          widget.isPromo ? "Détails Promo" : "Détails Divers",
          style: TextStyle(color: Colors.orange, fontSize: 16),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 25,
            ),
            Text(
              widget.article.titre,
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 12,
            ),
            ![null, 'null'].contains(widget.article.photoAvantLien)
                ? Container(
                    color: Colors.grey.shade300,
                    width: fullWidth(context),
                    height: MediaQuery.of(context).size.height * 0.35,
                    child: CachedNetworkImage(
                      height: MediaQuery.of(context).size.height * 0.35,
                      fit: BoxFit.cover,
                      imageUrl: widget.article.photoAvantLien.startsWith("http")
                          ? widget.article.photoAvantLien
                          : "http://" + widget.article.photoAvantLien,
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) {
                        return Center(
                          child: CircularProgressIndicator(
                            value: downloadProgress.progress,
                          ),
                        );
                      },
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  )
                : Container(),
            SizedBox(
              height: 25,
            ),
            Text(
              widget.article.contenu,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
