import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../helper/dataConnectionChecker.dart';
import '../../../model/notification.dart';
import '../../../page/inApp/commandes/orderDetails.dart';
import '../../../page/inApp/messages/orderConversation.dart';
import '../../../state/notificationState.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    var a = Provider.of<NotificationState>(context, listen: false);
    var showMore = ValueNotifier(10);
    return Scaffold(
      body: FutureBuilder(
          future: DataConnectionChecker().hasConnection,
          builder: (context, snapshot) {
            if ((snapshot.connectionState == ConnectionState.done) &&
                snapshot.data) {
              return FutureBuilder(
                future: a.getNotifications(context: context),
                builder: (context, snap) {
                  if (snap.connectionState == ConnectionState.done) {
                    if (a.notifications == null) {
                      return Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.link_off),
                              Text(
                                "Une erreur s'est produite. Notre équipe est en train de résoudre le problème.Veuillez réessayer plutard.",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              ),
                            ],
                          ),
                        ),
                      );
                    } else {
                      if (a.notifications.length != 0) {
                        return SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ValueListenableBuilder(
                                valueListenable: showMore,
                                builder: (context, n, _) {
                                  List<NotificationModel> uNotifs =
                                      a.notifications.sublist(
                                          0,
                                          (a.notifications.length - n > 0)
                                              ? n
                                              : a.notifications.length);
                                  return Column(
                                    children: [
                                      for (var i = 0; i < uNotifs.length; i++)
                                        Card(
                                          elevation: 3,
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 10.0, vertical: 3.5),
                                          color: uNotifs[i]
                                                      .etatLecture
                                                      .toString() ==
                                                  "0"
                                              ? Colors.grey.shade200
                                              : Colors.white,
                                          child: ListTile(
                                            onTap: () async {
                                              await Provider.of<
                                                          NotificationState>(
                                                      context,
                                                      listen: false)
                                                  .notificationIsRead(
                                                      context: context,
                                                      notificationId: a
                                                          .notifications[i].id);
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) => (a
                                                              .notifications[i]
                                                              .type ==
                                                          "ETAT_COMMANDE")
                                                      ? OrderDetails(
                                                          id: a.notifications[i]
                                                              .commandeId)
                                                      : OrderConversation(
                                                          id: a.notifications[i]
                                                              .commandeId,
                                                          reference: a
                                                              .notifications[i]
                                                              .commande[
                                                                  "reference"]
                                                              .toString()),
                                                ),
                                              );
                                            },
                                            leading: uNotifs[i]
                                                        .etatLecture
                                                        .toString() ==
                                                    "0"
                                                ? Container(
                                                    decoration: BoxDecoration(
                                                      color:
                                                          Colors.red.shade400,
                                                      shape: BoxShape.circle,
                                                    ),
                                                    height: 10,
                                                    width: 10,
                                                  )
                                                : Container(
                                                    decoration: BoxDecoration(
                                                      color:
                                                          Colors.grey.shade400,
                                                      shape: BoxShape.circle,
                                                    ),
                                                    height: 10,
                                                    width: 10,
                                                  ),
                                            title: Text(
                                              (uNotifs[i].type ==
                                                      "ETAT_COMMANDE")
                                                  ? "La commande ${uNotifs[i].commande["reference"]} est ${uNotifs[i].commandeStatut["param1"]}"
                                                  : ("Nouveau message à propos de la commande ${uNotifs[i].commande["reference"].toString()}"),
                                              style: TextStyle(fontSize: 13.0),
                                            ),
                                            subtitle: Text(uNotifs[i].date),
                                          ),
                                        ),
                                      if ((uNotifs.length > 10) &&
                                          (a.notifications.length - n > 0))
                                        Center(
                                          child: ElevatedButton(
                                            onPressed: () {
                                              showMore.value += 10;
                                            },
                                            child: Text("Montrer plus"),
                                          ),
                                        ),
                                    ],
                                  );
                                },
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Center(
                            child: Text(
                              "AUCUNE INFO DISPONIBLE ",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                //color: AppColors.scaffoldBackground,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        );
                      }
                    }
                  }
                  return Center(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      color: Colors.white,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(width: 10),
                          Text("Récupération des données ..")
                        ],
                      ),
                    ),
                  );
                },
              );
            }
            return Center(
              child: Container(
                padding: EdgeInsets.all(10),
                color: Colors.white,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(width: 10),
                    Text("Récupération des données..")
                  ],
                ),
              ),
            );
          }),
    );
  }
}
