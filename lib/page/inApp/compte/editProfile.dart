import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../../../helper/utils.dart';
import '../../../constants/appColors.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../helper/inputValidator.dart';
import '../../../page/inApp/compte/profilePage.dart';
import '../../../state/authState.dart';

class EditProfile extends StatefulWidget {
  final String nom;
  final String prenoms;
  final String telephone;
  final String email;
  EditProfile({
    @required this.nom,
    @required this.prenoms,
    @required this.telephone,
    @required this.email,
  });
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  String nom, prenoms, telephone, email;
  bool showSpinner = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        title: Text("Édition"),
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Builder(
          builder: (context) => Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        "Nom",
                        style: TextStyle(color: AppColors.indigo),
                      ),
                      Theme(
                        data: new ThemeData(
                          primaryColor: AppColors.indigo,
                        ),
                        child: TextFormField(
                          keyboardType: TextInputType.name,
                          textInputAction: TextInputAction.done,
                          maxLength: 20,
                          initialValue: widget.nom,
                          validator: (val) => validateNom(val),
                          onSaved: (val) => nom = val,
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        "Prénoms",
                        style: TextStyle(color: AppColors.indigo),
                      ),
                      Theme(
                        data: new ThemeData(
                          primaryColor: AppColors.indigo,
                        ),
                        child: TextFormField(
                          keyboardType: TextInputType.name,
                          textInputAction: TextInputAction.done,
                          maxLength: 20,
                          initialValue: widget.prenoms,
                          validator: (val) => validatePrenoms(val),
                          onSaved: (val) => prenoms = val,
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        "Télephone",
                        style: TextStyle(color: AppColors.indigo),
                      ),
                      Theme(
                        data: new ThemeData(
                          primaryColor: AppColors.indigo,
                        ),
                        child: TextFormField(
                          keyboardType: TextInputType.phone,
                          textInputAction: TextInputAction.done,
                          initialValue: widget.telephone,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              return "N'oubliez pas le numéro!";
                            }
                            for (var i = 0; i < val.length; i++) {
                              if (val[i] != "0" &&
                                  val[i] != "1" &&
                                  val[i] != "2" &&
                                  val[i] != "3" &&
                                  val[i] != "4" &&
                                  val[i] != "5" &&
                                  val[i] != "6" &&
                                  val[i] != "7" &&
                                  val[i] != "8" &&
                                  val[i] != "9") {
                                return 'Numéro Invalide!';
                              }
                            }
                            if (val.contains(" ")) {
                              return "Entrez un numéro sans espace";
                            }
                            return null;
                          },
                          onSaved: (val) => telephone = val,
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        "E-mail",
                        style: TextStyle(color: AppColors.indigo),
                      ),
                      Theme(
                        data: new ThemeData(
                          primaryColor: AppColors.indigo,
                        ),
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          initialValue: widget.email,
                          validator: (val) => validateEmail(val),
                          onSaved: (val) => email = val,
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                  SizedBox(height: 15),

                  ///VALIDATION BUTTON
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 35, 15, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            onPressed: () async {
                              if (await DataConnectionChecker().hasConnection) {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  setState(() {
                                    showSpinner = true;
                                  });
                                  List reponse = await Provider.of<AuthState>(
                                          context,
                                          listen: false)
                                      .updateProfile(
                                    context: context,
                                    nom: nom,
                                    prenoms: prenoms,
                                    phoneNumber: telephone,
                                    email: email,
                                  );
                                  setState(() {
                                    showSpinner = false;
                                  });
                                  if (reponse[0]) {
                                    await getNewToken(context: context);
                                    NavigatorState navigator =
                                        Navigator.of(context);
                                    Route route = ModalRoute.of(context);
                                    navigator.removeRouteBelow(route);
                                    Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ProfilePage(),
                                      ),
                                    );
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                          reponse[1],
                                          textAlign: TextAlign.center,
                                        ),
                                        behavior: SnackBarBehavior.floating,
                                        margin:
                                            EdgeInsets.fromLTRB(15, 0, 15, 30),
                                      ),
                                    );
                                  }
                                } else {
                                  noConnectionSnackbar(context: context);
                                }
                              }
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                Colors.green.withOpacity(0.8),
                              ),
                            ),
                            child: Text(
                              "SOUMETTRE",
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
