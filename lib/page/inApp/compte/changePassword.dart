import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../constants/appColors.dart';
import '../../../state/authState.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool showSpinner = false;
  final _formKey = GlobalKey<FormState>();
  String oldPassword;
  String newPassword;
  String confirmNewPassword;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Changer Mot de Passe",
          style: TextStyle(color: Colors.orange),
        ),
        backgroundColor: Colors.white,
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Builder(
          builder: (context) => SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 14.4, vertical: 28),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Mot de Passe Actuel",
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      onSaved: (newValue) => oldPassword = newValue,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Entrer le mot de passe!";
                        }
                        if (value.length < 8) {
                          return "Au moins 8 caractères requis";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 21),
                    Text(
                      "Nouveau Mot de Passe",
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      onSaved: (newValue) => newPassword = newValue,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Entrer le mot de passe!";
                        }
                        if (value.length < 8) {
                          return "Au moins 8 caractères requis";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 21),
                    Text(
                      "Répeter le nouveau Mot de Passe",
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      onSaved: (newValue) => confirmNewPassword = newValue,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Entrer le mot de passe!";
                        }
                        if (value.length < 8) {
                          return "Au moins 8 caractères requis";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 35),
                    Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                AppColors.green,
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(10),
                                ),
                              ),
                              padding: MaterialStateProperty.all(
                                EdgeInsets.symmetric(vertical: 10),
                              ),
                            ),
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                if (newPassword == confirmNewPassword) {
                                  SharedPreferences prefs =
                                      await SharedPreferences.getInstance();
                                  if (prefs.getString("password") ==
                                      oldPassword) {
                                    setState(() {
                                      showSpinner = true;
                                    });
                                    bool result = await Provider.of<AuthState>(
                                            context,
                                            listen: false)
                                        .changePassword(
                                            oldPassword: oldPassword,
                                            newPassword: newPassword,
                                            context: context);
                                    setState(() {
                                      showSpinner = false;
                                    });
                                    if (result) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          duration: Duration(seconds: 2),
                                          backgroundColor: AppColors.green,
                                          content: Text(
                                            "Opération éffectuée avec succès !",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13.0),
                                          ),
                                          behavior: SnackBarBehavior.floating,
                                          margin: EdgeInsets.fromLTRB(
                                              18, 0, 18, 49),
                                        ),
                                      );
                                      await Future.delayed(
                                          Duration(seconds: 2));
                                      Navigator.pop(context);
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          duration: Duration(seconds: 3),
                                          backgroundColor: Colors.black,
                                          content: Text(
                                            "Une erreur est survenue. Veuillez réessayer plutard.",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13.0),
                                          ),
                                          behavior: SnackBarBehavior.floating,
                                          margin: EdgeInsets.fromLTRB(
                                              18, 0, 18, 49),
                                        ),
                                      );
                                    }
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        duration: Duration(seconds: 3),
                                        backgroundColor: Colors.black,
                                        content: Text(
                                          "Le mot de passe actuel est incorrect",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 13.0),
                                        ),
                                        behavior: SnackBarBehavior.floating,
                                        margin:
                                            EdgeInsets.fromLTRB(18, 0, 18, 49),
                                      ),
                                    );
                                  }
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      duration: Duration(seconds: 3),
                                      backgroundColor: Colors.black,
                                      content: Text(
                                        "Le nouveau mot de passe n'a pas été répété correctement",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 13.0),
                                      ),
                                      behavior: SnackBarBehavior.floating,
                                      margin:
                                          EdgeInsets.fromLTRB(18, 0, 18, 49),
                                    ),
                                  );
                                }
                              }
                            },
                            child: Text(
                              "SOUMETTRE",
                              style: TextStyle(
                                fontSize: 15.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
