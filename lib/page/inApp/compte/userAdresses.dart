import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../../../constants/appColors.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../model/adresse.dart';
import '../../../services/localDatabase.dart';
import '../../../state/authState.dart';

class UserAdresses extends StatefulWidget {
  final bool calledByOrder;
  UserAdresses({this.calledByOrder});
  @override
  _UserAdressesState createState() => _UserAdressesState();
}

class _UserAdressesState extends State<UserAdresses> {
  bool showSpinner = false;
  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "MES ADRESSES",
          style: TextStyle(
            color: Colors.orange,
          ),
        ),
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: dSize.height * 0.0142),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Colors.white,
                          ),
                          elevation: MaterialStateProperty.all(7)),
                      onPressed: () async {
                        bool gotCountries = false;
                        if (await DataConnectionChecker().hasConnection) {
                          setState(() {
                            showSpinner = true;
                          });
                          gotCountries = await Provider.of<AuthState>(context,
                                  listen: false)
                              .getCountriesAvailable();
                          setState(() {
                            showSpinner = false;
                          });
                          if (gotCountries) {
                            bool answer = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => InsertAdressPage(),
                              ),
                            );
                            if (answer != null && answer) setState(() {});
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                  "Une erreur est survenue. Veuillez réessayer plutard.",
                                  textAlign: TextAlign.center,
                                ),
                                behavior: SnackBarBehavior.floating,
                                margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                              ),
                            );
                          }
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text(
                                "Connection internet faible ou inexistante.",
                                textAlign: TextAlign.center,
                              ),
                              behavior: SnackBarBehavior.floating,
                              margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                            ),
                          );
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Icon(
                            Icons.add,
                            color: Colors.blueAccent,
                          ),
                          Text(
                            "AJOUTER UNE ADRESSE",
                            style: TextStyle(
                              color: Colors.blueAccent,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: dSize.width * 0.0555),
              child: Divider(
                color: AppColors.marine,
                thickness: 2,
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: FutureBuilder(
                  future: LocalDatabaseManager.getAdresse(),
                  builder: (context, snap) {
                    if (snap.connectionState == ConnectionState.done) {
                      return snap.data.length != 0
                          ? Column(
                              children: [
                                for (var i = 0; i < snap.data.length; i++)
                                  Card(
                                    child: ListTile(
                                      isThreeLine: false,
                                      contentPadding: EdgeInsets.all(0),
                                      dense: true,
                                      title: Text(
                                        (snap.data[i].pays ?? '') +
                                                ', ' +
                                                snap.data[i].ville +
                                                ', ' +
                                                snap.data[i].quartier +
                                                ', ' +
                                                snap.data[i].adresse1 +
                                                ', ' +
                                                snap.data[i].adresse2 ??
                                            "",
                                        style: TextStyle(
                                          fontSize: 15,
                                        ),
                                      ),
                                      trailing: widget.calledByOrder == true
                                          ? ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty.all(
                                                  AppColors.indigo,
                                                ),
                                              ),
                                              child: Text(
                                                "CHOISIR",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16),
                                              ),
                                              onPressed: () {
                                                Navigator.pop(
                                                  context,
                                                  snap.data[i],
                                                );
                                              },
                                            )
                                          : Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                IconButton(
                                                  icon: Icon(
                                                    Icons.edit_outlined,
                                                    color: Colors.blueAccent,
                                                  ),
                                                  onPressed: () async {
                                                    bool answer =
                                                        await Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            InsertAdressPage(
                                                          lAdress: snap.data[i],
                                                        ),
                                                      ),
                                                    );
                                                    if (answer != null &&
                                                        answer) setState(() {});
                                                  },
                                                ),
                                                SizedBox(width: 3.6),
                                                IconButton(
                                                  icon: Icon(
                                                    Icons.delete,
                                                    color: Colors.redAccent,
                                                  ),
                                                  onPressed: () async {
                                                    bool doDelete = false;
                                                    await showDialog(
                                                      barrierDismissible: false,
                                                      context: context,
                                                      builder: (_) =>
                                                          AlertDialog(
                                                        title: Text(
                                                          "SUPPRESSION",
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .green),
                                                        ),
                                                        content: Text(
                                                          "Êtes vous sûr de vouloir supprimer cette adresse ?",
                                                          style: TextStyle(
                                                              fontSize: 17),
                                                        ),
                                                        actions: [
                                                          TextButton(
                                                            child: Text("NON"),
                                                            onPressed: () {
                                                              doDelete = false;
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                          ),
                                                          TextButton(
                                                            child: Text("OUI"),
                                                            onPressed: () {
                                                              doDelete = true;
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                    if (doDelete) {
                                                      LocalDatabaseManager
                                                          .deleteAdresse(
                                                              snap.data[i].id);
                                                      setState(() {});
                                                    }
                                                  },
                                                ),
                                              ],
                                            ),
                                    ),
                                  )
                              ],
                            )
                          : Center(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Text(
                                  "Aucune adresse enrégistrée pour le moment !",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                            );
                    }
                    return Center(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        color: Colors.white,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CircularProgressIndicator(),
                            SizedBox(width: 10),
                            Text("Récupération des données ..")
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class InsertAdressPage extends StatefulWidget {
  final Adresse lAdress;
  InsertAdressPage({this.lAdress});
  @override
  _InsertAdressPageState createState() => _InsertAdressPageState();
}

class _InsertAdressPageState extends State<InsertAdressPage> {
  final _formKey = GlobalKey<FormState>();
  bool showSpinner = false;
  String pays;
  String ville = "";
  String quartier = "";
  String adresse1 = "";
  String adresse2 = "";

  @override
  Widget build(BuildContext context) {
    AuthState authState = Provider.of<AuthState>(context, listen: false);
    List detailsAdresse = [
      "Ville",
      "Ville de livraison",
      "Quartier",
      "Quartier",
      "Détails sur l'adresse",
      "Détails sur l'adresse",
      "Autre précision",
      "Donnez une autre précision"
    ];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.orange,
        ),
        title: Text(
          widget.lAdress != null ? "MODIFICATION" : "AJOUT D'ADRESSE",
          style: TextStyle(
            color: Colors.orange,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(23, 15, 23, 0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 14),
                Text(
                  "PAYS",
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: AppColors.indigo,
                    fontSize: 13.0,
                  ),
                ),
                Builder(builder: (context) {
                  return DropdownButton<String>(
                    value: pays ??
                        ((widget.lAdress != null)
                            ? (widget.lAdress.pays ??
                                authState.paysNamesList[0])
                            : authState.paysNamesList[0]),
                    isExpanded: true,
                    onChanged: (String newValue) {
                      setState(() {
                        pays = newValue;
                      });
                    },
                    items: [
                      for (var i = 0; i < authState.paysNamesList.length; i++)
                        DropdownMenuItem<String>(
                          value: authState.paysNamesList[i],
                          child: Text(
                            authState.paysNamesList[i],
                          ),
                        )
                    ],
                  );
                }),
                SizedBox(height: 14),
                for (var i = 0; i < detailsAdresse.length; i = i + 2)
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(detailsAdresse[i]),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: TextFormField(
                          keyboardType: i < 4
                              ? TextInputType.name
                              : TextInputType.multiline,
                          maxLines: i < 4 ? 1 : 3,
                          textInputAction: i < 4
                              ? TextInputAction.next
                              : TextInputAction.newline,
                          initialValue: widget.lAdress != null
                              ? (i == 0
                                  ? widget.lAdress.ville
                                  : (i == 2
                                      ? widget.lAdress.quartier
                                      : (i == 4
                                          ? widget.lAdress.adresse1
                                          : widget.lAdress.adresse2)))
                              : '',
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                  color: AppColors.lightGrey, width: 0.3),
                            ),
                          ),
                          validator: (val) {
                            if (i != 6) {
                              if (val == null || val.isEmpty) {
                                return "Entrez une valeur";
                              }
                            }
                            return null;
                          },
                          onSaved: (val) {
                            switch (i) {
                              case 0:
                                setState(() {
                                  ville = val;
                                });
                                break;
                              case 2:
                                setState(() {
                                  quartier = val;
                                });
                                break;
                              case 4:
                                setState(() {
                                  adresse1 = val;
                                });
                                break;
                              case 6:
                                setState(() {
                                  adresse2 = val;
                                });
                                break;
                              default:
                            }
                            setState(() {});
                          },
                        ),
                      ),
                    ],
                  ),
                SizedBox(height: 25),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 18.0),
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                              AppColors.green,
                            ),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              setState(() {
                                showSpinner = true;
                              });
                              if (widget.lAdress != null) {
                                if (!(widget.lAdress.pays == pays &&
                                    widget.lAdress.ville == ville &&
                                    widget.lAdress.quartier == quartier &&
                                    widget.lAdress.adresse1 == adresse1 &&
                                    widget.lAdress.adresse2 == adresse2)) {
                                  LocalDatabaseManager.updateAdresse(
                                    Adresse(
                                      id: widget.lAdress.id,
                                      pays: pays,
                                      ville: ville,
                                      quartier: quartier,
                                      adresse1: adresse1,
                                      adresse2: adresse2,
                                      idFromServer: null,
                                    ),
                                  );
                                }
                              } else {
                                LocalDatabaseManager.insertAdresse(
                                  Adresse(
                                    pays: pays ?? authState.paysNamesList[0],
                                    ville: ville,
                                    quartier: quartier,
                                    adresse1: adresse1,
                                    adresse2: adresse2,
                                    idFromServer: null,
                                  ),
                                );
                              }
                              setState(() {
                                showSpinner = false;
                              });
                              Navigator.pop(context, true);
                            }
                          },
                          child: Text(
                            widget.lAdress != null ? "MODIFIER" : "VALIDER",
                            style: TextStyle(
                              fontSize: 19,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 25),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
