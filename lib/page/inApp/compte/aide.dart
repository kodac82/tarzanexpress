import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:tarzanexpress/constants/appColors.dart';
import 'package:tarzanexpress/constants/appNames.dart';
import '../../../page/inApp/about/aProposDeNous.dart';
import '../../../page/inApp/about/termAndConditions.dart';
import '../../../page/inApp/compte/agenciesInfos.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class Aide extends StatefulWidget {
  @override
  _AideState createState() => _AideState();
}

class _AideState extends State<Aide> {
  bool showSpinner = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.orange,
        ),
        title: Text(
          "Aide",
          style: TextStyle(
            color: Colors.orange,
          ),
        ),
        centerTitle: true,
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Builder(
          builder: (context) => Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                child: ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsAndConditions(),
                      ),
                    );
                  },
                  isThreeLine: false,
                  dense: true,
                  leading: Icon(
                    Icons.privacy_tip_outlined,
                    color: Colors.orange,
                  ),
                  title: Text(
                    "Conditions et Pol. de confidentialité",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                child: ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProposDeNous(),
                      ),
                    );
                  },
                  isThreeLine: false,
                  dense: true,
                  leading: Icon(
                    Icons.more_horiz,
                    color: Colors.orange,
                  ),
                  title: Text(
                    "Infos de l'application",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                child: ListTile(
                  onTap: () async {
                    setState(() {
                      showSpinner = true;
                    });
                    String _url;
                    try {
                      var uri =
                          Uri.parse(AppNames.hostUrl + AppNames.getLinkForAide);
                      var response = await http.get(
                        uri,
                        headers: {
                          "Content-Type": "application/json",
                          "Accept": "application/json"
                        },
                      );
                      //print("REPONSE EST : ${response.statusCode}");
                      //print("BODY IS : ${response.body}");
                      var a = jsonDecode(response.body);
                      if (response.statusCode.toString().startsWith("20")) {
                        _url = a['lien'];
                      }
                    } catch (e) {
                      //print("Exception lors du getLinkForAide(): $e");
                    }
                    setState(() {
                      showSpinner = false;
                    });
                    if (_url != null) {
                      await canLaunch(_url)
                          ? await launch(_url)
                          : throw 'Could not launch $_url';
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                            "Nous avons rencontré un problème, veuillez réessayer svp !",
                            textAlign: TextAlign.center,
                          ),
                          behavior: SnackBarBehavior.floating,
                          margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                        ),
                      );
                    }
                  },
                  isThreeLine: false,
                  dense: true,
                  leading: Icon(
                    Icons.help_outline,
                    color: Colors.orange,
                  ),
                  title: Text(
                    "Comment ça marche",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                child: ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AgenciesInfos(),
                      ),
                    );
                  },
                  isThreeLine: false,
                  dense: true,
                  leading: Icon(
                    Icons.location_on_outlined,
                    color: Colors.orange,
                  ),
                  title: Text(
                    "Nos agences",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
