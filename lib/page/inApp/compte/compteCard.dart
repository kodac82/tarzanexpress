import 'package:flutter/material.dart';

class CompteCard extends StatelessWidget {
  final Icon icon;
  final String title;
  final String subtitle;
  CompteCard(
      {@required this.icon, @required this.title, @required this.subtitle});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
      child: ListTile(
        isThreeLine: false,
        dense: true,
        leading: icon,
        title: Text(
          title,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
        ),
        subtitle: subtitle != null ? Text(subtitle) : null,
      ),
    );
  }
}
