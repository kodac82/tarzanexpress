import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../page/inApp/compte/changePassword.dart';
import '../../../services/localDatabase.dart';
import '../../../state/appState.dart';
import '../../../state/articleState.dart';
import '../../../state/authState.dart';
import '../../../state/insertFromPanierState.dart';
import '../../../state/messageState.dart';
import '../../../state/newOrderState.dart';
import '../../../state/notificationState.dart';
import '../../../state/orderState.dart';
import '../../../state/panierState.dart';

import '../../../constants/appColors.dart';
import '../../../page/auth/welcomePage.dart';
import '../../../page/inApp/compte/editProfile.dart';

Widget customRow(String titlePart1, String titlePart2, String trailing,
    {int flex1 = 3, int flex2 = 1}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                titlePart1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.5,
                ),
              ),
              Flexible(
                child: Text(
                  titlePart2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.5,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Informations"),
      ),
      body: FutureBuilder(
        future: SharedPreferences.getInstance(),
        builder: (context, snap) {
          if (snap.connectionState == ConnectionState.done) {
            SharedPreferences prefs = snap.data;
            String nom = prefs.getString("nom");
            String prenoms = prefs.getString("prenoms");
            String nomComplet = prefs.getString("nomComplet");
            String indicatif = prefs.getString('indicatif');
            String telephone = prefs.getString("telephone");
            String email = prefs.getString("email");
            String codeParrainage = prefs.getString("codeParrainage");
            final List _profiledata = [
              {
                "type": "",
                "title": nomComplet ?? "..",
                "trailing": "Editer",
              },
              {
                "type": "Téléphone: ",
                "title": '$telephone' != 'null'
                    ? ('+' + '$indicatif' + ' ' + telephone)
                    : "..",
                "trailing": "Editer",
              },
              {
                "type": "Mail: ",
                "title": email ?? "..",
                "trailing": "Editer",
              },
              /* {
                "type": "Code parrainage: ",
                "title": codeParrainage ?? "..",
                "trailing": "Copier",
              }, */
            ];
            return SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      padding: EdgeInsets.all(7.2),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.orange,
                          border: Border.all(color: Colors.black, width: 1)),
                      child: Icon(
                        Icons.person,
                        size: 43,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(14, 10, 14, 10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        for (var i = 0; i < _profiledata.length; i++)
                          Column(
                            children: [
                              customRow(
                                _profiledata[i]["type"],
                                _profiledata[i]["title"],
                                _profiledata[i]["trailing"],
                              ),
                              (i != 3) ? Divider() : Container(),
                            ],
                          ),
                        Text(
                          "Code parrainage: ",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.5,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              codeParrainage ?? "..",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.5,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            OutlinedButton(
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) => Colors.blue,
                                ),
                              ),
                              onPressed: () {
                                Clipboard.setData(
                                  new ClipboardData(text: codeParrainage ?? ""),
                                );
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      "Le code a été copié.",
                                      textAlign: TextAlign.center,
                                    ),
                                    behavior: SnackBarBehavior.floating,
                                    margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                  ),
                                );
                              },
                              child: Text(
                                "Copier",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.5,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Filleuls :",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16.0),
                                ),
                                SizedBox(
                                  width: 18,
                                ),
                                FutureBuilder(
                                    future:
                                        DataConnectionChecker().hasConnection,
                                    builder: (context, snaps) {
                                      if ((snaps.connectionState ==
                                              ConnectionState.done) &&
                                          snaps.data) {
                                        return FutureBuilder(
                                          future: Provider.of<AuthState>(
                                                  context,
                                                  listen: false)
                                              .countFilleul(context: context),
                                          builder: (context, snapshot) {
                                            if (snapshot.connectionState ==
                                                ConnectionState.done) {
                                              return Text(
                                                (snapshot.data[0]
                                                    ? snapshot.data[1]
                                                        .toString()
                                                    : ".."),
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              );
                                            }
                                            return CircularProgressIndicator();
                                          },
                                        );
                                      }
                                      return Text(
                                        "..",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      );
                                    }),
                              ],
                            ),
                            OutlinedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.blueAccent,
                                ),
                              ),
                              onPressed: () {
                                setState(() {});
                              },
                              child: Text(
                                "ACTUALISER",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 35),
                  Center(
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EditProfile(
                                  nom: nom,
                                  prenoms: prenoms,
                                  telephone: telephone,
                                  email: email,
                                ),
                              ),
                            );
                          },
                          child: Text(
                            "Éditer le profil",
                            style: TextStyle(
                              fontSize: 20,
                              color: AppColors.indigo,
                            ),
                          ),
                        ),
                        SizedBox(height: 15),
                        GestureDetector(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ChangePassword(),
                              )),
                          child: Text(
                            "Changer de mot de passe",
                            style: TextStyle(
                              fontSize: 20,
                              color: AppColors.green,
                            ),
                          ),
                        ),
                        SizedBox(height: 15),
                        GestureDetector(
                          onTap: () async {
                            SharedPreferences sPref =
                                await SharedPreferences.getInstance();
                            await sPref.clear();
                            await LocalDatabaseManager.database
                                .delete('panier', where: '1');
                            Provider.of<AppState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<ArticleState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<AuthState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<InsertFromPanierState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<MessageState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<NewOrderState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<NotificationState>(
                              context,
                              listen: false,
                            ).clear();

                            Provider.of<OrderState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<PanierState>(
                              context,
                              listen: false,
                            ).clear();
                            Provider.of<MessageState>(
                              context,
                              listen: false,
                            ).clearSAV();
                            /* Provider.of<TransactionState>(
                              context,
                              listen: false,
                            ).clear(); */
                            await sPref.setBool("isFreshlyInstalled", false);
                            await sPref.setBool("isLoggedIn", false);
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                  builder: (context) => WelcomePage(),
                                ),
                                (Route<dynamic> route) => false);
                          },
                          child: Text(
                            "Déconneter ce compte",
                            style: TextStyle(
                              fontSize: 20,
                              color: AppColors.red,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                ],
              ),
            );
          }
          return Center(
            child: Container(
              padding: EdgeInsets.all(10),
              color: Colors.white,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 10),
                  Text("Récupération des données ..")
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
