import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tarzanexpress/page/inApp/compte/debtDetails.dart';
import 'package:tarzanexpress/state/transactionState.dart';
import '../../../constants/appAssets.dart';
import '../../../constants/appNames.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../page/inApp/commandes/filteredOrders.dart';
import '../../../page/inApp/commandes/payerRecap.dart';
import '../../../page/inApp/compte/userAdresses.dart';
import '../../../page/inApp/serviceClient/serviceClient.dart';
import '../../../state/orderState.dart';
import '../../../widgets/customWidgets.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../constants/appColors.dart';
import '../../../page/inApp/compte/profilePage.dart';
import 'aide.dart';
import 'historiqueTransactions.dart';

class Compte extends StatefulWidget {
  @override
  _CompteState createState() => _CompteState();
}

class _CompteState extends State<Compte> {
  String methode;
  String suggestionText;

  void _launchWhatsApp() async {
    String nomClient =
        (await SharedPreferences.getInstance()).getString('nomComplet');
    final String textToSend =
        "Bonjour/Bonsoir, Je suis le client $nomClient. J'aimerais des informations concernant ...";
    String _url =
        "https://wa.me/22870222929?text=${Uri.encodeComponent(textToSend)}";
    await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
  }

  void selectPaymentType({
    @required BuildContext context,
  }) {
    methode = null;
    showModalBottomSheet<void>(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32),
            topRight: Radius.circular(32),
          ),
        ),
        backgroundColor: Colors.white,
        builder: (BuildContext context) {
          List zContent = [
            AppAssets.logoFlooz,
            AppAssets.logoTMoney,
          ];
          List zContentText = [
            AppNames.methodePaiementFLOOZ,
            AppNames.methodePaiementTMONEY,
          ];
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Recharger par :",
                  style: TextStyle(
                    fontSize: 17,
                    color: Colors.amber[800],
                  ),
                ),
                SizedBox(height: 7),
                for (var i = 0; i < zContent.length; i++)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 7.5),
                    child: GestureDetector(
                      onTap: () {
                        methode = zContentText[i];
                        //print("METHODE == $methode");
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PayerRecap(
                                commandeId: null,
                                montantApayer: null,
                                method: methode,
                                reference: null,
                                type: null),
                          ),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 18,
                            width: 18,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border:
                                  Border.all(width: 2, color: AppColors.indigo),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(4.0),
                            margin: const EdgeInsets.only(left: 8.0),
                            decoration: BoxDecoration(
                              color: zContentText[i] ==
                                      AppNames.methodePaiementFLOOZ
                                  ? Colors.black
                                  : Colors.yellowAccent[700],
                              borderRadius: BorderRadius.all(
                                Radius.circular(5),
                              ),
                            ),
                            child: Image.asset(
                              zContent[i],
                              height: 45,
                              width: 75,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                SizedBox(height: 25)
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    List typesCommandes = [
      Icons.account_balance_wallet_outlined,
      "Non-payées",
      Icons.wallet_giftcard_outlined,
      "En attente d'expédition",
      Icons.directions_bus_outlined,
      "Expédiées",
      Icons.cancel_outlined,
      "Annulées",
      Icons.info_outline,
      "En attente d'avis",
      Icons.done,
      "Terminées"
    ];
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () {
          return Future.delayed(
            Duration(milliseconds: 1),
            () {
              setState(() {});
            },
          );
        },
        child: Builder(
          builder: (context) => SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.fromLTRB(dSize.width * 0.03,
                dSize.width * 0.039, dSize.width * 0.03, dSize.width * 0.04),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(dSize.width * 0.02),
                  width: fullWidth(context) - 21,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    //color: Colors.green.shade400
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [Colors.green.shade300, Colors.green.shade600],
                    ),
                  ),
                  child: Row(
                    children: [
                      Flexible(
                        child: Row(
                          //mainAxisSize: MainAxisSize.min,
                          children: [
                            Column(
                              children: [
                                Text(
                                  "Solde (XOF)",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15.0,
                                  ),
                                ),
                                FutureBuilder(
                                  future: DataConnectionChecker().hasConnection,
                                  builder: (context, snapy) {
                                    if ((snapy.connectionState ==
                                            ConnectionState.done) &&
                                        snapy.data) {
                                      return FutureBuilder(
                                        future: Provider.of<OrderState>(context,
                                                listen: false)
                                            .getPortefeuille(context: context),
                                        builder: (context, snapshot) {
                                          if (snapshot.connectionState ==
                                              ConnectionState.done) {
                                            if (snapshot.data[0]) {
                                              return Text(
                                                snapshot.data[1].toString(),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 17.0,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: 2.0),
                                              );
                                            } else {
                                              return Text(
                                                "..",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15.0,
                                                    fontWeight: FontWeight.bold,
                                                    letterSpacing: 2.0),
                                              );
                                            }
                                          }
                                          return CircularProgressIndicator();
                                        },
                                      );
                                    }
                                    return Text(
                                      "..",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    );
                                  },
                                ),
                                FutureBuilder(
                                  future: DataConnectionChecker().hasConnection,
                                  builder: (context, snapy) {
                                    if ((snapy.connectionState ==
                                            ConnectionState.done) &&
                                        snapy.data) {
                                      return FutureBuilder(
                                        future: Provider.of<TransactionState>(
                                                context,
                                                listen: false)
                                            .debts(context: context),
                                        builder: (context, snapshot) {
                                          if (snapshot.connectionState ==
                                              ConnectionState.done) {
                                            if (snapshot.data[0]) {
                                              return (['null', '0', '00']
                                                      .contains(snapshot.data[1]
                                                          .toString()))
                                                  ? Container()
                                                  : Column(
                                                      children: [
                                                        Text(
                                                          "Impayés (XOF)",
                                                          style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 15.0,
                                                          ),
                                                        ),
                                                        ElevatedButton(
                                                          onPressed: () {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        DebtDetails(),
                                                              ),
                                                            );
                                                          },
                                                          style: ButtonStyle(
                                                            backgroundColor:
                                                                MaterialStateProperty
                                                                    .all(Colors
                                                                        .yellow),
                                                            elevation:
                                                                MaterialStateProperty
                                                                    .all(16.0),
                                                          ),
                                                          child: Text(
                                                            snapshot.data[1]
                                                                .toString(),
                                                            style: TextStyle(
                                                              color: Colors
                                                                  .redAccent,
                                                              fontSize: 17.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              letterSpacing:
                                                                  2.0,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    );
                                            } else {
                                              return Container();
                                            }
                                          }
                                          return Container();
                                        },
                                      );
                                    }
                                    return Container();
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HistoriquePortefeuille(),
                            ),
                          );
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                              Icons.history_outlined,
                              size: 36,
                              color: Colors.white,
                            ),
                            Text(
                              "Historique",
                              style: TextStyle(
                                fontSize: 13.0,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 9),
                      GestureDetector(
                        onTap: () {
                          selectPaymentType(
                            context: context,
                          );
                        },
                        child: Column(
                          children: [
                            Icon(
                              Icons.add,
                              size: 36,
                              color: Colors.white,
                            ),
                            Text(
                              "Recharger",
                              style: TextStyle(
                                fontSize: 13.0,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: dSize.height * 0.019943),
                Card(
                  elevation: 4,
                  margin: EdgeInsets.all(0),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: dSize.width * 0.010,
                        vertical: dSize.height * 0.0099715),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: dSize.height * 0.014245),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Commandes",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              GestureDetector(
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => FilteredOrders(
                                        title: "MES COMMANDES",
                                        filterList: null),
                                  ),
                                ),
                                child: Text(
                                  "Tout voir >",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.blueAccent),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(thickness: 1),
                        Center(
                          child: Wrap(
                            spacing: 8,
                            runSpacing: 8,
                            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              for (var i = 0;
                                  i < typesCommandes.length;
                                  i = i + 2)
                                GestureDetector(
                                  onTap: () {
                                    Map<int, Widget> tempFilters = {
                                      0: FilteredOrders(
                                        title: "NON PAYÉES",
                                        filterList: ['1', '3'],
                                      ),
                                      1: FilteredOrders(
                                        title: "EN ATTENTE D'EXPÉDITION",
                                        filterList: ['2'],
                                      ),
                                      2: FilteredOrders(
                                        title: "EXPÉDIÉES",
                                        filterList: ['4'],
                                      ),
                                      3: FilteredOrders(
                                        title: "ANNULÉES",
                                        filterList: ['0', '5', '6'],
                                      ),
                                      4: FilteredOrders(
                                        title: "EN ATTENTE D'AVIS",
                                        filterList: ['9', '10'],
                                      ),
                                      5: FilteredOrders(
                                        title: "TERMINÉES",
                                        filterList: ['7'],
                                      ),
                                    };
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            tempFilters[i / 2],
                                      ),
                                    );
                                  },
                                  child: Container(
                                    //height: dSize.height * 0.114,
                                    width: dSize.width * 0.27,
                                    child: Wrap(
                                      alignment: WrapAlignment.center,
                                      children: [
                                        Icon(
                                          typesCommandes[i],
                                          color: Colors.orange,
                                          size: 27,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Flexible(
                                              child: Text(
                                                typesCommandes[i + 1],
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProfilePage(),
                    ),
                  ),
                  child: Card(
                    elevation: 4,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.person,
                                size: 25,
                                color: Colors.orange,
                              ),
                              SizedBox(width: 7.2),
                              Text(
                                "Mes Informations",
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ],
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 23,
                            color: Colors.orange,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 8),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => UserAdresses(),
                      ),
                    );
                  },
                  child: Card(
                    elevation: 4,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(
                                Icons.location_on,
                                size: 25,
                                color: Colors.orange,
                              ),
                              SizedBox(width: 7.2),
                              Text(
                                "Mes adresses",
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ],
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 23,
                            color: Colors.orange,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: () {
                        _launchWhatsApp();
                      },
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(7.2),
                          child: Column(
                            children: [
                              Image.asset(
                                "assets/images/whatsapp.png",
                                height: 21,
                                width: 21,
                                color: Colors.black,
                              ),
                              Text(
                                "WhatsApp",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ServiceClient(),
                          ),
                        );
                      },
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(7.2),
                          child: Column(
                            children: [
                              Icon(
                                Icons.support_agent_outlined,
                                color: Colors.black,
                                size: 21,
                              ),
                              Text(
                                "Service Client",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Aide(),
                          ),
                        );
                      },
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(7.2),
                          child: Column(
                            children: [
                              Icon(
                                Icons.info_outline,
                                color: Colors.black,
                                size: 21,
                              ),
                              Text(
                                "TarzanExpress",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
