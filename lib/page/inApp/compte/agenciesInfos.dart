import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../state/newOrderState.dart';
import '../../../widgets/customWidgets.dart';

class AgenciesInfos extends StatefulWidget {
  @override
  _AgenciesInfosState createState() => _AgenciesInfosState();
}

class _AgenciesInfosState extends State<AgenciesInfos> {
  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Nos agences",
        ),
      ),
      body: FutureBuilder(
        future: DataConnectionChecker().hasConnection,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data) {
              return FutureBuilder(
                future: Provider.of<NewOrderState>(context, listen: false)
                    .getModesLivraison(
                  context: context,
                  decompose: false,
                ),
                builder: (context, snap) {
                  if (snap.connectionState == ConnectionState.done) {
                    if (snap.data[0]) {
                      return SingleChildScrollView(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Nos agences sont ouvertes de 08h à 19h (UTC).",
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(height: dSize.height * 0.03),
                            Builder(
                              builder: (context) {
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    for (var i = 0;
                                        i < snap.data[1].length;
                                        i++)
                                      if (snap.data[1][i]['villes'].length != 0)
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Container(
                                                  height: 10,
                                                  width: 10,
                                                  margin:
                                                      EdgeInsets.only(right: 5),
                                                  decoration: BoxDecoration(
                                                    color: Colors.orange,
                                                    shape: BoxShape.circle,
                                                  ),
                                                ),
                                                Text(
                                                  snap.data[1][i]['nom'],
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left: dSize.width * 0.03),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  for (var j = 0;
                                                      j <
                                                          snap
                                                              .data[1][i]
                                                                  ['villes']
                                                              .length;
                                                      j++)
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        for (var k = 0;
                                                            k <
                                                                snap
                                                                    .data[1][i][
                                                                        'villes']
                                                                        [j][
                                                                        'agences']
                                                                    .length;
                                                            k++)
                                                          Card(
                                                            elevation: 2,
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .all(
                                                                      15.0),
                                                              child: Row(
                                                                children: [
                                                                  Flexible(
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          "Agence " +
                                                                              snap.data[1][i]['villes'][j]['agences'][k]['nom'],
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                16,
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          snap.data[1][i]['villes'][j]['nom'] +
                                                                              ", ${snap.data[1][i]['villes'][j]['agences'][k]['adresse']['quartier'] ?? ""}, ${snap.data[1][i]['villes'][j]['agences'][k]['adresse']['adresse1'] ?? ""}, ${snap.data[1][i]['villes'][j]['agences'][k]['adresse']['adresse2'] ?? ""}",
                                                                          style:
                                                                              TextStyle(fontSize: 16),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                      ],
                                                    ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Divider(
                                                color: Colors.grey,
                                                thickness: 2,
                                              ),
                                            )
                                          ],
                                        ),
                                  ],
                                );
                              },
                            ),
                          ],
                        ),
                      );
                    }
                    return Container(
                      height: fullHeight(context),
                      width: fullWidth(context),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(Icons.link_off),
                              Text(
                                "Une erreur s'est produite. Notre équipe est en train de résoudre le problème. Veuillez réessayer plutard.",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                  return Container(
                    height: fullHeight(context),
                    width: fullWidth(context),
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        color: Colors.white,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CircularProgressIndicator(),
                            SizedBox(width: 10),
                            Text("Récupération des données ..")
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            }
            return Container(
              height: fullHeight(context),
              width: fullWidth(context),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.wifi_off),
                      Text(
                        "Connection internet faible ou inexistante.",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                      ElevatedButton(
                        onPressed: () => setState(() {}),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Colors.blue,
                          ),
                        ),
                        child: Text(
                          "RÉESSAYER",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
          return Container(
            height: fullHeight(context),
            width: fullWidth(context),
            child: Center(
              child: Container(
                padding: EdgeInsets.all(10),
                color: Colors.white,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(width: 10),
                    Text("Récupération des données ..")
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
