import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../constants/appColors.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../state/messageState.dart';
import '../../../widgets/customWidgets.dart';

class ServiceClient extends StatefulWidget {
  final bool getQuote;
  ServiceClient({this.getQuote});
  @override
  _ServiceClientState createState() => _ServiceClientState();
}

class _ServiceClientState extends State<ServiceClient> {
  final _formKey = GlobalKey<FormState>();
  String contenu;
  ValueNotifier<bool> showPopUp = ValueNotifier(true);

  void _launchWhatsApp() async {
    String nomClient =
        (await SharedPreferences.getInstance()).getString('nomComplet');
    final String textToSend =
        "Bonjour/Bonsoir, Je suis le client $nomClient. J'aimerais des " +
            (((widget.getQuote != null) == widget.getQuote)
                ? "avoir une côtation pour l'article"
                : "informations concernant") +
            " ...";
    String _url =
        "https://wa.me/22870222929?text=${Uri.encodeComponent(textToSend)}";
    await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
  }

  List<String> monthString = [
    "JANVIER",
    "FÉVRIER",
    "MARS",
    "AVRIL",
    "MAI",
    "JUIN",
    "JUILLET",
    "AOÛT",
    "SEPTEMBRE",
    "OCTOBRE",
    "NOVEMBRE",
    "DÉCEMBRE",
  ];

  Future<File> handleChooseFromGallery() async {
    return File(
        (await ImagePicker().getImage(source: ImageSource.gallery)).path);
  }

  @override
  void initState() {
    SharedPreferences.getInstance().then((mPrefs) {
      mPrefs.setBool("isInsideSAV", true);
    });
    super.initState();
  }

  @override
  void dispose() {
    SharedPreferences.getInstance().then((mPrefs) {
      mPrefs.remove("isInsideSAV");
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MessageState b = Provider.of<MessageState>(context, listen: false);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (showPopUp.value && ((widget.getQuote != null) == widget.getQuote)) {
        showDialog(
          context: context,
          builder: (context) => SimpleDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
            ),
            contentPadding: EdgeInsets.fromLTRB(12, 35, 12, 21),
            children: <Widget>[
              Text(
                "Pour avoir un devis, envoyez un lien/une image et une description à notre service client.",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14.0),
              ),
              SizedBox(
                height: 7,
              ),
              Center(
                child: TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.orange),
                  ),
                ),
              )
            ],
          ),
        );
        showPopUp.value = false;
      }
    });
    return Scaffold(
      backgroundColor: AppColors.scaffoldBackground,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, true),
        ),
        title: Text("SERVICE CLIENT"),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 6.0),
            child: GestureDetector(
              onTap: () {
                _launchWhatsApp();
              },
              child: Container(
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: Colors.green,
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/images/whatsapp.png",
                  height: 25,
                  width: 25,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
      body: FutureBuilder(
        future: DataConnectionChecker().hasConnection,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (!snapshot.data) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.wifi_off),
                      Text(
                        "Connection internet faible ou inexistante.",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                      ElevatedButton(
                        onPressed: () => setState(() {}),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Colors.blue,
                          ),
                        ),
                        child: Text(
                          "RÉESSAYER",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            } else {
              return FutureBuilder(
                future: (b.messagesSAV != null)
                    ? Future.delayed(Duration(milliseconds: 1))
                    : b.getMessagesSAV(
                        context: context,
                      ),
                builder: (context, snap) {
                  if (snap.connectionState == ConnectionState.done) {
                    return Consumer<MessageState>(
                      builder: (context, a, _) {
                        DateTime tempDate = DateTime.parse("1900-01-01");
                        compareDateToPrevious(String tDateString) {
                          tDateString = tDateString.replaceAll('/', '-');
                          DateTime tDate = DateTime.parse(tDateString);
                          tDate = DateTime(tDate.year, tDate.month, tDate.day);
                          if (!(tDate.compareTo(tempDate) < 1)) {
                            tempDate = tDate;
                            return true;
                          }
                          return false;
                        }

                        if (a.messagesSAV == null) {
                          return Center(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.link_off),
                                  Text(
                                    "Une erreur s'est produite. Notre équipe est en train de résoudre le problème. Veuillez réessayer plutard.",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 20),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: (a.messagesSAV.length != 0)
                                  ? SingleChildScrollView(
                                      reverse: true,
                                      physics: ScrollPhysics(),
                                      padding:
                                          EdgeInsets.fromLTRB(15, 15, 15, 0),
                                      child: Column(
                                        children: [
                                          for (var i = 0;
                                              i < a.messagesSAV.length;
                                              i++)
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5),
                                              child: Column(
                                                children: [
                                                  Center(
                                                    child:
                                                        !compareDateToPrevious(a
                                                                .messagesSAV[i]
                                                                .date)
                                                            ? Container()
                                                            : Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        8),
                                                                child:
                                                                    Container(
                                                                  padding: EdgeInsets
                                                                      .symmetric(
                                                                          vertical:
                                                                              5,
                                                                          horizontal:
                                                                              5),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: AppColors
                                                                        .yellowAppbar,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            10),
                                                                  ),
                                                                  child: Text(
                                                                    tempDate.day
                                                                            .toString() +
                                                                        ' ' +
                                                                        monthString[tempDate.month -
                                                                            1] +
                                                                        ' ' +
                                                                        tempDate
                                                                            .year
                                                                            .toString(),
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            17,
                                                                        fontWeight:
                                                                            FontWeight.w500),
                                                                  ),
                                                                ),
                                                              ),
                                                  ),
                                                  Column(
                                                    crossAxisAlignment: (a
                                                                .messagesSAV[i]
                                                                .isAdmin ==
                                                            "1")
                                                        ? CrossAxisAlignment
                                                            .start
                                                        : CrossAxisAlignment
                                                            .end,
                                                    children: [
                                                      (a.messagesSAV[i]
                                                                  .isAdmin ==
                                                              "1")
                                                          ? Row(
                                                              children: [
                                                                Text(
                                                                  "TarzanExpress ADMIN",
                                                                  style:
                                                                      TextStyle(
                                                                    color: AppColors
                                                                        .drawerTitle,
                                                                    fontSize:
                                                                        14,
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          : Container(),
                                                      (a.messagesSAV[i].image !=
                                                                  "null" &&
                                                              a.messagesSAV[i]
                                                                      .image !=
                                                                  null)
                                                          ? Row(
                                                              mainAxisAlignment: (a
                                                                          .messagesSAV[
                                                                              i]
                                                                          .isAdmin ==
                                                                      "1")
                                                                  ? MainAxisAlignment
                                                                      .start
                                                                  : MainAxisAlignment
                                                                      .end,
                                                              children: [
                                                                GestureDetector(
                                                                  onTap: () =>
                                                                      Navigator
                                                                          .push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              ViewCachedImageMessage(
                                                                        imageUrl: a
                                                                            .messagesSAV[i]
                                                                            .image,
                                                                        sender: a
                                                                            .messagesSAV[i]
                                                                            .isAdmin,
                                                                        date: a
                                                                            .messagesSAV[i]
                                                                            .date,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  child:
                                                                      Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      color: Colors
                                                                          .white,
                                                                      borderRadius:
                                                                          BorderRadius.all(
                                                                              Radius.circular(12)),
                                                                    ),
                                                                    child:
                                                                        SizedBox(
                                                                      height: MediaQuery.of(context)
                                                                              .size
                                                                              .height *
                                                                          0.2849,
                                                                      width: MediaQuery.of(context)
                                                                              .size
                                                                              .width *
                                                                          0.55555,
                                                                      child:
                                                                          CachedNetworkImage(
                                                                        imageUrl: a.messagesSAV[i].image.startsWith("http")
                                                                            ? a.messagesSAV[i].image
                                                                            : "https://" + a.messagesSAV[i].image,
                                                                        progressIndicatorBuilder: (context,
                                                                            url,
                                                                            downloadProgress) {
                                                                          return Center(
                                                                            child:
                                                                                CircularProgressIndicator(
                                                                              value: downloadProgress.progress,
                                                                            ),
                                                                          );
                                                                        },
                                                                        errorWidget: (context,
                                                                                url,
                                                                                error) =>
                                                                            Icon(Icons.error),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          : Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                left: (((a.messagesSAV[i]
                                                                            .isAdmin ==
                                                                        "1")
                                                                    ? 0
                                                                    : 40)),
                                                                right: (((a.messagesSAV[i]
                                                                            .isAdmin ==
                                                                        "1")
                                                                    ? 40
                                                                    : 0)),
                                                              ),
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(12),
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          10.0),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          10.0),
                                                                  bottomLeft: (a
                                                                              .messagesSAV[
                                                                                  i]
                                                                              .isAdmin ==
                                                                          "1")
                                                                      ? Radius
                                                                          .circular(
                                                                              0.0)
                                                                      : Radius.circular(
                                                                          10.0),
                                                                  bottomRight: (a
                                                                              .messagesSAV[
                                                                                  i]
                                                                              .isAdmin ==
                                                                          "1")
                                                                      ? Radius
                                                                          .circular(
                                                                              10.0)
                                                                      : Radius
                                                                          .circular(
                                                                              0.0),
                                                                ),
                                                                color: (a
                                                                            .messagesSAV[
                                                                                i]
                                                                            .isAdmin ==
                                                                        "1")
                                                                    ? AppColors
                                                                        .imageUploadBackgroundBorder
                                                                    : AppColors
                                                                        .indigo,
                                                              ),
                                                              child: Text(
                                                                a.messagesSAV[i]
                                                                    .contenu,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 16,
                                                                  color: (a.messagesSAV[i].isAdmin ==
                                                                          "1")
                                                                      ? Colors
                                                                          .black
                                                                      : Colors
                                                                          .white,
                                                                ),
                                                              ),
                                                            ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        mainAxisAlignment: (a
                                                                    .messagesSAV[
                                                                        i]
                                                                    .isAdmin ==
                                                                "1")
                                                            ? MainAxisAlignment
                                                                .start
                                                            : MainAxisAlignment
                                                                .end,
                                                        children: [
                                                          Text(
                                                            a.messagesSAV[i].date.substring(
                                                                a.messagesSAV[i]
                                                                    .date
                                                                    .indexOf(
                                                                        ' '),
                                                                a.messagesSAV[i]
                                                                    .date
                                                                    .lastIndexOf(
                                                                        ':')),
                                                            style: TextStyle(
                                                                fontSize: 15,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          Visibility(
                                            visible: a.isSendingSAV,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Container(
                                                        padding:
                                                            EdgeInsets.all(12),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    10.0),
                                                            topRight:
                                                                Radius.circular(
                                                                    10.0),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    10.0),
                                                            bottomRight:
                                                                Radius.circular(
                                                                    0.0),
                                                          ),
                                                          color: Colors.white,
                                                        ),
                                                        child: Text(
                                                          "En cours d'envoi..",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              color:
                                                                  Colors.black),
                                                        ),
                                                      ),
                                                      SizedBox(height: 5),
                                                      Text(""),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 25,
                                          ),
                                        ],
                                      ),
                                    )
                                  : Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              (widget.getQuote != null)
                                                  ? "Envoyez l'image ou le lien d'un article et nous vous fournirons une estimation de la commande de cet article."
                                                  : "Obtenez de l'aide, une côtation ou faites nous part de vos suggestions. Nous serons ravi de prendre en charge vos préoccupations.",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontSize: 16),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                _launchWhatsApp();
                                              },
                                              child: Card(
                                                elevation: 4,
                                                child: Padding(
                                                  padding: EdgeInsets.all(7.2),
                                                  child: Column(
                                                    children: [
                                                      Image.asset(
                                                        "assets/images/whatsapp.png",
                                                        height: 15,
                                                        width: 15,
                                                        color: Colors.green,
                                                      ),
                                                      Text(
                                                        "WhatsApp",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color:
                                                                Colors.green),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                            Form(
                              key: _formKey,
                              child: Container(
                                color: Colors.white,
                                padding: EdgeInsets.fromLTRB(8, 10, 8, 10),
                                child: Row(
                                  children: [
                                    IconButton(
                                      icon: Icon(Icons.photo_camera),
                                      onPressed: () async {
                                        File image =
                                            await handleChooseFromGallery();
                                        if (image != null) {
                                          await a.postMessageSAV(
                                            context: context,
                                            image: image,
                                            contenu: null,
                                          );
                                        }
                                      },
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                      child: TextFormField(
                                        keyboardType: TextInputType.text,
                                        textInputAction:
                                            TextInputAction.newline,
                                        decoration: InputDecoration(
                                          hintText: "Message",
                                          contentPadding: EdgeInsets.fromLTRB(
                                              10, 10, 10, 10),
                                        ),
                                        validator: (val) {
                                          if (val == null || val.isEmpty) {
                                            return " ";
                                          }
                                          return null;
                                        },
                                        onSaved: (val) {
                                          contenu = val;
                                        },
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: AppColors.green,
                                        shape: BoxShape.circle,
                                      ),
                                      child: IconButton(
                                        onPressed: () async {
                                          if (_formKey.currentState
                                              .validate()) {
                                            _formKey.currentState.save();
                                            if (await DataConnectionChecker()
                                                .hasConnection) {
                                              _formKey.currentState.reset();
                                              await a.postMessageSAV(
                                                context: context,
                                                contenu: contenu,
                                                image: null,
                                              );
                                            } else {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                SnackBar(
                                                  content: Text(
                                                    "Assurez vous d'avoir une bonne connection et réessayez",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  behavior:
                                                      SnackBarBehavior.floating,
                                                  margin: EdgeInsets.fromLTRB(
                                                      15, 0, 15, 30),
                                                ),
                                              );
                                            }
                                          }
                                        },
                                        icon: Icon(
                                          Icons.send,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        );
                      },
                    );
                  }
                  return Center(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      color: Colors.white,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(width: 10),
                          Text("Récupération des données ..")
                        ],
                      ),
                    ),
                  );
                },
              );
            }
          }
          return Center(
            child: Container(
              padding: EdgeInsets.all(10),
              color: Colors.white,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 10),
                  Text("Récupération des données..")
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class ViewCachedImageMessage extends StatelessWidget {
  final String imageUrl;
  final String sender;
  final String date;
  ViewCachedImageMessage({
    @required this.imageUrl,
    @required this.sender,
    @required this.date,
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.yellowAppbar,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              sender == "1" ? "TarzanExpress ADMIN" : "Vous",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              date,
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
      ),
      body: Container(
        height: fullHeight(context),
        width: fullWidth(context),
        child: CachedNetworkImage(
          imageUrl:
              imageUrl.startsWith("http") ? imageUrl : "http://" + imageUrl,
          fit: BoxFit.contain,
          progressIndicatorBuilder: (context, url, downloadProgress) {
            return Center(
              child: CircularProgressIndicator(
                value: downloadProgress.progress,
              ),
            );
          },
        ),
      ),
    );
  }
}
