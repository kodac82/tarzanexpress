import 'dart:convert';
import 'package:flutter/material.dart';
import '../../../widgets/customWidgets.dart';

class ShowImage extends StatelessWidget {
  final String imageBase64;
  ShowImage({@required this.imageBase64});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.orange,
        ),
        title: Text(
          "IMAGE",
          style: TextStyle(
            color: Colors.orange,
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        height: fullHeight(context),
        width: fullWidth(context),
        child: Image.memory(
          base64Decode(imageBase64),
          height: fullHeight(context),
          width: fullWidth(context),
        ),
      ),
    );
  }
}
