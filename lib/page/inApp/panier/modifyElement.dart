import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../../../constants/appColors.dart';
import '../../../state/panierState.dart';

class ModifyStoredCartElement extends StatefulWidget {
  final String productUrl;
  final String productName;
  final String productDescription;
  final int index;
  final PanierState a;
  ModifyStoredCartElement({
    @required this.productName,
    @required this.productDescription,
    @required this.productUrl,
    @required this.index,
    @required this.a,
  });
  @override
  _ModifyStoredCartElementState createState() =>
      _ModifyStoredCartElementState();
}

class _ModifyStoredCartElementState extends State<ModifyStoredCartElement> {
  final _formKey = GlobalKey<FormState>();
  String productUrl = "";
  String productName = "";
  String productDescription = "";
  String productQuantite = "";
  File image;

  handleChooseFromGallery() async {
    File tempFile =
        File((await ImagePicker().getImage(source: ImageSource.gallery)).path);
    if (tempFile != null) {
      setState(() {
        image = tempFile;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.only(left: 5),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(25)),
      ),
      children: <Widget>[
        Form(
          key: _formKey,
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            child: Theme(
              data: new ThemeData(
                primaryColor: AppColors.indigo,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ///
                  ///NOM DU PRODUIT
                  ///
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Nom",
                          style: TextStyle(color: AppColors.indigo),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.name,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          initialValue:
                              widget.a.contentPanier[widget.index].nom,
                          decoration: InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500),
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                  color: AppColors.lightGrey, width: 0.3),
                            ),
                          ),
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              return "N'oubliez pas le nom";
                            }
                            if (val.length < 3) {
                              return "Entrez un nom valide";
                            }
                            return null;
                          },
                          onSaved: (val) {
                            productName = val;
                          },
                        ),
                      ],
                    ),
                  ),

                  ///
                  ///DESCRIPTION
                  ///
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Description",
                          style: TextStyle(color: AppColors.indigo),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.text,
                          maxLines: 5,
                          textInputAction: TextInputAction.next,
                          initialValue:
                              widget.a.contentPanier[widget.index].description,
                          decoration: InputDecoration(
                            hintText:
                                "Précisez votre préference (Couleur, Taille, ...)",
                            hintStyle: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500),
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                  color: AppColors.lightGrey, width: 0.3),
                            ),
                          ),
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              return "N'oubliez pas la description";
                            }
                            if (val.length < 4) {
                              return "Entrez au moins quatre lettres";
                            }
                            return null;
                          },
                          onSaved: (val) {
                            productDescription = val;
                          },
                        ),
                      ],
                    ),
                  ),

                  ///
                  ///QUANTITE
                  ///
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Quantité",
                          style: TextStyle(color: AppColors.indigo),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          initialValue: widget
                              .a.contentPanier[widget.index].quantite
                              .toString(),
                          decoration: InputDecoration(
                            hintText: "Quantité",
                            hintStyle: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500),
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                  color: AppColors.lightGrey, width: 0.3),
                            ),
                          ),
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              return "N'oubliez pas la quantité";
                            }
                            if (val.contains('-') ||
                                val.contains(',') ||
                                val.contains(' ') ||
                                val.contains('.')) {
                              return "Entrez un nombre sans espaces ni <, . ->";
                            }
                            if (int.parse(val) == 0) {
                              return "La quantité ne doit pas être 0";
                            }
                            return null;
                          },
                          onSaved: (val) {
                            productQuantite = val;
                          },
                        ),
                      ],
                    ),
                  ),

                  ///
                  ///IMAGE
                  ///
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Image",
                          style: TextStyle(color: AppColors.indigo),
                        ),
                        Visibility(
                          visible: image == null &&
                              widget.a.contentPanier[widget.index].image ==
                                  null,
                          child: Text(
                            "Il est conseillé d'ajouter un screenshot de la page web montrant le produit",
                            style: TextStyle(color: AppColors.yellowAppbar),
                          ),
                        ),
                        SizedBox(height: 8),
                        GestureDetector(
                          onTap: () {
                            handleChooseFromGallery();
                          },
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: AppColors.imageUploadBackground
                                      .withOpacity(0.2),
                                  border: Border.all(
                                      color: AppColors
                                          .imageUploadBackgroundBorder),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                ),
                                child: SizedBox(
                                  height: 50,
                                  width: 50,
                                  child: image != null
                                      ? Image.file(
                                          image,
                                          fit: BoxFit.cover,
                                        )
                                      : (![null, 'null'].contains(widget
                                              .a
                                              .contentPanier[widget.index]
                                              .image)
                                          ? Image.memory(
                                              base64Decode(widget
                                                  .a
                                                  .contentPanier[widget.index]
                                                  .image),
                                              fit: BoxFit.cover,
                                            )
                                          : Center(
                                              child:
                                                  Icon(Icons.arrow_circle_up),
                                            )),
                                ),
                              ),
                              SizedBox(width: 5),
                              Flexible(
                                child: (!['null', null].contains(widget
                                            .a
                                            .contentPanier[widget.index]
                                            .image)) ||
                                        (image != null)
                                    ? Text("Image insérée")
                                    : Text("Insérer une image du produit"),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text(
                          "Annuler",
                          style: TextStyle(
                            fontSize: 17,
                            color: AppColors.yellowAppbar,
                          ),
                        ),
                      ),
                      SizedBox(width: 7),
                      ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            AppColors.green,
                          ),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(12),
                            ),
                          ),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            await widget.a.modifyContentPanierElement(
                              index: widget.index,
                              nom: productName,
                              description: productDescription,
                              quantite: int.parse(productQuantite),
                              image: (image != null)
                                  ? base64Encode(image.readAsBytesSync())
                                  : null,
                            );
                            Navigator.of(context).pop();
                          } else {
                            SnackBar(
                              backgroundColor: AppColors
                                  .scaffoldBackgroundYellowForWelcomePage,
                              content: Text(
                                "Une information est ommise ou erronée",
                                textAlign: TextAlign.center,
                              ),
                              behavior: SnackBarBehavior.floating,
                              margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                            );
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          child: Text(
                            "Modifier",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
