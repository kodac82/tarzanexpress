import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:tarzanexpress/page/inApp/homePage.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../helper/utils.dart';

import '../../../constants/appAssets.dart';
import '../../../constants/appColors.dart';
import '../../../page/inApp/commander/newOrder.dart';
import '../../../page/inApp/shop/shopWebview.dart';
import '../../../state/newOrderState.dart';
import '../../../state/panierState.dart';
import 'addElement.dart';
import 'modifyElement.dart';
import 'showImage.dart';
import 'viewProduct.dart';

class Panier extends StatefulWidget {
  final bool calledFromHomePage;
  Panier({this.calledFromHomePage});
  @override
  _PanierState createState() => _PanierState();
}

class _PanierState extends State<Panier> {
  bool showSpinner = false;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      try {
        Provider.of<PanierState>(context, listen: false).getProducts();
      } catch (e) {}
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.scaffoldBackground,
      appBar: (![null, false].contains(widget.calledFromHomePage))
          ? null
          : AppBar(
              /* automaticallyImplyLeading:
                  !(![null, false].contains(widget.calledFromHomePage)), */
              title: Text(
                "PANIER",
                style: TextStyle(
                  color: Colors.orange,
                ),
              ),
              centerTitle: true,
            ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: Consumer<PanierState>(
          builder: (context, a, _) {
            int checked = 0;
            for (var i = 0; i < a.selected.length; i++) {
              if (a.selected[i]) {
                checked++;
              }
            }
            return Column(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Visibility(
                        visible: (a.contentPanier.length != 0),
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  OutlinedButton(
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                        Colors.green,
                                      ),
                                      elevation: MaterialStateProperty.all(5),
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => AddElement(),
                                        ),
                                      );
                                    },
                                    child: Row(
                                      children: [
                                        Image(
                                          image: AssetImage(
                                            AppAssets.cartAdd,
                                          ),
                                          height: 31,
                                        ),
                                        SizedBox(width: 10),
                                        Text(
                                          "Ajouter un produit",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              /* Text(
                                "${a.contentPanier.length} produits dans le panier",
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                              Text(
                                a.selected.length != 0
                                    ? "$checked sélectionnés !"
                                    : "",
                                style: TextStyle(
                                  fontSize: 17,
                                ),
                              ), */
                            ],
                          ),
                        ),
                      ),
                      (a.contentPanier.length != 0)
                          ? Expanded(
                              child: SingleChildScrollView(
                                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    for (var i = 0;
                                        i < a.contentPanier.length;
                                        i++)
                                      GestureDetector(
                                        onTap: () {
                                          a.modifySelectedElement(
                                              index: i, value: !a.selected[i]);
                                        },
                                        child: Row(
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                      color: Colors.white,
                                                      width: 2),
                                                  color: a.selected[i]
                                                      ? AppColors.green
                                                      : Colors.grey[350]),
                                              child: a.selected[i]
                                                  ? Icon(
                                                      Icons.check,
                                                      color: Colors.white,
                                                      size: 30,
                                                    )
                                                  : Icon(
                                                      Icons.check,
                                                      color: Colors.white,
                                                      size: 30,
                                                    ),
                                            ),
                                            SizedBox(width: 5),
                                            Flexible(
                                              child: Card(
                                                child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 15,
                                                    vertical: 10,
                                                  ),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Row(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          Container(
                                                            height: 70,
                                                            width: 70,
                                                            color: AppColors
                                                                .imageUploadBackground,
                                                            child: (a.contentPanier[i].image !=
                                                                        null &&
                                                                    a.contentPanier[i]
                                                                            .image !=
                                                                        'null')
                                                                ? Image.memory(
                                                                    base64Decode(a
                                                                        .contentPanier[
                                                                            i]
                                                                        .image),
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  )
                                                                : Center(
                                                                    child: Text(
                                                                      "Pas d'image",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                    ),
                                                                  ),
                                                          ),
                                                          SizedBox(width: 13),
                                                          Flexible(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Text(
                                                                  "${a.contentPanier[i].nom}",
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  style:
                                                                      TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    fontSize:
                                                                        17,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "${a.contentPanier[i].description}",
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  style:
                                                                      TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontSize:
                                                                        15,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "Quantité : ${a.contentPanier[i].quantite}",
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        15,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceAround,
                                                        children: [
                                                          IconButton(
                                                            icon: Icon(
                                                              Icons.link,
                                                              color:
                                                                  Colors.blue,
                                                            ),
                                                            onPressed:
                                                                () async {
                                                              String gLien =
                                                                  "${a.contentPanier[i].lien}";
                                                              /* if (!gLien
                                                                  .startsWith(
                                                                      "http")) {
                                                                gLien =
                                                                    "http://" +
                                                                        gLien;
                                                              } */
                                                              if (checkValidUrl(
                                                                  url: gLien)) {
                                                                Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            ViewProduct(
                                                                      initialUrl:
                                                                          gLien,
                                                                    ),
                                                                  ),
                                                                );
                                                              } else {
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                  SnackBar(
                                                                    content:
                                                                        Text(
                                                                      "Le lien associé est invalide ou expiré!",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                    ),
                                                                    behavior:
                                                                        SnackBarBehavior
                                                                            .floating,
                                                                    margin: EdgeInsets
                                                                        .fromLTRB(
                                                                            15,
                                                                            0,
                                                                            15,
                                                                            30),
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                          IconButton(
                                                            icon: Icon(
                                                              Icons.visibility,
                                                              color: AppColors
                                                                  .green,
                                                            ),
                                                            onPressed: () {
                                                              if (a
                                                                          .contentPanier[
                                                                              i]
                                                                          .image !=
                                                                      null &&
                                                                  a.contentPanier[i]
                                                                          .image !=
                                                                      'null') {
                                                                Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            ShowImage(
                                                                      imageBase64: a
                                                                          .contentPanier[
                                                                              i]
                                                                          .image,
                                                                    ),
                                                                  ),
                                                                );
                                                              } else {
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                  SnackBar(
                                                                    duration:
                                                                        Duration(
                                                                      seconds:
                                                                          1,
                                                                    ),
                                                                    content:
                                                                        Text(
                                                                      "Ce produit n'a pas d'image associée",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                    ),
                                                                    behavior:
                                                                        SnackBarBehavior
                                                                            .floating,
                                                                    margin: EdgeInsets
                                                                        .fromLTRB(
                                                                            15,
                                                                            0,
                                                                            15,
                                                                            30),
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                          IconButton(
                                                            icon: Icon(
                                                              Icons.edit,
                                                              color: AppColors
                                                                  .indigo,
                                                            ),
                                                            onPressed: () {
                                                              showDialog(
                                                                barrierDismissible:
                                                                    false,
                                                                context:
                                                                    context,
                                                                builder:
                                                                    (context) {
                                                                  return ModifyStoredCartElement(
                                                                    productName: a
                                                                        .contentPanier[
                                                                            i]
                                                                        .nom,
                                                                    productDescription: a
                                                                        .contentPanier[
                                                                            i]
                                                                        .description,
                                                                    productUrl: a
                                                                        .contentPanier[
                                                                            i]
                                                                        .lien,
                                                                    index: i,
                                                                    a: a,
                                                                  );
                                                                },
                                                              );
                                                            },
                                                          ),
                                                          IconButton(
                                                            icon: Icon(
                                                              Icons
                                                                  .delete_outline_rounded,
                                                              color:
                                                                  AppColors.red,
                                                            ),
                                                            onPressed:
                                                                () async {
                                                              bool doDelete =
                                                                  false;
                                                              await showDialog(
                                                                barrierDismissible:
                                                                    false,
                                                                context:
                                                                    context,
                                                                builder: (_) =>
                                                                    AlertDialog(
                                                                  title: Text(
                                                                    "SUPPRESSION",
                                                                    style: TextStyle(
                                                                        color: AppColors
                                                                            .green),
                                                                  ),
                                                                  content: Text(
                                                                    "Êtes vous sûr de vouloir supprimer cet article ?",
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            17),
                                                                  ),
                                                                  actions: [
                                                                    TextButton(
                                                                      child: Text(
                                                                          "NON"),
                                                                      onPressed:
                                                                          () {
                                                                        doDelete =
                                                                            false;
                                                                        Navigator.of(context)
                                                                            .pop();
                                                                      },
                                                                    ),
                                                                    TextButton(
                                                                      child: Text(
                                                                          "OUI"),
                                                                      onPressed:
                                                                          () {
                                                                        doDelete =
                                                                            true;
                                                                        Navigator.of(context)
                                                                            .pop();
                                                                      },
                                                                    )
                                                                  ],
                                                                ),
                                                              );
                                                              if (doDelete)
                                                                a.deleteContentPanierElement(
                                                                  id: a
                                                                      .contentPanier[
                                                                          i]
                                                                      .id,
                                                                );
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                  ],
                                ),
                              ),
                            )
                          : Expanded(
                              child: Container(
                                color: Colors.white,
                                child: Center(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: dSize.width * 1 / 18),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Vous n'avez aucun produit dans le panier!",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 15),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty.all(
                                                  Colors.green,
                                                ),
                                                elevation:
                                                    MaterialStateProperty.all(
                                                        5),
                                              ),
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        AddElement(),
                                                  ),
                                                );
                                              },
                                              child: Row(
                                                children: [
                                                  Image(
                                                    image: AssetImage(
                                                      AppAssets.cartAdd,
                                                    ),
                                                    height: 31,
                                                  ),
                                                  SizedBox(width: 10),
                                                  Text(
                                                    "Ajouter un produit",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 15),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: dSize.height / 70,
                                              horizontal: dSize.width / 14),
                                          child: Divider(thickness: 2),
                                        ),
                                        Text(
                                          "Vous pouvez également faire vos courses en cliquant sur votre boutique préferée ici !",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 15),
                                        ),
                                        SizedBox(height: 10),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            for (var i = 0;
                                                i < boutiquesImg.length;
                                                i++)
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          HomePage(),
                                                    ),
                                                  );
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          ShopWebview(
                                                        brand: boutiquesUrl[i],
                                                      ),
                                                    ),
                                                  );
                                                },
                                                child: Image.asset(
                                                  boutiquesImg[i],
                                                  height: dSize.height *
                                                      0.071225071,
                                                  width:
                                                      dSize.width * 0.13888888,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 2.5),
                  color: AppColors.marine,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: GestureDetector(
                          onTap: () {
                            if (a.contentPanier.length != 0) {
                              a.selectAll();
                            }
                          },
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.white),
                                    color: (a.selected.length != 0 &&
                                            checked == a.selected.length)
                                        ? AppColors.green
                                        : Colors.transparent),
                                child: (a.selected.length != 0 &&
                                        checked == a.selected.length)
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      )
                                    : Icon(
                                        Icons.check_box_outline_blank,
                                        color: Colors.transparent,
                                      ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Flexible(
                                child: Text(
                                  "Tout selectionner",
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            (a.contentPanier.length != 0 && checked > 0)
                                ? AppColors.pink
                                : Colors.grey,
                          ),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(12),
                            ),
                          ),
                        ),
                        onPressed: () async {
                          if (a.contentPanier.length != 0 && checked > 0) {
                            if (await DataConnectionChecker().hasConnection) {
                              var hState = Provider.of<NewOrderState>(context,
                                  listen: false);
                              setState(() {
                                showSpinner = true;
                              });
                              List result = await hState.getModesLivraison(
                                context: context,
                              );
                              setState(() {
                                showSpinner = false;
                              });
                              if (result[0]) {
                                hState.newProducts.clear();
                                for (var i = 0; i < a.selected.length; i++) {
                                  if (a.selected[i]) {
                                    hState.newProductsAdd(a.contentPanier[i]);
                                  }
                                }
                                /* print(
                                    /* "IMAGE IS ${hState.newProducts[0].image}\n */ "DESC IS ${hState.newProducts[0].description}\nNAME IS ${hState.newProducts[0].nom}\nLIEN IS ${hState.newProducts[0].lien}\nQUANTITE IS ${hState.newProducts[0].quantite}\n"); */
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => NewOrder(
                                      agences: result[1],
                                      modesId: result[2],
                                    ),
                                  ),
                                );
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      "Une erreur est survenue. Veuillez réessayer plutard.",
                                      textAlign: TextAlign.center,
                                    ),
                                    behavior: SnackBarBehavior.floating,
                                    margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                  ),
                                );
                              }
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                    "Connection internet faible ou inexistante.",
                                    textAlign: TextAlign.center,
                                  ),
                                  behavior: SnackBarBehavior.floating,
                                  margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                ),
                              );
                            }
                          }
                        },
                        child: Text(
                          "Commander ($checked)",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
