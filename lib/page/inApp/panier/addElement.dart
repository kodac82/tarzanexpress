import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../../../constants/appColors.dart';
import '../../../model/product.dart';
import '../../../state/panierState.dart';

class AddElement extends StatefulWidget {
  final String intentText;
  AddElement({this.intentText});
  @override
  _AddElementState createState() => _AddElementState();
}

class _AddElementState extends State<AddElement> {
  final _formKey = GlobalKey<FormState>();
  bool showSpinner = false;
  String productUrl = "";
  String productName = "";
  String productDescription = "";
  String productQuantite = "";
  File image;

  handleChooseFromGallery() async {
    File tempFile =
        File((await ImagePicker().getImage(source: ImageSource.gallery)).path);
    if (tempFile != null) {
      setState(() {
        image = tempFile;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.orange,
          ),
          title: Text(
            "AJOUT AU PANIER",
            style: TextStyle(
              color: Colors.orange,
            ),
          ),
          centerTitle: true,
        ),
        body: ModalProgressHUD(
          inAsyncCall: showSpinner,
          color: AppColors.scaffoldBackgroundYellowForWelcomePage,
          dismissible: true,
          progressIndicator: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
          ),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                child: Theme(
                  data: new ThemeData(
                    primaryColor: AppColors.indigo,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ///
                      ///URL DU PRODUIT
                      ///
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Lien",
                              style: TextStyle(color: AppColors.indigo),
                            ),
                            TextFormField(
                              keyboardType: TextInputType.name,
                              //maxLines: 1,
                              textInputAction: TextInputAction.next,
                              enabled: (widget.intentText == null),
                              initialValue: (widget.intentText != null)
                                  ? (((widget.intentText.substring(
                                          widget.intentText.indexOf("https"))))
                                      .split(' '))[0]
                                  : widget.intentText,
                              decoration: InputDecoration(
                                hintText: "https://www.alibaba.com/...",
                                hintStyle: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10, 10, 10, 10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      color: AppColors.lightGrey, width: 0.3),
                                ),
                              ),
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return "N'oubliez pas le lien";
                                }
                                if (val.length < 3) {
                                  return "Entrez un lien valide";
                                }
                                return null;
                              },
                              onSaved: (val) {
                                productUrl = val;
                              },
                            ),
                          ],
                        ),
                      ),

                      ///
                      ///NOM DU PRODUIT
                      ///
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Nom",
                              style: TextStyle(color: AppColors.indigo),
                            ),
                            TextFormField(
                              keyboardType: TextInputType.name,
                              maxLines: 1,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                hintText: "Nom du produit",
                                hintStyle: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10, 10, 10, 10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      color: AppColors.lightGrey, width: 0.3),
                                ),
                              ),
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return "N'oubliez pas le nom";
                                }
                                if (val.length < 3) {
                                  return "Entrez un nom valide";
                                }
                                return null;
                              },
                              onSaved: (val) {
                                productName = val;
                              },
                            ),
                          ],
                        ),
                      ),

                      ///
                      ///DESCRIPTION
                      ///
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Description",
                              style: TextStyle(color: AppColors.indigo),
                            ),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              maxLines: 5,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                hintText:
                                    "Précisez votre préference (Couleur, Taille, ...)",
                                hintStyle: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10, 10, 10, 10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      color: AppColors.lightGrey, width: 0.3),
                                ),
                              ),
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return "N'oubliez pas la description";
                                }
                                if (val.length < 4) {
                                  return "Entrez au moins quatre lettres";
                                }
                                return null;
                              },
                              onSaved: (val) {
                                productDescription = val;
                              },
                            ),
                          ],
                        ),
                      ),

                      ///
                      ///QUANTITE
                      ///
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Quantité",
                              style: TextStyle(color: AppColors.indigo),
                            ),
                            TextFormField(
                              keyboardType: TextInputType.number,
                              maxLines: 1,
                              textInputAction: TextInputAction.next,
                              initialValue: "1",
                              decoration: InputDecoration(
                                hintText: "Quantité",
                                hintStyle: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10, 10, 10, 10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      color: AppColors.lightGrey, width: 0.3),
                                ),
                              ),
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return "N'oubliez pas la quantité";
                                }
                                if (val.contains('-') ||
                                    val.contains(',') ||
                                    val.contains(' ') ||
                                    val.contains('.')) {
                                  return "Entrez un nombre sans espaces ni <, . ->";
                                }
                                if (int.parse(val) == 0) {
                                  return "La quantité ne doit pas être 0";
                                }
                                return null;
                              },
                              onSaved: (val) {
                                productQuantite = val;
                              },
                            ),
                          ],
                        ),
                      ),

                      ///
                      ///IMAGE
                      ///
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Image",
                              style: TextStyle(color: AppColors.indigo),
                            ),
                            Visibility(
                              visible: image == null,
                              child: Text(
                                "Il est conseillé d'ajouter un screenshot de la page web montrant le produit",
                                style: TextStyle(color: AppColors.yellowAppbar),
                              ),
                            ),
                            SizedBox(height: 8),
                            GestureDetector(
                              onTap: () {
                                handleChooseFromGallery();
                              },
                              child: Row(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      color: AppColors.imageUploadBackground
                                          .withOpacity(0.2),
                                      border: Border.all(
                                          color: AppColors
                                              .imageUploadBackgroundBorder),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(12),
                                      ),
                                    ),
                                    child: SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: image != null
                                          ? Image.file(
                                              image,
                                              fit: BoxFit.cover,
                                            )
                                          : Center(
                                              child:
                                                  Icon(Icons.arrow_circle_up),
                                            ),
                                    ),
                                  ),
                                  SizedBox(width: 5),
                                  Flexible(
                                    child: image != null
                                        ? Text("Image insérée")
                                        : Text("Insérer une image du produit"),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 12),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextButton(
                            onPressed: () => Navigator.pop(context),
                            child: Text(
                              "Annuler",
                              style: TextStyle(
                                fontSize: 17,
                                color: AppColors.yellowAppbar,
                              ),
                            ),
                          ),
                          SizedBox(width: 7),
                          ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                AppColors.green,
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(12),
                                ),
                              ),
                            ),
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                setState(() {
                                  showSpinner = true;
                                });
                                Provider.of<PanierState>(context, listen: false)
                                    .contentPanierAdd(
                                  Product(
                                    nom: productName,
                                    lien: productUrl,
                                    description: productDescription,
                                    quantite: int.parse(productQuantite),
                                    image: (image != null)
                                        ? base64Encode(image.readAsBytesSync())
                                        : null,
                                  ),
                                );
                                setState(() {
                                  showSpinner = false;
                                });
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    backgroundColor: Colors.green,
                                    duration: Duration(seconds: 3),
                                    content: Text(
                                      "Article ajouté avec succès!",
                                      textAlign: TextAlign.center,
                                    ),
                                    behavior: SnackBarBehavior.floating,
                                    margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                  ),
                                );
                                if (widget.intentText != null) {
                                  ///TODO: FIND THE RELATIVE METHOD FOR IOS
                                  ///THIS CLOSE THE APP
                                  SystemNavigator.pop();
                                } else {
                                  Navigator.of(context).pop();
                                }
                              } else {
                                SnackBar(
                                  backgroundColor: AppColors
                                      .scaffoldBackgroundYellowForWelcomePage,
                                  content: Text(
                                    "Une information est ommise ou erronée",
                                    textAlign: TextAlign.center,
                                  ),
                                  behavior: SnackBarBehavior.floating,
                                  margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                );
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 12),
                              child: Text(
                                "Ajouter",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
