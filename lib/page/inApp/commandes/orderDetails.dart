import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../constants/appAssets.dart';
import '../../../constants/infosLivraison.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../helper/utils.dart';
import '../../../state/orderState.dart';
import '../../../widgets/customWidgets.dart';

import '../../../constants/appColors.dart';
import '../../../model/order.dart';
import '../../../page/inApp/commandes/orderRecap.dart';
import '../../../page/inApp/messages/orderConversation.dart';
import '../../../page/inApp/panier/viewProduct.dart';

enum MenuOptions { Discuter, Details, Cacher }

TextStyle zStyle = TextStyle(fontSize: 13.0);

List<String> monthString = [
  "JANVIER",
  "FÉVRIER",
  "MARS",
  "AVRIL",
  "MAI",
  "JUIN",
  "JUILLET",
  "AOÛT",
  "SEPTEMBRE",
  "OCTOBRE",
  "NOVEMBRE",
  "DÉCEMBRE",
];

void showCustomModal({
  @required BuildContext context,
  @required List<Widget> jData,
}) {
  showModalBottomSheet<void>(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(32),
          topRight: Radius.circular(32),
        ),
      ),
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: jData,
          ),
        );
      });
}

Widget customColumn({@required String firstPart, @required String secondPart}) {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              firstPart,
              style: zStyle,
            ),
            SizedBox(width: 5),
            Flexible(
              child: Text(
                secondPart,
                style: zStyle,
              ),
            ),
          ],
        ),
      ),
      Divider(),
    ],
  );
}

class OrderDetails extends StatefulWidget {
  final String id;
  final int tabPosition;
  OrderDetails({@required this.id, this.tabPosition});
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  var hideOrders = ValueNotifier(true);

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    if (widget.tabPosition != null) {
      _tabController.animateTo(widget.tabPosition);
    }
  }

  Widget etatPart({
    @required Order order,
  }) {
    String param1 = order.statut["param1"];
    String param2 = order.statut["param2"];
    Map<String, dynamic> c =
        new Map<String, dynamic>.from(order.detailsCommande["colis"][0]);
    String transport = "${c["transport"]}";
    String livraisonDomicile = "${c["frais_livraison"]}";
    List parts = [
      "État",
      param1,
      "Date",
      order.detailsCommande["date_commande"],
      "Informations",
      order.information != "null" ? order.information : "",
      "Montant commande",
      order.montantCommande != "null" ? order.montantCommande : "",
      "Transport",
      !([null, 'null'].contains(transport)) ? transport : "",
      "Livraison à domicile",
      !([null, 'null'].contains(livraisonDomicile)) ? livraisonDomicile : "",
      "Frais Service",
      order.montantService != "null" ? order.montantService : "",
      "Montant Total",
      "${(order.montantLivraison != "null" ? double.parse(order.montantLivraison) : 0) + (order.montantCommande != "null" ? double.parse(order.montantCommande) : 0) + (order.montantService != "null" ? double.parse(order.montantService) : 0)} F CFA",
    ];
    Map<String, dynamic> d;
    String lastTransactMode;
    String lastTransactEtat;
    //List lastTransact;
    bool showLastTransact =
        order.detailsCommande["transaction"].toString() != 'null';
    if (showLastTransact) {
      d = new Map<String, dynamic>.from(order.detailsCommande["transaction"]);
      lastTransactMode =
          (Map<String, dynamic>.from(d["mode_paiement"]))["param1"].toString();
      lastTransactEtat =
          (Map<String, dynamic>.from(d["etat_paiement"]))["param1"].toString();
      /* lastTransact = [
        "État",
        lastTransactEtat != "null" ? lastTransactEtat : "",
        "Réference",
        d["reference"],
        "Montant",
        "${d["montant"]}" != "null" ? "${d["montant"]}" : "",
        "Mode",
        lastTransactMode != "null" ? lastTransactMode : "",
        "Commentaire",
        "${d["commentaire"]}" != "null" ? "${d["commentaire"]}" : "",
        "Date",
        "${d["date"]}" != "null" ? "${d["date"]}" : "",
      ]; */
    }

    return Column(
      children: [
        for (var i = 0; i < parts.length; i = i + 2)
          customColumn(firstPart: parts[i], secondPart: parts[i + 1]),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Trans.",
                style: zStyle,
              ),
              Flexible(
                child: param2 != "3"
                    ? ((param2 == "2" || param2 == "7")
                        ? Text(
                            "Effectué",
                            style: zStyle,
                          )
                        : Text(
                            " ",
                            style: zStyle,
                          ))
                    : ElevatedButton(
                        onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => OrderRecap(
                              order: order,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Colors.green.withOpacity(0.8),
                          ),
                        ),
                        child: Text(
                          "Effectuer le paiement",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                      ),
              ),
            ],
          ),
        ),
        Divider(),
        if (showLastTransact)
          Text(
            "Dernière Transaction",
            style: zStyle.copyWith(fontWeight: FontWeight.bold),
          ),
        if (showLastTransact)
          ListTile(
            onTap: () {
              showCustomModal(context: context, jData: [
                Center(
                  child: Text(
                    "TRANSACTION",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(height: 10),
                //DATE
                Text(
                  "Date: " + d['date'].toString()
                  /* (d['date'].substring(0, d['date'].indexOf('/'))) +
                      ' ' +
                      monthString[int.parse(d['date']
                              .substring(d['date'].indexOf('/') + 1)
                              .substring(0, 2)) -
                          1] +
                      ' ' +
                      (d['date']
                          .substring(d['date'].indexOf('/') + 1)
                          .substring(3)) */
                  ,
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 10),
                Text(
                  "Montant: " +
                      ("${d["montant"]}" != "null" ? "${d["montant"]}" : "") +
                      " XOF",
                  style: TextStyle(fontSize: 16),
                ),

                SizedBox(height: 10),
                Text(
                  "Moyen de paiement: $lastTransactMode",
                  style: TextStyle(fontSize: 16),
                ),
              ]);
            },
            leading: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.grey.shade100,
              ),
              child: Icon(
                lastTransactEtat == "Effectué"
                    ? Icons.check_outlined
                    : Icons.close,
                color: lastTransactEtat == "Effectué"
                    ? Colors.green.shade400
                    : Colors.red.shade400,
              ),
            ),
            title: Text(
              "Paiement $lastTransactEtat",
              style: zStyle,
            ),
            subtitle: Text(
              d['date'].toString()
              /* (d['date'].substring(0, d['date'].indexOf('/'))) +
                  ' ' +
                  monthString[int.parse(d['date']
                          .substring(d['date'].indexOf('/') + 1)
                          .substring(0, 2)) -
                      1] +
                  ' ' +
                  (d['date']
                      .substring(d['date'].indexOf('/') + 1)
                      .substring(3)) */
              ,
              style: zStyle,
            ),
            trailing: Text(
              d['montant'] + " XOF",
              style: zStyle.copyWith(fontWeight: FontWeight.w600),
            ),
          ),
        SizedBox(height: 10),
        /* if (showLastTransact)
          for (var i = 0; i < lastTransact.length; i = i + 2)
            customColumn(
                firstPart: lastTransact[i], secondPart: lastTransact[i + 1]), */
      ],
    );
  }

  Widget adressePart({
    @required Order order,
  }) {
    Map<String, dynamic> c =
        new Map<String, dynamic>.from(order.detailsCommande["colis"][0]);
    List parts = [
      "Réf. Colis",
      "${c["reference"]}" == "null" ? "" : "${c["reference"]}",
      "Poids",
      "${c["poids"]}" == "null" ? "" : "${c["poids"]}",
      "Commentaires",
      "${c["commentaires"]}" == "null" ? "" : "${c["commentaires"]}"
    ];
    List colisImages = c["images_customized"];
    String lieuRetrait;
    if (c['agence'] != null) {
      Map grAdress = Map.from(c['agence']['adresse']);
      lieuRetrait = ((['null', null].contains(grAdress["ville"])
                  ? ""
                  : ("Agence " + grAdress["ville"] + ": ")))
              .toString() +
          ((['null', null].contains(grAdress["pays"]) ? '' : grAdress["pays"]) +
                  ", " +
                  (['null', null].contains(grAdress["ville"])
                      ? ""
                      : grAdress["ville"]) +
                  ", " +
                  (['null', null].contains(grAdress["quartier"])
                      ? ''
                      : grAdress["quartier"]) +
                  ", " +
                  (['null', null].contains(grAdress["adresse1"])
                      ? ''
                      : grAdress["adresse1"]) +
                  "\n" +
                  (['null', null].contains(grAdress["adresse2"])
                      ? ''
                      : grAdress["adresse2"]))
              .toString();
    } else {
      lieuRetrait = ("${c["adresse_livraison"]["ville"]}" == "null"
              ? ""
              : c["adresse_livraison"]["ville"]) +
          " " +
          ("${c["adresse_livraison"]["pays"]}" == "null"
              ? ""
              : (c["adresse_livraison"]["pays"] + ", ")) +
          ("${c["adresse_livraison"]["quartier"]}" == "null"
              ? ""
              : (c["adresse_livraison"]["quartier"] + ", ")) +
          ("${c["adresse_livraison"]["adresse1"]}" == "null"
              ? ""
              : (c["adresse_livraison"]["adresse1"] + "\n")) +
          ("${c["adresse_livraison"]["adresse2"]}" == "null"
              ? ""
              : c["adresse_livraison"]["adresse2"]);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var i = 0; i < parts.length; i = i + 2)
          customColumn(firstPart: parts[i], secondPart: parts[i + 1]),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Adresse Livraison",
                style: zStyle,
              ),
              Text(
                lieuRetrait,
                style: zStyle.copyWith(
                  fontSize: 13.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 7),
          child: (colisImages == null || colisImages.length == 0)
              ? Text(
                  "AUCUNE IMAGE DISPONIBLE",
                  style: TextStyle(fontSize: 15.0),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "IMAGES DU COLIS",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    Wrap(
                      children: [
                        for (var i = 0; i < colisImages.length; i++)
                          SizedBox(
                            height: 210,
                            width: 144,
                            child: CachedNetworkImage(
                              imageUrl: colisImages[i].startsWith("http")
                                  ? colisImages[i]
                                  : "http://" + colisImages[i],
                              fit: BoxFit.cover,
                              progressIndicatorBuilder:
                                  (context, url, downloadProgress) {
                                return Center(
                                  child: CircularProgressIndicator(
                                    value: downloadProgress.progress,
                                  ),
                                );
                              },
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
        ),
        Divider(),
      ],
    );
  }

  Widget livraisonPart({
    @required Order order,
  }) {
    String livraisonPrevueMin =
        order.detailsCommande["date_prevu_livraison_min"];
    String livraisonPrevueMax =
        order.detailsCommande["date_prevu_livraison_max"];
    int typeLivraison = order.detailsCommande["colis"][0]["type_livraison_id"];
    List<Map> trajets = [
      for (var i = 0;
          i < order.detailsCommande["colis"][0]["livraison_details"].length;
          i++)
        Map.from(order.detailsCommande["colis"][0]["livraison_details"][i])
    ];
    //print("TRAJETS ====" + trajets.toString());
    //print("TYPE LIVRAISON ====" + typeLivraison.toString());

    List parts = [
      "Livraison prévue",
      "Entre ${(![
        null,
        'null'
      ].contains(livraisonPrevueMin) ? livraisonPrevueMin : "..")} et ${(![
        null,
        'null'
      ].contains(livraisonPrevueMax) ? livraisonPrevueMax : "..")}",
      "Livraison",
      typeLivraison == 15
          ? modesLivraison[ModeLivraison.AVION]
          : modesLivraison[ModeLivraison.BATEAU],
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var i = 0; i < parts.length; i = i + 2)
          customColumn(firstPart: parts[i], secondPart: parts[i + 1]),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Suivi du colis",
                style: zStyle,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Divider(
                  thickness: 2,
                ),
              ),
              (trajets == null || trajets.length == 0)
                  ? Column(
                      children: [
                        Text(
                          "AUCUNE INFORMATION DE LIVRAISON DISPONIBLE.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.amber,
                            fontSize: 17,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Les informations de livraison vont s'afficher ici dès que disponibles.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 17,
                          ),
                        ),
                      ],
                    )
                  : Column(
                      children: [
                        for (var i = 0; i < trajets.length; i++)
                          ListTile(
                            contentPadding: EdgeInsets.symmetric(vertical: 5),
                            isThreeLine: false,
                            leading: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  Icons.check_circle,
                                  color: AppColors.green,
                                ),
                                SizedBox(width: 3),
                                Text(trajets[i]["date"]
                                    .toString()
                                    .replaceAll(' ', "\n")),
                              ],
                            ),
                            title: Text(
                              trajets[i]["position"].toString(),
                              style: zStyle.copyWith(
                                  fontSize: 15, fontWeight: FontWeight.w600),
                            ),
                            subtitle:
                                Text(trajets[i]["commentaire"].toString()),
                          ),
                      ],
                    ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Détails Commande",
            style: TextStyle(color: Colors.orange),
          ),
          actions: [
            FutureBuilder(
              future: SharedPreferences.getInstance(),
              builder: (context, snapD) {
                if ((snapD.connectionState == ConnectionState.done) &&
                    (snapD.data != null)) {
                  SharedPreferences dPrefs = snapD.data;
                  if (dPrefs.containsKey('id')) {
                    hideOrders.value =
                        ((dPrefs.containsKey("hiddenOrdersId")) &&
                            (dPrefs.getStringList("hiddenOrdersId"))
                                .contains(widget.id));
                    return ValueListenableBuilder(
                      valueListenable: hideOrders,
                      builder: (context, n, _) {
                        return PopupMenuButton(
                          icon: Icon(
                            Icons.more_vert_rounded,
                            color: Colors.black,
                          ),
                          color: Colors.white,
                          onSelected: (value) async {
                            if (n) {
                              //NE PLUS CACHER
                              List<String> newList =
                                  dPrefs.getStringList("hiddenOrdersId");
                              newList.remove(widget.id);
                              dPrefs.setStringList("hiddenOrdersId", newList);
                            } else {
                              //CACHER
                              dPrefs.setStringList(
                                  "hiddenOrdersId",
                                  dPrefs.getStringList("hiddenOrdersId") +
                                      [widget.id]);
                            }
                            hideOrders.value = !hideOrders.value;
                          },
                          itemBuilder: (BuildContext context) {
                            return <PopupMenuEntry>[
                              PopupMenuItem(
                                value: MenuOptions.Cacher,
                                child: Wrap(
                                  children: [
                                    Icon(
                                      n
                                          ? Icons.visibility_outlined
                                          : Icons.visibility_off_outlined,
                                      color: Colors.brown,
                                    ),
                                    Text(
                                      n ? "Ne pas cacher" : "Cacher",
                                      style: TextStyle(
                                        color: Colors.brown,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ];
                          },
                        );
                      },
                    );
                  } else {
                    return Container();
                  }
                }
                return Container();
              },
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: FutureBuilder(
            future: DataConnectionChecker().hasConnection,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data) {
                  return FutureBuilder(
                    future: Provider.of<OrderState>(context, listen: false)
                        .getPerId(context: context, id: widget.id),
                    builder: (context, snap) {
                      if (snap.connectionState == ConnectionState.done) {
                        if (snap.data[0]) {
                          return (snap.data[1] != null)
                              ? Container(
                                  padding: EdgeInsets.fromLTRB(
                                      dSize.width * 0.029,
                                      20,
                                      dSize.width * 0.029,
                                      0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Wrap(
                                        spacing: 4,
                                        /*mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,*/
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              Clipboard.setData(
                                                new ClipboardData(
                                                    text: snap.data[1]
                                                            .reference ??
                                                        ""),
                                              );
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                SnackBar(
                                                  content: Text(
                                                    "La réference a été copiée.",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  behavior:
                                                      SnackBarBehavior.floating,
                                                  margin: EdgeInsets.fromLTRB(
                                                      15, 0, 15, 30),
                                                ),
                                              );
                                            },
                                            child: Container(
                                              padding: EdgeInsets.symmetric(
                                                horizontal: dSize.width * 0.018,
                                                vertical: 10,
                                              ),
                                              color: Colors.grey.shade300,
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  RichText(
                                                    text: TextSpan(
                                                      text: "Réf. Cmd: ",
                                                      style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                      ),
                                                      children: <TextSpan>[
                                                        TextSpan(
                                                          text: snap
                                                              .data[1].reference
                                                              .toString()
                                                              .substring(6),
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: dSize.width * 0.005,
                                                  ),
                                                  Icon(
                                                    Icons.copy_outlined,
                                                    color: Colors.blue,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              ElevatedButton(
                                                style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                    AppColors.yellowAppbar,
                                                  ),
                                                  shape:
                                                      MaterialStateProperty.all(
                                                          RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            12),
                                                  )),
                                                ),
                                                onPressed: () => Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        OrderConversation(
                                                      id: snap.data[1].id,
                                                      reference: snap
                                                          .data[1].reference,
                                                    ),
                                                  ),
                                                ),
                                                child: Row(
                                                  children: [
                                                    Icon(
                                                      Icons.mail_outline,
                                                      color: Colors.white,
                                                    ),
                                                    SizedBox(
                                                        width: dSize.width *
                                                            0.005),
                                                    Text(
                                                      "Discuter",
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: dSize.height * 0.014),
                                      Container(
                                        color: Colors.orange,
                                        child: Center(
                                          child: TabBar(
                                            controller: _tabController,
                                            indicatorColor: Colors.green,
                                            indicatorWeight: 4.0,
                                            labelColor: Colors.white,
                                            unselectedLabelColor: Colors.black,
                                            isScrollable: true,
                                            tabs: [
                                              Tab(
                                                icon: Icon(Icons.list),
                                                child: Text("ETAT"),
                                              ),
                                              Tab(
                                                icon: Icon(Icons.article),
                                                child: Text("PRODUITS"),
                                              ),
                                              Tab(
                                                icon: Icon(Icons.location_on),
                                                child: Text("COLIS"),
                                              ),
                                              Tab(
                                                icon:
                                                    Icon(Icons.local_shipping),
                                                child: Text("SUIVI"),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: TabBarView(
                                          controller: _tabController,
                                          children: [
                                            SingleChildScrollView(
                                              padding: EdgeInsets.symmetric(
                                                vertical: 7,
                                                horizontal: 7.2,
                                              ),
                                              child:
                                                  etatPart(order: snap.data[1]),
                                            ),
                                            SingleChildScrollView(
                                              padding: EdgeInsets.symmetric(
                                                vertical: 7,
                                                horizontal: 7.2,
                                              ),
                                              child: ProduitsPart(
                                                order: snap.data[1],
                                              ),
                                            ),
                                            SingleChildScrollView(
                                              padding: EdgeInsets.symmetric(
                                                vertical: 7,
                                                horizontal: 7.2,
                                              ),
                                              child: adressePart(
                                                order: snap.data[1],
                                              ),
                                            ),
                                            SingleChildScrollView(
                                              padding: EdgeInsets.symmetric(
                                                vertical: 7,
                                                horizontal: 7.2,
                                              ),
                                              child: livraisonPart(
                                                order: snap.data[1],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Spacer(),
                                      Image(
                                        width: 60,
                                        height: 60,
                                        image: AssetImage(AppAssets.orderEmpty),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 35),
                                        child: Text(
                                          "COMMANDE INTROUVABLE",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 19),
                                        ),
                                      ),
                                      Spacer(),
                                    ],
                                  ),
                                );
                        }
                        return Container(
                          height: fullHeight(context),
                          width: fullWidth(context),
                          child: Center(
                            child: Container(
                              padding: EdgeInsets.all(10),
                              color: Colors.white,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.link_off),
                                  Text(
                                    "Une erreur est survenue. Notre équipe est en train de résoudre le problème. Veuillez réessayer plutard",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 17),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }
                      return Center(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          color: Colors.white,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              CircularProgressIndicator(),
                              SizedBox(width: 10),
                              Text("Récupération des données ..")
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }
                return Container(
                  height: fullHeight(context),
                  width: fullWidth(context),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.wifi_off),
                          Text(
                            "Connection internet faible ou inexistante.",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 20),
                          ),
                          ElevatedButton(
                            onPressed: () => setState(() {}),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                Colors.blue,
                              ),
                            ),
                            child: Text(
                              "RÉESSAYER",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
              return Container(
                height: fullHeight(context),
                width: fullWidth(context),
                child: Center(
                  child: Container(
                    padding: EdgeInsets.all(10),
                    color: Colors.white,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(width: 10),
                        Text("Récupération des données ..")
                      ],
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}

class ProduitsPart extends StatefulWidget {
  final Order order;
  ProduitsPart({
    @required this.order,
  });
  @override
  _ProduitsPartState createState() => _ProduitsPartState();
}

class _ProduitsPartState extends State<ProduitsPart> {
  List<bool> listOfExpanded = [];

  @override
  void initState() {
    mounted
        ? setState(() {
            listOfExpanded = [
              for (var i = 0;
                  i <
                      widget
                          .order.detailsCommande["colis"][0]["produits"].length;
                  i++)
                false
            ];
          })
        : listOfExpanded = [
            for (var i = 0;
                i < widget.order.detailsCommande["colis"][0]["produits"].length;
                i++)
              false
          ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List parts(int i) => [
          "Nom",
          (widget.order.detailsCommande["colis"][0]["produits"][i]["nom"]
                          as String)
                      .length >
                  40
              ? (widget.order.detailsCommande["colis"][0]["produits"][i]["nom"]
                      as String)
                  .substring(0, 40)
              : widget.order.detailsCommande["colis"][0]["produits"][i]["nom"]
                  as String,
          "Description",
          (widget.order.detailsCommande["colis"][0]["produits"][i]
                          ["description"] as String)
                      .length >
                  40
              ? (widget.order.detailsCommande["colis"][0]["produits"][i]
                      ["description"] as String)
                  .substring(0, 40)
              : widget.order.detailsCommande["colis"][0]["produits"][i]
                  ["description"] as String,
          "Montant",
          widget.order.detailsCommande["colis"][0]["produits"][i]["montant"]
                      .toString() !=
                  "null"
              ? widget
                  .order.detailsCommande["colis"][0]["produits"][i]["montant"]
                  .toString()
              : "",
          "Quantité",
          widget.order.detailsCommande["colis"][0]["produits"][i]["quantite"]
                      .toString() !=
                  "null"
              ? widget
                  .order.detailsCommande["colis"][0]["produits"][i]["quantite"]
                  .toString()
              : "",
          "Poids",
          widget.order.detailsCommande["colis"][0]["produits"][i]["poids"]
                      .toString() !=
                  "null"
              ? widget.order.detailsCommande["colis"][0]["produits"][i]["poids"]
                  .toString()
              : "",
          "Statut",
          widget.order.detailsCommande["colis"][0]["produits"][i]["statut"]
                      .toString() !=
                  "null"
              ? widget
                  .order.detailsCommande["colis"][0]["produits"][i]["statut"]
                  .toString()
              : "",
        ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var i = 0;
            i < widget.order.detailsCommande["colis"][0]["produits"].length;
            i++)
          Row(
            children: [
              Expanded(
                child: ExpansionPanelList(
                  expansionCallback: (int index, bool isExpanded) {
                    setState(() {
                      listOfExpanded[i] = !listOfExpanded[i];
                    });
                  },
                  children: [
                    ExpansionPanel(
                      isExpanded: listOfExpanded[i],
                      canTapOnHeader: true,
                      headerBuilder: (BuildContext context, bool isExpanded) {
                        return Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            widget.order.detailsCommande["colis"][0]["produits"]
                                [i]["nom"],
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        );
                      },
                      body: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            for (var j = 0; j < parts(i).length; j = j + 2)
                              customColumn(
                                firstPart: parts(i)[j],
                                secondPart: parts(i)[j + 1],
                              ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(10, 10, 10, 20),
                              child: GestureDetector(
                                onTap: () async {
                                  /* print(
                                      "LE LIEN EST : ${widget.order.detailsCommande["colis"][0]["produits"][i]["lien"]}"); */
                                  String gLien =
                                      "${widget.order.detailsCommande["colis"][0]["produits"][i]["lien"] as String}";
                                  if (checkValidUrl(url: gLien)) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ViewProduct(
                                          initialUrl: gLien,
                                        ),
                                      ),
                                    );
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                          "Le lien associé est invalide ou expiré!",
                                          textAlign: TextAlign.center,
                                        ),
                                        behavior: SnackBarBehavior.floating,
                                        margin:
                                            EdgeInsets.fromLTRB(15, 0, 15, 30),
                                      ),
                                    );
                                  }
                                },
                                child: Text(
                                  "Voir le produit",
                                  style:
                                      zStyle.copyWith(color: AppColors.indigo),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
      ],
    );
  }
}
