import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../model/order.dart';
import '../../../constants/appColors.dart';
import '../../../page/inApp/messages/orderConversation.dart';
import '../../../page/inApp/commandes/orderDetails.dart';
import '../../../state/orderState.dart';
import '../../../constants/appAssets.dart';

enum MenuOptions { Discuter, Details }

class Orders extends StatefulWidget {
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  TextStyle whiteColorTextStyle = TextStyle(color: Colors.white);
  Map<String, dynamic> etats = {
    //Annulé
    '0': Colors.brown,
    //EN COURS DE CÔTATION
    "1": AppColors.indigo,
    //En cours d'exécussion
    '2': AppColors.green,
    //EN ATTENTE DE PAIEMENT
    '3': AppColors.yellowAppbar,
    //En attente de livraison
    '4': Colors.teal,
    //Suspendu
    '5': AppColors.bgSecondary,
    //Refusé
    '6': Colors.black,
    //Terminé
    '7': Colors.blue,
    //En attente de livraison
    '8': Colors.teal,
    //En attente d'avis-Côtation
    '9': Colors.blueGrey,
    //En attente d'avis-Éxecution
    '10': Colors.lime,
  };
  String day = DateTime.now().day.toString();
  String month = DateTime.now().month.toString();
  String year = DateTime.now().year.toString();
  String filterFirstDate = "2019/01/01";
  String filterLastDate = "${DateTime.now().year}" +
      '/' +
      (DateTime.now().month.toString().length > 1
          ? "${DateTime.now().month}"
          : "0${DateTime.now().month}") +
      '/' +
      (DateTime.now().day.toString().length > 1
          ? "${DateTime.now().day}"
          : "0${DateTime.now().day}");

  String reformatDate(String s) {
    String tempDate = s.substring(0, s.indexOf(' '));
    String day = tempDate.substring(0, tempDate.indexOf('/'));
    String month = tempDate.substring(
        tempDate.indexOf('/') + 1, tempDate.lastIndexOf('/'));
    String year = tempDate.substring(tempDate.lastIndexOf('/') + 1);
    return year + '-' + month + '-' + day + s.substring(s.indexOf(' '));
  }

  @override
  Widget build(BuildContext context) {
    DateTime start = DateTime.parse(
      filterFirstDate.replaceAll('/', '-') + " 00:00:00",
    );
    DateTime end = DateTime.parse(
      filterLastDate.replaceAll('/', '-') + " 23:59:59",
    );
    List<Order> firstFiltered = [];
    List<Order> lastFiltered = [];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.orange,
        ),
        title: Text(
          "MES COMMANDES",
          style: TextStyle(
            color: Colors.orange,
          ),
        ),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  child: Text(
                    (filterFirstDate
                            .substring(filterFirstDate.lastIndexOf('/') + 1)) +
                        '/' +
                        (filterFirstDate
                                .substring(filterFirstDate.indexOf('/')))
                            .substring(1, filterFirstDate.indexOf('/') - 1) +
                        '/' +
                        (filterFirstDate.substring(
                            0, filterFirstDate.indexOf('/'))),
                    style: TextStyle(
                      fontSize: 20,
                      color: AppColors.indigo,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  onTap: () async {
                    DateTime tempDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2025),
                    );
                    if (tempDate != null)
                      setState(() {
                        filterFirstDate = tempDate.year.toString() +
                            (tempDate.month.toString().length > 1
                                ? "/${tempDate.month}"
                                : "/0${tempDate.month}") +
                            (tempDate.day.toString().length > 1
                                ? "/${tempDate.day}"
                                : "/0${tempDate.day}");
                      });
                  },
                ),
                Text(
                  "À",
                  style: TextStyle(fontSize: 20),
                ),
                GestureDetector(
                  child: Text(
                    (filterLastDate
                            .substring(filterLastDate.lastIndexOf('/') + 1)) +
                        '/' +
                        (filterLastDate.substring(filterLastDate.indexOf('/')))
                            .substring(1, filterLastDate.indexOf('/') - 1) +
                        '/' +
                        (filterLastDate.substring(
                            0, filterLastDate.indexOf('/'))),
                    style: TextStyle(
                      fontSize: 20,
                      color: AppColors.indigo,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  onTap: () async {
                    DateTime tempDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2025),
                    );
                    if (tempDate != null)
                      setState(() {
                        filterLastDate = tempDate.year.toString() +
                            (tempDate.month.toString().length > 1
                                ? "/${tempDate.month}"
                                : "/0${tempDate.month}") +
                            (tempDate.day.toString().length > 1
                                ? "/${tempDate.day}"
                                : "/0${tempDate.day}");
                      });
                  },
                ),
              ],
            ),
          ),
          FutureBuilder(
              future: DataConnectionChecker().hasConnection,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (!snapshot.data) {
                    return Expanded(
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(Icons.wifi_off),
                              Text(
                                "Connection internet faible ou inexistante !",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              ),
                              ElevatedButton(
                                onPressed: () => setState(() {}),
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Colors.blue,
                                  ),
                                ),
                                child: Text(
                                  "RÉESSAYER",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  } else {
                    return FutureBuilder(
                      future: Provider.of<OrderState>(context, listen: false)
                          .getOrders(context: context),
                      builder: (context, a) {
                        if (a.connectionState == ConnectionState.done) {
                          if (a.data[0]) {
                            for (var i = 0; i < a.data[1].length; i++) {
                              int result = start.compareTo(DateTime.parse(
                                reformatDate(
                                  a.data[1][i].detailsCommande["date_commande"],
                                ),
                              ));
                              if (result < 1 || result == 0) {
                                firstFiltered.add(a.data[1][i]);
                              }
                            }
                            for (var i = 0; i < firstFiltered.length; i++) {
                              int result = end.compareTo(DateTime.parse(
                                reformatDate(
                                  firstFiltered[i]
                                      .detailsCommande["date_commande"],
                                ),
                              ));
                              if (result > -1 || result == 0) {
                                lastFiltered.add(firstFiltered[i]);
                              }
                            }
                          }
                          if (!a.data[0]) {
                            return Expanded(
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(Icons.link_off),
                                      Text(
                                        "Une erreur s'est produite. Notre équipe est en train de résoudre le problème. Veuillez réessayer plutard.",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 20),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }
                          return (a.data[0] && a.data[1].length != 0)
                              ? Flexible(
                                  child: SingleChildScrollView(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        if (lastFiltered.length == 0)
                                          Padding(
                                            padding: EdgeInsets.all(10),
                                            child: Center(
                                              child: Text(
                                                "AUCUNE COMMANDE TROUVÉE",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 19),
                                              ),
                                            ),
                                          ),
                                        if (lastFiltered.length != 0)
                                          for (var i = 0;
                                              i < lastFiltered.length;
                                              i++)
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        OrderDetails(
                                                      id: lastFiltered[i].id,
                                                    ),
                                                  ),
                                                );
                                              },
                                              child: Container(
                                                margin:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 5.0),
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        25, 25, 5, 25),
                                                decoration: BoxDecoration(
                                                  color: etats[lastFiltered[i]
                                                      .statut["param2"]],
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(12),
                                                  ),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Flexible(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            lastFiltered[i]
                                                                .reference,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style:
                                                                whiteColorTextStyle
                                                                    .copyWith(
                                                              fontSize: 17,
                                                            ),
                                                          ),
                                                          SizedBox(height: 7),
                                                          Text(
                                                            lastFiltered[i]
                                                                .detailsCommande[
                                                                    "date_commande"]
                                                                .toString(),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style:
                                                                whiteColorTextStyle
                                                                    .copyWith(
                                                              fontSize: 17,
                                                            ),
                                                          ),
                                                          SizedBox(height: 7),
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            children: [
                                                              Text(
                                                                "MONTANT: ",
                                                                style:
                                                                    whiteColorTextStyle
                                                                        .copyWith(
                                                                  fontSize: 17,
                                                                ),
                                                              ),
                                                              Text(
                                                                lastFiltered[i]
                                                                            .montantCommande ==
                                                                        "null"
                                                                    ? ".."
                                                                    : lastFiltered[i]
                                                                            .montantCommande ??
                                                                        "..",
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: whiteColorTextStyle.copyWith(
                                                                    fontSize:
                                                                        17,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              ),
                                                            ],
                                                          ),
                                                          SizedBox(height: 7),
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            children: [
                                                              Text(
                                                                "ETAT: ",
                                                                style:
                                                                    whiteColorTextStyle
                                                                        .copyWith(
                                                                  fontSize: 17,
                                                                ),
                                                              ),
                                                              Text(
                                                                lastFiltered[i]
                                                                    .statut[
                                                                        "param1"]
                                                                    .toString(),
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: whiteColorTextStyle.copyWith(
                                                                    fontSize:
                                                                        17,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              ),
                                                            ],
                                                          ),
                                                          lastFiltered[i].detailsCommande[
                                                                      "date_livraison"] !=
                                                                  null
                                                              ? Text(
                                                                  "DATE LIVRAISON: " +
                                                                      lastFiltered[
                                                                              i]
                                                                          .detailsCommande[
                                                                              "date_livraison"]
                                                                          .toString(),
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  style: whiteColorTextStyle.copyWith(
                                                                      fontSize:
                                                                          15,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                )
                                                              : Container(),
                                                        ],
                                                      ),
                                                    ),
                                                    PopupMenuButton(
                                                      icon: Icon(
                                                        Icons.more_vert_rounded,
                                                        color: Colors.white,
                                                      ),
                                                      color: Colors.white,
                                                      onSelected: (value) =>
                                                          Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) => value ==
                                                                  MenuOptions
                                                                      .Discuter
                                                              ? OrderConversation(
                                                                  id: lastFiltered[
                                                                          i]
                                                                      .id,
                                                                  reference:
                                                                      lastFiltered[
                                                                              i]
                                                                          .reference,
                                                                )
                                                              : OrderDetails(
                                                                  id: lastFiltered[
                                                                          i]
                                                                      .id,
                                                                ),
                                                        ),
                                                      ),
                                                      itemBuilder: (BuildContext
                                                          context) {
                                                        return <PopupMenuEntry>[
                                                          PopupMenuItem(
                                                            value: MenuOptions
                                                                .Discuter,
                                                            child: Row(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: [
                                                                Icon(Icons
                                                                    .mail_outline),
                                                                Text(
                                                                    "Discuter"),
                                                              ],
                                                            ),
                                                          ),
                                                          PopupMenuItem(
                                                            value: MenuOptions
                                                                .Details,
                                                            child: Row(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: [
                                                                Icon(Icons
                                                                    .arrow_circle_up_outlined),
                                                                Text("Détails"),
                                                              ],
                                                            ),
                                                          ),
                                                        ];
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                      ],
                                    ),
                                  ),
                                )
                              : Expanded(
                                  child: Column(
                                    children: [
                                      Spacer(),
                                      Image(
                                        width: 60,
                                        height: 60,
                                        image: AssetImage(AppAssets.orderEmpty),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 35),
                                        child: Text(
                                          "AUCUNE COMMANDE ENREGISTREE",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 19),
                                        ),
                                      ),
                                      Spacer(),
                                    ],
                                  ),
                                );
                        }

                        return Expanded(
                          child: Center(
                            child: Container(
                              padding: EdgeInsets.all(10),
                              color: Colors.white,
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  CircularProgressIndicator(),
                                  SizedBox(width: 10),
                                  Text("Récupération des données ..")
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                }
                return Expanded(
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      color: Colors.white,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(width: 10),
                          Text("Récupération des données ..")
                        ],
                      ),
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
