import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:provider/provider.dart';
import '../../../state/transactionState.dart';
import '../../../widgets/customWidgets.dart';
import 'package:url_launcher/url_launcher.dart';

class PayGateWebview extends StatefulWidget {
  final String url;
  final String transactionId;
  PayGateWebview({@required this.url, @required this.transactionId});
  @override
  _PayGateWebviewState createState() => _PayGateWebviewState();
}

class _PayGateWebviewState extends State<PayGateWebview> {
  final GlobalKey webViewKey = GlobalKey();
  InAppWebViewController webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(
      useShouldOverrideUrlLoading: true,
      mediaPlaybackRequiresUserGesture: false,
    ),
    android: AndroidInAppWebViewOptions(
      useHybridComposition: true,
    ),
    ios: IOSInAppWebViewOptions(
      allowsInlineMediaPlayback: true,
    ),
  );
  PullToRefreshController pullToRefreshController;
  String url = "";
  double progress = 0;
  final urlController = TextEditingController();
  bool errorOccured = false;
  bool webviewCreated = false;

  @override
  void initState() {
    super.initState();

    pullToRefreshController = PullToRefreshController(
      options: PullToRefreshOptions(
        color: Colors.blue,
      ),
      onRefresh: () async {
        if (Platform.isAndroid) {
          webViewController?.reload();
        } else if (Platform.isIOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(url: await webViewController?.getUrl()));
        }
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Étape Transfert"),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              webViewController?.goBack();
            },
          ),
          IconButton(
            icon: Icon(Icons.arrow_forward_ios),
            onPressed: () {
              webViewController?.goForward();
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Visibility(
            visible: !webviewCreated,
            child: Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text(
                      "VEUILLEZ PATIENTER..",
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.orange,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            child: (errorOccured
                ? Container(
                    width: fullWidth(context),
                    padding: EdgeInsets.fromLTRB(10, 21, 10, 21),
                    child: Column(
                      children: [
                        Icon(
                          Icons.flash_off,
                          color: Colors.amber,
                          size: 54,
                        ),
                        Text(
                          "Hmm. Nous ne parvenons pas à trouver cette page.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                        SizedBox(height: 14),
                        Text(
                          "Voici trois choses que vous pouvez essayer:",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 21),
                        Text(
                          " - Vérifiez votre connection réseau.\n - Réessayez plus tard.\n - Si vous êtes connecté mais derrière un pare-feu, vérifiez d'être autorisé à accéder au Web.",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                        SizedBox(height: 14),
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.blue[900]),
                          ),
                          onPressed: () {
                            setState(() {
                              errorOccured = false;
                            });
                          },
                          child: Text(
                            "Réessayer",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : Stack(
                    children: [
                      InAppWebView(
                        key: webViewKey,
                        initialUrlRequest:
                            URLRequest(url: Uri.parse(widget.url)),
                        initialOptions: options,
                        pullToRefreshController: pullToRefreshController,
                        onWebViewCreated: (controller) {
                          setState(() {
                            webviewCreated = true;
                          });
                          webViewController = controller;
                        },
                        onLoadStart: (controller, url) {
                          setState(() {
                            this.url = url.toString();
                            urlController.text = this.url;
                          });
                        },
                        androidOnPermissionRequest:
                            (controller, origin, resources) async {
                          return PermissionRequestResponse(
                              resources: resources,
                              action: PermissionRequestResponseAction.GRANT);
                        },
                        shouldOverrideUrlLoading:
                            (controller, navigationAction) async {
                          var uri = navigationAction.request.url;

                          if (![
                            "http",
                            "https",
                            "file",
                            "chrome",
                            "data",
                            "javascript",
                            "about"
                          ].contains(uri.scheme)) {
                            if (await canLaunch(url)) {
                              // Launch the App
                              await launch(
                                url,
                              );
                              // and cancel the request
                              return NavigationActionPolicy.CANCEL;
                            }
                          }

                          return NavigationActionPolicy.ALLOW;
                        },
                        onLoadStop: (controller, url) async {
                          pullToRefreshController.endRefreshing();
                          setState(() {
                            this.url = url.toString();
                            urlController.text = this.url;
                          });
                          if ([
                            "paygateglobal.com/tmoney/declined",
                            //"paygateglobal.com/tmoney/accepted",
                            "https://paygateglobal.com/v1/confirmation",
                            "https://paygateglobal.com/tmoney/cancelled",
                          ].contains(url)) {
                            await Future.delayed(
                              Duration(
                                seconds: "$url" ==
                                        "https://paygateglobal.com/v1/confirmation"
                                    ? 10
                                    : 3,
                              ),
                            );
                            ScaffoldFeatureController snackCtrl =
                                ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                  "MISE À JOUR DES INFORMATIONS DE LA TRANSACTION...",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                duration: Duration(seconds: 30),
                                behavior: SnackBarBehavior.floating,
                                margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                              ),
                            );
                            await Provider.of<TransactionState>(context,
                                    listen: false)
                                .checkState(
                              context: context,
                              transactionId: widget.transactionId,
                            );
                            snackCtrl.close();
                          }
                        },
                        onLoadError: (controller, url, code, message) {
                          pullToRefreshController.endRefreshing();
                          setState(() {
                            errorOccured = true;
                          });
                        },
                        onProgressChanged: (controller, progress) {
                          if (progress == 100) {
                            pullToRefreshController.endRefreshing();
                          }
                          setState(() {
                            this.progress = progress / 100;
                            urlController.text = this.url;
                          });
                        },
                        onUpdateVisitedHistory:
                            (controller, url, androidIsReload) {
                          setState(() {
                            this.url = url.toString();
                            urlController.text = this.url;
                          });
                        },
                        onConsoleMessage: (controller, consoleMessage) {
                          //print(consoleMessage);
                        },
                      ),
                      progress < 1.0
                          ? LinearProgressIndicator(value: progress)
                          : Container(),
                    ],
                  )),
          ),
        ],
      ),
    );
  }
}
