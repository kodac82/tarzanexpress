import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../model/order.dart';
import '../../../page/inApp/homePage.dart';
import '../../../widgets/customWidgets.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../constants/appColors.dart';
import '../../../page/inApp/commandes/orderDetails.dart';
import '../../../page/inApp/messages/orderConversation.dart';
import '../../../state/orderState.dart';

enum MenuOptions { Discuter, Details, Cacher }

List<String> monthString = [
  "JAN",
  "FÉV",
  "MAR",
  "AVR",
  "MAI",
  "JUIN",
  "JUIL",
  "AOÛ",
  "SEP",
  "OCT",
  "NOV",
  "DÉC",
];

class FilteredOrders extends StatefulWidget {
  final String title;
  final List<String> filterList;
  FilteredOrders({@required this.title, @required this.filterList});
  @override
  _FilteredOrdersState createState() => _FilteredOrdersState();
}

class _FilteredOrdersState extends State<FilteredOrders> {
  TextStyle whiteColorTextStyle = TextStyle(color: Colors.white);
  var hideOrders = ValueNotifier(true);

  Map<String, dynamic> etats = {
    //Annulé
    '0': Colors.brown,
    //EN COURS DE CÔTATION
    "1": AppColors.indigo,
    //En cours d'exécussion
    '2': AppColors.green,
    //EN ATTENTE DE PAIEMENT
    '3': AppColors.yellowAppbar,
    //En attente de livraison
    '4': Colors.teal,
    //Suspendu
    '5': AppColors.bgSecondary,
    //Refusé
    '6': Colors.black,
    //Terminé
    '7': Colors.blue,
    //En attente de livraison
    '8': Colors.teal,
    //En attente d'avis-Côtation
    '9': Colors.blueGrey,
    //En attente d'avis-Éxecution
    '10': Colors.lime,
  };

  String day = DateTime.now().day.toString();
  String month = DateTime.now().month.toString();
  String year = DateTime.now().year.toString();
  String filterFirstDate = ((DateTime.now().month - 6 > 0)
          ? "${DateTime.now().year}"
          : "${DateTime.now().year - 1}") +
      '/' +
      ((DateTime.now().month - 6 > 0)
          ? (((DateTime.now().month - 6).toString().length > 1
              ? "${DateTime.now().month - 6}"
              : "0${DateTime.now().month - 6}"))
          : (((DateTime.now().month + 6).toString().length > 1
              ? "${DateTime.now().month + 6}"
              : "0${DateTime.now().month + 6}"))) +
      '/' +
      (DateTime.now().day.toString().length > 1
          ? "${DateTime.now().day}"
          : "0${DateTime.now().day}");
  String filterLastDate = "${DateTime.now().year}" +
      '/' +
      (DateTime.now().month.toString().length > 1
          ? "${DateTime.now().month}"
          : "0${DateTime.now().month}") +
      '/' +
      (DateTime.now().day.toString().length > 1
          ? "${DateTime.now().day}"
          : "0${DateTime.now().day}");

  String reformatDate(String s) {
    String tempDate = s.substring(0, s.indexOf(' '));
    String day = tempDate.substring(0, tempDate.indexOf('/'));
    String month = tempDate.substring(
        tempDate.indexOf('/') + 1, tempDate.lastIndexOf('/'));
    String year = tempDate.substring(tempDate.lastIndexOf('/') + 1);
    return year + '-' + month + '-' + day + s.substring(s.indexOf(' '));
  }

  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    DateTime start = DateTime.parse(
      filterFirstDate.replaceAll('/', '-') + " 00:00:00",
    );
    DateTime end = DateTime.parse(
      filterLastDate.replaceAll('/', '-') + " 23:59:59",
    );
    List<Order> firstFiltered = [];
    List<Order> lastFiltered = [];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.orange,
        ),
        title: Text(
          widget.title,
          style: TextStyle(
            color: Colors.orange,
          ),
        ),
        centerTitle: true,
        actions: [
          ValueListenableBuilder(
            valueListenable: hideOrders,
            builder: (context, n, _) {
              return PopupMenuButton(
                icon: Icon(
                  Icons.more_vert_rounded,
                  color: Colors.black,
                ),
                color: Colors.white,
                onSelected: (value) async {
                  hideOrders.value = !hideOrders.value;
                },
                itemBuilder: (BuildContext context) {
                  return <PopupMenuEntry>[
                    PopupMenuItem(
                      value: MenuOptions.Cacher,
                      child: Wrap(
                        children: [
                          Icon(
                            n
                                ? Icons.visibility_outlined
                                : Icons.visibility_off_outlined,
                            color: Colors.brown,
                          ),
                          Text(
                            (n ? "Afficher" : "Ne pas afficher") +
                                " les cachées",
                            style: TextStyle(
                              color: Colors.brown,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ];
                },
              );
            },
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: dSize.height * 0.0025),
          Padding(
            padding: EdgeInsets.fromLTRB(
                0, dSize.height * 0.01, 0, dSize.height * 0.0025),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  child: Text(
                    (filterFirstDate
                            .substring(filterFirstDate.lastIndexOf('/') + 1)) +
                        ' ' +
                        monthString[int.parse(((filterFirstDate.substring(
                                        filterFirstDate.indexOf('/')))
                                    .substring(
                                        1, filterFirstDate.indexOf('/') - 1))
                                .toString()) -
                            1] +
                        ' ' +
                        (filterFirstDate.substring(
                            0, filterFirstDate.indexOf('/'))),
                    style: TextStyle(
                      fontSize: 20,
                      color: AppColors.indigo,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  onTap: () async {
                    DateTime tempDate;
                    await showCupertinoModalPopup(
                      context: context,
                      builder: (context) => CupertinoActionSheet(
                        actions: [
                          SizedBox(
                            height: dSize.height * 0.256,
                            child: CupertinoDatePicker(
                              minimumYear: 2019,
                              maximumDate: DateTime(DateTime.now().year,
                                  DateTime.now().month, DateTime.now().day - 1),
                              initialDateTime: filterFirstDate != null
                                  ? (DateTime(
                                      int.parse(
                                        filterFirstDate.substring(
                                          0,
                                          filterFirstDate.indexOf('/'),
                                        ),
                                      ),
                                      int.parse(((filterFirstDate.substring(
                                                  filterFirstDate.indexOf('/')))
                                              .substring(
                                                  1,
                                                  filterFirstDate.indexOf('/') -
                                                      1))
                                          .toString()),
                                      int.parse(
                                        filterFirstDate.substring(
                                            filterFirstDate.lastIndexOf('/') +
                                                1),
                                      ),
                                    ))
                                  : DateTime(
                                      DateTime.now().year,
                                      DateTime.now().month,
                                      DateTime.now().day - 1),
                              mode: CupertinoDatePickerMode.date,
                              onDateTimeChanged: (value) => tempDate = value,
                            ),
                          ),
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: Text('OK'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    );
                    if (tempDate != null)
                      setState(() {
                        filterFirstDate = tempDate.year.toString() +
                            (tempDate.month.toString().length > 1
                                ? "/${tempDate.month}"
                                : "/0${tempDate.month}") +
                            (tempDate.day.toString().length > 1
                                ? "/${tempDate.day}"
                                : "/0${tempDate.day}");
                      });
                  },
                ),
                Text(
                  "À",
                  style: TextStyle(fontSize: 20),
                ),
                GestureDetector(
                  child: Text(
                    (filterLastDate
                            .substring(filterLastDate.lastIndexOf('/') + 1)) +
                        ' ' +
                        monthString[int.parse(((filterLastDate
                                        .substring(filterLastDate.indexOf('/')))
                                    .substring(
                                        1, filterLastDate.indexOf('/') - 1))
                                .toString()) -
                            1] +
                        ' ' +
                        (filterLastDate.substring(
                            0, filterLastDate.indexOf('/'))),
                    style: TextStyle(
                      fontSize: 20,
                      color: AppColors.indigo,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  onTap: () async {
                    DateTime tempDate;
                    await showCupertinoModalPopup(
                      context: context,
                      builder: (context) => CupertinoActionSheet(
                        actions: [
                          SizedBox(
                            height: dSize.height * 0.256,
                            child: CupertinoDatePicker(
                              minimumYear: 2019,
                              maximumDate: DateTime(DateTime.now().year,
                                  DateTime.now().month, DateTime.now().day),
                              initialDateTime: filterLastDate != null
                                  ? (DateTime(
                                      int.parse(
                                        filterLastDate.substring(
                                          0,
                                          filterLastDate.indexOf('/'),
                                        ),
                                      ),
                                      int.parse(((filterLastDate.substring(
                                                  filterLastDate.indexOf('/')))
                                              .substring(
                                                  1,
                                                  filterLastDate.indexOf('/') -
                                                      1))
                                          .toString()),
                                      int.parse(
                                        filterLastDate.substring(
                                            filterLastDate.lastIndexOf('/') +
                                                1),
                                      ),
                                    ))
                                  : DateTime(DateTime.now().year,
                                      DateTime.now().month, DateTime.now().day),
                              mode: CupertinoDatePickerMode.date,
                              onDateTimeChanged: (value) => tempDate = value,
                            ),
                          ),
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: Text('OK'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    );
                    if (tempDate != null)
                      setState(() {
                        filterLastDate = tempDate.year.toString() +
                            (tempDate.month.toString().length > 1
                                ? "/${tempDate.month}"
                                : "/0${tempDate.month}") +
                            (tempDate.day.toString().length > 1
                                ? "/${tempDate.day}"
                                : "/0${tempDate.day}");
                      });
                  },
                ),
              ],
            ),
          ),
          FutureBuilder(
              future: SharedPreferences.getInstance(),
              builder: (context, snapA) {
                if (snapA.connectionState == ConnectionState.done) {
                  if (snapA.data != null) {
                    List hiddenOrdersId = [];
                    SharedPreferences uPrefs = snapA.data;
                    if (uPrefs.containsKey("hiddenOrdersId")) {
                      hiddenOrdersId = uPrefs.getStringList('hiddenOrdersId');
                    } else {
                      uPrefs.setStringList("hiddenOrdersId", []);
                    }

                    return FutureBuilder(
                        future: DataConnectionChecker().hasConnection,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.data) {
                              return FutureBuilder(
                                future: Provider.of<OrderState>(context,
                                        listen: false)
                                    .getOrders(context: context),
                                builder: (context, a) {
                                  if (a.connectionState ==
                                      ConnectionState.done) {
                                    if (a.data[0]) {
                                      for (var i = 0;
                                          i < a.data[1].length;
                                          i++) {
                                        int result =
                                            start.compareTo(DateTime.parse(
                                          reformatDate(
                                            a.data[1][i].detailsCommande[
                                                "date_commande"],
                                          ),
                                        ));
                                        if (result < 1 || result == 0) {
                                          firstFiltered.add(a.data[1][i]);
                                        }
                                      }
                                      for (var i = 0;
                                          i < firstFiltered.length;
                                          i++) {
                                        int result =
                                            end.compareTo(DateTime.parse(
                                          reformatDate(
                                            firstFiltered[i].detailsCommande[
                                                "date_commande"],
                                          ),
                                        ));
                                        if (result > -1 || result == 0) {
                                          if (widget.filterList != null) {
                                            if (widget.filterList.contains(
                                                firstFiltered[i]
                                                    .statut["param2"])) {
                                              lastFiltered
                                                  .add(firstFiltered[i]);
                                            }
                                          } else {
                                            lastFiltered.add(firstFiltered[i]);
                                          }
                                        }
                                      }
                                    }
                                    if (!a.data[0]) {
                                      return Expanded(
                                        child: Container(
                                          child: Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Icon(Icons.link_off),
                                                  Text(
                                                    "Une erreur s'est produite. Notre équipe est en train de résoudre le problème. Veuillez réessayer plutard.",
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        TextStyle(fontSize: 20),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    }

                                    return Flexible(
                                      child: SingleChildScrollView(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: dSize.width * 0.04166,
                                            vertical: dSize.height * 0.014245),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            if (lastFiltered.length == 0)
                                              Padding(
                                                padding: EdgeInsets.all(10),
                                                child: Column(
                                                  children: [
                                                    Center(
                                                      child: Text(
                                                        "AUCUNE COMMANDE TROUVÉE",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            fontSize: 19),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          fullHeight(context) *
                                                              2 /
                                                              3,
                                                      width: fullWidth(context),
                                                      child: Center(
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Expanded(
                                                                  child:
                                                                      GestureDetector(
                                                                    onTap:
                                                                        () async {
                                                                      String
                                                                          _url =
                                                                          "https://www.youtube.com/watch?v=mHnvbWu-Ujo";
                                                                      await canLaunch(
                                                                              _url)
                                                                          ? await launch(
                                                                              _url)
                                                                          : throw 'Could not launch $_url';
                                                                    },
                                                                    child: Card(
                                                                      elevation:
                                                                          4,
                                                                      child:
                                                                          Container(
                                                                        height: dSize.height *
                                                                            0.21,
                                                                        padding:
                                                                            const EdgeInsets.all(8.0),
                                                                        child:
                                                                            Center(
                                                                          child:
                                                                              Column(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.center,
                                                                            children: [
                                                                              Icon(
                                                                                Icons.info_outline,
                                                                                color: Colors.orange,
                                                                              ),
                                                                              SizedBox(height: 5),
                                                                              Text(
                                                                                "AIDE",
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Expanded(
                                                                  child:
                                                                      GestureDetector(
                                                                    onTap: () {
                                                                      while (Navigator
                                                                          .canPop(
                                                                              context)) {
                                                                        Navigator.pop(
                                                                            context);
                                                                      }
                                                                      Navigator
                                                                          .pushReplacement(
                                                                        context,
                                                                        MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              HomePage(
                                                                            index:
                                                                                2,
                                                                          ),
                                                                        ),
                                                                      );
                                                                    },
                                                                    child: Card(
                                                                      elevation:
                                                                          4,
                                                                      child:
                                                                          Container(
                                                                        height: dSize.height *
                                                                            0.21,
                                                                        padding:
                                                                            const EdgeInsets.all(8.0),
                                                                        child:
                                                                            Center(
                                                                          child:
                                                                              Column(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.center,
                                                                            children: [
                                                                              Icon(
                                                                                Icons.shopping_cart_outlined,
                                                                                color: Colors.orange,
                                                                              ),
                                                                              SizedBox(height: 5),
                                                                              Text(
                                                                                "ALLER AU PANIER",
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            if (lastFiltered.length != 0)
                                              for (var i = 0;
                                                  i < lastFiltered.length;
                                                  i++)
                                                ValueListenableBuilder(
                                                    valueListenable: hideOrders,
                                                    builder: (context, n, _) {
                                                      return (!n ||
                                                              (!hiddenOrdersId
                                                                  .contains(
                                                                      lastFiltered[
                                                                              i]
                                                                          .id)))
                                                          ? GestureDetector(
                                                              onTap: () async {
                                                                bool
                                                                    reloadPage =
                                                                    await Navigator
                                                                        .push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            OrderDetails(
                                                                      id: lastFiltered[
                                                                              i]
                                                                          .id,
                                                                    ),
                                                                  ),
                                                                );
                                                                if ((reloadPage !=
                                                                        null) &&
                                                                    reloadPage)
                                                                  setState(
                                                                      () {});
                                                              },
                                                              child: Container(
                                                                margin: EdgeInsets.symmetric(
                                                                    vertical: dSize
                                                                            .height *
                                                                        0.007122),
                                                                padding: EdgeInsets.fromLTRB(
                                                                    dSize.width *
                                                                        0.0694,
                                                                    dSize.height *
                                                                        0.0356125,
                                                                    dSize.width *
                                                                        0.0138,
                                                                    dSize.height *
                                                                        0.0356125),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: etats[lastFiltered[
                                                                              i]
                                                                          .statut[
                                                                      "param2"]],
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .all(
                                                                    Radius
                                                                        .circular(
                                                                            12),
                                                                  ),
                                                                ),
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Flexible(
                                                                      child:
                                                                          Column(
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.start,
                                                                        children: [
                                                                          Text(
                                                                            lastFiltered[i].reference,
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                            style:
                                                                                whiteColorTextStyle.copyWith(
                                                                              fontSize: 17,
                                                                            ),
                                                                          ),
                                                                          SizedBox(
                                                                              height: dSize.height * 0.0099715),
                                                                          Text(
                                                                            lastFiltered[i].detailsCommande["date_commande"].toString(),
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                            style:
                                                                                whiteColorTextStyle.copyWith(
                                                                              fontSize: 17,
                                                                            ),
                                                                          ),
                                                                          SizedBox(
                                                                              height: dSize.height * 0.009971),
                                                                          Wrap(
                                                                            /*mainAxisSize:
                                                                  MainAxisSize.min,*/
                                                                            children: [
                                                                              Text(
                                                                                "MONTANT: ",
                                                                                style: whiteColorTextStyle.copyWith(
                                                                                  fontSize: 17,
                                                                                ),
                                                                              ),
                                                                              Text(
                                                                                lastFiltered[i].montantCommande == "null" ? ".." : lastFiltered[i].montantCommande ?? "..",
                                                                                overflow: TextOverflow.ellipsis,
                                                                                style: whiteColorTextStyle.copyWith(fontSize: 17, fontWeight: FontWeight.w600),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                          SizedBox(
                                                                              height: dSize.height * 0.009971),
                                                                          Wrap(
                                                                            /*mainAxisSize:
                                                                  MainAxisSize.min,*/
                                                                            children: [
                                                                              Text(
                                                                                "ETAT: ",
                                                                                style: whiteColorTextStyle.copyWith(
                                                                                  fontSize: 17,
                                                                                ),
                                                                              ),
                                                                              Text(
                                                                                lastFiltered[i].statut["param1"].toString(),
                                                                                overflow: TextOverflow.ellipsis,
                                                                                style: whiteColorTextStyle.copyWith(fontSize: 17, fontWeight: FontWeight.w600),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                          lastFiltered[i].detailsCommande["date_livraison"] != null
                                                                              ? Text(
                                                                                  "DATE LIVRAISON: " + lastFiltered[i].detailsCommande["date_livraison"].toString(),
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  style: whiteColorTextStyle.copyWith(fontSize: 15, fontWeight: FontWeight.w600),
                                                                                )
                                                                              : Container(),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    PopupMenuButton(
                                                                      icon:
                                                                          Icon(
                                                                        Icons
                                                                            .more_vert_rounded,
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                      color: Colors
                                                                          .white,
                                                                      onSelected:
                                                                          (value) async {
                                                                        if (value !=
                                                                            MenuOptions.Cacher) {
                                                                          bool
                                                                              reloadPage =
                                                                              await Navigator.push(
                                                                            context,
                                                                            MaterialPageRoute(
                                                                              builder: (context) => value == MenuOptions.Discuter
                                                                                  ? OrderConversation(
                                                                                      id: lastFiltered[i].id,
                                                                                      reference: lastFiltered[i].reference,
                                                                                    )
                                                                                  : OrderDetails(
                                                                                      id: lastFiltered[i].id,
                                                                                    ),
                                                                            ),
                                                                          );
                                                                          if ((reloadPage != null) &&
                                                                              reloadPage)
                                                                            setState(() {});
                                                                        } else {
                                                                          SharedPreferences
                                                                              uPrefs =
                                                                              await SharedPreferences.getInstance();
                                                                          if (uPrefs !=
                                                                              null) {
                                                                            if (uPrefs.containsKey("hiddenOrdersId")) {
                                                                              if (uPrefs.getStringList("hiddenOrdersId").contains(lastFiltered[i].id)) {
                                                                                List<String> newList = uPrefs.getStringList("hiddenOrdersId");
                                                                                newList.remove(lastFiltered[i].id);
                                                                                uPrefs.setStringList("hiddenOrdersId", newList);
                                                                              } else {
                                                                                uPrefs.setStringList(
                                                                                  "hiddenOrdersId",
                                                                                  uPrefs.getStringList("hiddenOrdersId") +
                                                                                      [
                                                                                        lastFiltered[i].id
                                                                                      ],
                                                                                );
                                                                              }
                                                                            } else {
                                                                              uPrefs.setStringList(
                                                                                "hiddenOrdersId",
                                                                                [
                                                                                  lastFiltered[i].id
                                                                                ],
                                                                              );
                                                                            }
                                                                            setState(() {});
                                                                          } else {
                                                                            ScaffoldMessenger.of(context).showSnackBar(
                                                                              SnackBar(
                                                                                content: Text(
                                                                                  "L'application a rencontré une erreur. Veuillez réessayer svp.",
                                                                                  textAlign: TextAlign.center,
                                                                                ),
                                                                                behavior: SnackBarBehavior.floating,
                                                                                margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                                                                              ),
                                                                            );
                                                                          }
                                                                        }
                                                                      },
                                                                      itemBuilder:
                                                                          (BuildContext
                                                                              context) {
                                                                        return <
                                                                            PopupMenuEntry>[
                                                                          PopupMenuItem(
                                                                            value:
                                                                                MenuOptions.Discuter,
                                                                            child:
                                                                                Row(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              children: [
                                                                                Icon(Icons.mail_outline),
                                                                                Text("Discuter"),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          PopupMenuItem(
                                                                            value:
                                                                                MenuOptions.Details,
                                                                            child:
                                                                                Row(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              children: [
                                                                                Icon(Icons.arrow_circle_up_outlined),
                                                                                Text("Détails"),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          PopupMenuItem(
                                                                            value:
                                                                                MenuOptions.Cacher,
                                                                            child:
                                                                                FutureBuilder(
                                                                              future: SharedPreferences.getInstance(),
                                                                              builder: (context, snapj) {
                                                                                if (snapj.connectionState == ConnectionState.done && (snapj.data != null)) {
                                                                                  String jTitle = "Cacher";
                                                                                  if (snapj.data.getStringList("hiddenOrdersId").contains(lastFiltered[i].id)) {
                                                                                    jTitle = "Ne pas cacher";
                                                                                  }
                                                                                  return Row(
                                                                                    mainAxisSize: MainAxisSize.min,
                                                                                    children: [
                                                                                      Icon(Icons.visibility),
                                                                                      Text(jTitle),
                                                                                    ],
                                                                                  );
                                                                                }
                                                                                return Container();
                                                                              },
                                                                            ),
                                                                          ),
                                                                        ];
                                                                      },
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            )
                                                          : Container();
                                                    }),
                                          ],
                                        ),
                                      ),
                                    );
                                  }

                                  return Expanded(
                                    child: Center(
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        color: Colors.white,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            CircularProgressIndicator(),
                                            SizedBox(width: 10),
                                            Text("Récupération des données ..")
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              );
                            }
                            return Expanded(
                              child: Container(
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(Icons.wifi_off),
                                        Text(
                                          "Connection internet faible ou inexistante.",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        ElevatedButton(
                                          onPressed: () => setState(() {}),
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                              Colors.blue,
                                            ),
                                          ),
                                          child: Text(
                                            "RÉESSAYER",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }
                          return Expanded(
                            child: Center(
                              child: Container(
                                padding: EdgeInsets.all(10),
                                color: Colors.white,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    CircularProgressIndicator(),
                                    SizedBox(width: 10),
                                    Text("Récupération des données ..")
                                  ],
                                ),
                              ),
                            ),
                          );
                        });
                  }
                  return Expanded(
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        color: Colors.white,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                                "L'application a rencontré un problème. Veuillez réessayer svp !"),
                            SizedBox(height: 15),
                            Center(
                              child: ElevatedButton(
                                onPressed: () {
                                  setState(() {});
                                },
                                child: Text(
                                  "RÉESSAYER",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }
                return Expanded(
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      color: Colors.white,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(width: 10),
                          Text("Initialisation..")
                        ],
                      ),
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
