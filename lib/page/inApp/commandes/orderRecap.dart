import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../constants/appAssets.dart';
import '../../../constants/appNames.dart';
import '../../../constants/infosLivraison.dart';
import '../../../helper/utils.dart';
import '../../../main.dart';
import '../../../services/notificationService.dart';
import '../../../state/orderState.dart';

import '../../../constants/appColors.dart';
import '../../../model/order.dart';
import '../../../page/inApp/commandes/payerRecap.dart';

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

class OrderRecap extends StatefulWidget {
  final Order order;
  OrderRecap({@required this.order});
  @override
  _OrderRecapState createState() => _OrderRecapState();
}

class _OrderRecapState extends State<OrderRecap> {
  bool showSpinner = false;
  int etape = 0;
  String type;
  String methode;

  List<String> zType = [
    "La totalité du montant",
    "Le transport à la livraison",
  ];
  List<String> zMethod = [
    AppAssets.walletIcon,
    AppAssets.logoFlooz,
    AppAssets.logoTMoney,
  ];
  List<String> zMethodText = [
    AppNames.methodePaiementPortefeuille,
    AppNames.methodePaiementFLOOZ,
    AppNames.methodePaiementTMONEY,
  ];

  chooseMethod({
    @required String montantApayer,
    @required double totalMtransport,
    @required String transport,
  }) {
    showModalBottomSheet<void>(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(32),
          topRight: Radius.circular(32),
        ),
      ),
      backgroundColor: Colors.white,
      builder: (context) {
        methode = null;
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      type = null;
                      methode = null;
                      Navigator.pop(context);
                      selectPaymentConfig(
                        context: context,
                        totalMtransport: totalMtransport,
                        total:
                            "${(widget.order.montantLivraison != "null" ? double.parse(widget.order.montantLivraison) : 0) + (widget.order.montantCommande != "null" ? double.parse(widget.order.montantCommande) : 0) + (widget.order.montantService != "null" ? double.parse(widget.order.montantService) : 0)}",
                        transport: transport,
                      );
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Choisissez une méthode :",
                        style: TextStyle(
                          fontSize: 21,
                          color: Colors.amber[800],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              for (var j = 0; j < zMethod.length; j++)
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 7.5),
                  child: GestureDetector(
                    onTap: () async {
                      methode = zMethodText[j];
                      Navigator.pop(context);
                      if (j == 0) {
                        //CONFIRMATION
                        bool moveOn = false;
                        bool userAnnuled = false;
                        final _formKey = GlobalKey<FormState>();
                        String confirmPass;
                        await showDialog(
                          barrierDismissible: true,
                          context: context,
                          builder: (_) => AlertDialog(
                            title: Center(
                              child: Text(
                                "Confirmation",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: AppColors.green),
                              ),
                            ),
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Vous êtes sur le point de débiter votre compte. Veuillez confirmer en entrant votre mot de passe.",
                                  style: TextStyle(fontSize: 15),
                                ),
                                Form(
                                  key: _formKey,
                                  child: Theme(
                                    data: new ThemeData(
                                      primaryColor: AppColors.indigo,
                                    ),
                                    child: TextFormField(
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      textInputAction: TextInputAction.done,
                                      obscureText: true,
                                      decoration: InputDecoration(
                                        hintText: "MOT DE PASSE",
                                        hintStyle: TextStyle(
                                          fontFamily: "Poppins",
                                          fontSize: 17,
                                        ),
                                      ),
                                      validator: (val) {
                                        if ((val == null) && val.isEmpty) {
                                          return "Veuillez entrer une valeur";
                                        }
                                        if (val.length < 8) {
                                          return "Entrez au moins 8 caractères";
                                        }
                                        return null;
                                      },
                                      onSaved: (newValue) {
                                        confirmPass = newValue;
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            actions: [
                              TextButton(
                                child: Text("ANNULER"),
                                onPressed: () async {
                                  userAnnuled = true;
                                  navKey.currentState.pop();
                                },
                              ),
                              TextButton(
                                child: Text("CONFIRMER"),
                                onPressed: () async {
                                  if (_formKey.currentState.validate()) {
                                    _formKey.currentState.save();
                                    SharedPreferences yPrefs =
                                        await SharedPreferences.getInstance();
                                    if (confirmPass ==
                                        yPrefs.getString('password')) {
                                      moveOn = true;
                                    }
                                    navKey.currentState.pop();
                                  }
                                },
                              ),
                            ],
                          ),
                        );
                        if (moveOn) {
                          setState(() {
                            showSpinner = true;
                          });

                          ///PAIEMENT PORTEFEUILLE
                          List result = await Provider.of<OrderState>(
                            navKey.currentContext,
                            listen: false,
                          ).makeTransaction(
                            type: type,
                            methode: methode,
                            montant: montantApayer,
                            numero: null,
                            commandeId: widget.order.id,
                            reference: widget.order.reference,
                            context: context,
                          );
                          setState(() {
                            showSpinner = false;
                          });
                          if (result[0]) {
                            showNotification(
                              body:
                                  "Paiement accepté. Notre équipe a débuté son exécution.",
                              helper: ("PAY" +
                                  widget.order.id +
                                  '/' +
                                  widget.order.reference),
                            );
                          } else {
                            ScaffoldMessenger.of(navKey.currentContext)
                                .showSnackBar(
                              SnackBar(
                                backgroundColor: Colors.black,
                                duration: Duration(seconds: 3),
                                content: Text(
                                  result[1],
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                behavior: SnackBarBehavior.floating,
                                margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                              ),
                            );
                          }
                          navKey.currentState.pop();
                        } else {
                          if (!userAnnuled) {
                            ScaffoldMessenger.of(navKey.currentContext)
                                .showSnackBar(
                              SnackBar(
                                backgroundColor: Colors.black,
                                duration: Duration(seconds: 3),
                                content: Text(
                                  "Mot de passe incorrect",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                behavior: SnackBarBehavior.floating,
                                margin: EdgeInsets.fromLTRB(15, 0, 15, 30),
                              ),
                            );
                          }
                        }
                      } else {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PayerRecap(
                                commandeId: widget.order.id,
                                montantApayer: montantApayer,
                                method: methode,
                                type: type,
                                reference: widget.order.reference,
                              ),
                            ));
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border:
                                Border.all(width: 2, color: AppColors.indigo),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: j == 0
                              ? Text(
                                  zMethodText[j],
                                  style: TextStyle(
                                      fontSize: 21,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.blue),
                                )
                              : Container(
                                  padding: EdgeInsets.all(4),
                                  color: zMethodText[j] ==
                                          AppNames.methodePaiementFLOOZ
                                      ? Colors.black
                                      : Colors.yellowAccent[700],
                                  child: Image.asset(
                                    zMethod[j],
                                    height: 50,
                                    width: 80,
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ),
                ),
              SizedBox(height: 25)
            ],
          ),
        );
      },
    );
  }

  void selectPaymentConfig({
    @required BuildContext context,
    @required String total,
    @required double totalMtransport,
    @required String transport,
  }) {
    showModalBottomSheet<void>(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32),
            topRight: Radius.circular(32),
          ),
        ),
        backgroundColor: Colors.white,
        builder: (BuildContext context) {
          type = null;
          methode = null;
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Je veux payer :",
                  style: TextStyle(
                    fontSize: 16,
                    color: AppColors.indigo,
                  ),
                ),
                SizedBox(height: 8),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      for (var i = 0; i < zType.length; i++)
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(
                              left: (i == 0) ? 0 : 10,
                              right: (i == 0) ? 10 : 0,
                            ),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  i != 0 ? Colors.blue : AppColors.green,
                                ),
                              ),
                              onPressed: () async {
                                type = zType[i];
                                Navigator.pop(context);
                                chooseMethod(
                                    totalMtransport: totalMtransport,
                                    montantApayer: i == 0
                                        ? total
                                        : totalMtransport.toString(),
                                    transport: transport);
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5),
                                child: Text(
                                  zType[i],
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 15,
                                    /* color:
                                        i != 0 ? Colors.blue : AppColors.green, */
                                    //fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
                SizedBox(height: 8),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: Text(
                    "NB: Payer 'Le transport à la livraison' signifie régler le coût des articles du colis (" +
                        totalMtransport.toString() +
                        " F CFA), et une fois le colis arrivé, régler les frais qu'a coûté le transport du colis (" +
                        transport +
                        " F CFA)",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 15,
                      //fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(height: 10)
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Widget customCard({
      @required Widget child,
    }) {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.circular(12),
        ),
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 18),
        child: child,
      );
    }

    Map<String, dynamic> c =
        new Map<String, dynamic>.from(widget.order.detailsCommande["colis"][0]);
    String typeLivraison = widget
        .order.detailsCommande["colis"][0]["type_livraison_id"]
        .toString();
    String lieuRetrait;
    if (c['agence'] != null) {
      Map grAdress = Map.from(c['agence']['adresse']);
      lieuRetrait = ((['null', null].contains(grAdress["ville"])
                  ? ""
                  : grAdress["ville"]) +
              " " +
              (['null', null].contains(grAdress["pays"])
                  ? ''
                  : grAdress["pays"]) +
              "\n" +
              (['null', null].contains(grAdress["quartier"])
                  ? ''
                  : grAdress["quartier"]) +
              "\n" +
              (['null', null].contains(grAdress["adresse1"])
                  ? ''
                  : grAdress["adresse1"]) +
              "\n" +
              (['null', null].contains(grAdress["adresse2"])
                  ? ''
                  : grAdress["adresse2"]))
          .toString();
    } else {
      lieuRetrait = ("${c["adresse_livraison"]["ville"]}" == "null"
              ? ""
              : c["adresse_livraison"]["ville"]) +
          " " +
          ("${c["adresse_livraison"]["pays"]}" == "null"
              ? ""
              : (c["adresse_livraison"]["pays"] + "\n")) +
          ("${c["adresse_livraison"]["quartier"]}" == "null"
              ? ""
              : (c["adresse_livraison"]["quartier"] + "\n")) +
          ("${c["adresse_livraison"]["adresse1"]}" == "null"
              ? ""
              : (c["adresse_livraison"]["adresse1"] + "\n")) +
          ("${c["adresse_livraison"]["adresse2"]}" == "null"
              ? ""
              : c["adresse_livraison"]["adresse2"]);
    }
    //print("TRANSPORT ====== " + "${c["transport"]}");
    String transport = "${c["transport"]}" == "null"
        ? "0"
        : "${c["transport"]}".replaceAll(',', '.');
    String total = (double.parse(
                widget.order.montantLivraison.toString() == "null"
                    ? "0"
                    : widget.order.montantLivraison
                        .toString()
                        .replaceAll(',', '.')) +
            double.parse((widget.order.montantCommande.toString() == "null"
                ? "0"
                : widget.order.montantCommande
                    .toString()
                    .replaceAll(',', '.'))) +
            double.parse(widget.order.montantService.toString() == "null"
                ? "0"
                : widget.order.montantService.toString().replaceAll(',', '.')))
        .toString();
    double totalMtransport = double.parse(total) - double.parse(transport);
    List part3 = [
      "Référence",
      widget.order.reference,
      "Nombre de produits",
      widget.order.detailsCommande["colis"][0]["produits"].length,
      "Montant commande",
      widget.order.montantCommande != "null"
          ? widget.order.montantCommande
          : "",
      "Transport",
      transport != "null" ? transport : "",
      "Frais Service",
      widget.order.montantService != "null" ? widget.order.montantService : "",
      "Total",
      total + " F CFA",
    ];

    TextStyle zStyle = TextStyle(fontSize: 15);

    Widget customColumn(
        {@required String firstPart, @required String secondPart}) {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  firstPart,
                  style: zStyle,
                ),
                SizedBox(width: 5),
                Flexible(
                  child: Text(
                    secondPart,
                    style: zStyle,
                  ),
                ),
              ],
            ),
          ),
          Divider(),
        ],
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.scaffoldBackground,
      appBar: yellowAppbar(title: "RECAP DE VOTRE COMMANDE"),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        color: AppColors.scaffoldBackgroundYellowForWelcomePage,
        dismissible: true,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
        ),
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              SizedBox(
                height: 25,
              ),
              customCard(
                child: Row(
                  children: [
                    Column(
                      children: [
                        Text(
                          "Livraison " +
                              (typeLivraison == "15"
                                  ? modesLivraison[ModeLivraison.AVION]
                                  : modesLivraison[ModeLivraison.BATEAU]) +
                              "\n" +
                              lieuRetrait,
                          style: zStyle.copyWith(fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              customCard(
                  child: Column(
                children: [
                  for (var i = 0;
                      i <
                          widget.order.detailsCommande["colis"][0]["produits"]
                              .length;
                      i++)
                    ListTile(
                      dense: true,
                      isThreeLine: false,
                      contentPadding: EdgeInsets.all(0),
                      leading: Container(
                        decoration: BoxDecoration(
                          color:
                              AppColors.imageUploadBackground.withOpacity(0.2),
                          border: Border.all(
                              color: AppColors.imageUploadBackgroundBorder),
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        width: 50,
                        height: 50,
                        //margin: const EdgeInsets.all(8.0),
                        child: (widget.order.detailsCommande["colis"][0]
                                        ["produits"][i]["images"] !=
                                    null &&
                                widget.order.detailsCommande["colis"][0]
                                        ["produits"][i]["images"] !=
                                    'null')
                            ? CachedNetworkImage(
                                imageUrl: widget
                                        .order
                                        .detailsCommande["colis"][0]["produits"]
                                            [i]["images"]
                                        .startsWith("http")
                                    ? widget.order.detailsCommande["colis"][0]
                                        ["produits"][i]["images"]
                                    : "http://" +
                                        widget.order.detailsCommande["colis"][0]
                                            ["produits"][i]["images"],
                                fit: BoxFit.contain,
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error_outline),
                                placeholder: (context, url) => Center(
                                  child: CircularProgressIndicator(),
                                ),
                                /* progressIndicatorBuilder:
                                    (context, url, downloadProgress) {
                                  return Center(
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress,
                                    ),
                                  );
                                }, */
                              )
                            : Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(".."),
                                  ],
                                ),
                              ),
                      ),
                      title: Text(
                        widget.order
                            .detailsCommande["colis"][0]["produits"][i]["nom"]
                            .toString(),
                        overflow: TextOverflow.ellipsis,
                        style: zStyle,
                      ),
                      subtitle: Text(
                        " X " +
                            (widget
                                        .order
                                        .detailsCommande["colis"][0]["produits"]
                                            [i]["quantite"]
                                        .toString() !=
                                    "null"
                                ? widget
                                    .order
                                    .detailsCommande["colis"][0]["produits"][i]
                                        ["quantite"]
                                    .toString()
                                : ""),
                        style: zStyle,
                      ),
                      trailing: Text(
                        widget
                                    .order
                                    .detailsCommande["colis"][0]["produits"][i]
                                        ["montant"]
                                    .toString() !=
                                "null"
                            ? (widget
                                    .order
                                    .detailsCommande["colis"][0]["produits"][i]
                                        ["montant"]
                                    .toString() +
                                ' F CFA')
                            : "",
                        style: zStyle,
                      ),
                    ),
                ],
              )),
              customCard(
                child: Column(
                  children: [
                    for (var i = 0; i < part3.length; i = i + 2)
                      customColumn(
                          firstPart: part3[i],
                          secondPart: part3[i + 1].toString())
                  ],
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(AppColors.red),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Annuler',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        AppColors.green,
                      ),
                    ),
                    onPressed: () {
                      selectPaymentConfig(
                        context: context,
                        totalMtransport: totalMtransport,
                        total: total,
                        transport: transport,
                      );
                    },
                    child: Text(
                      'Passer la commande',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 70,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
