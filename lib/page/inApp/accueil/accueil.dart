import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tarzanexpress/helper/carousel_slider/carousel_slider.dart';
import 'package:tarzanexpress/page/inApp/commander/detailsLivraison.dart';
import 'package:tarzanexpress/page/inApp/serviceClient/serviceClient.dart';

import '../../../constants/appAssets.dart';
import '../../../constants/appColors.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../helper/utils.dart';
import '../../../model/article.dart';
import '../../../page/inApp/compte/agenciesInfos.dart';
import '../../../page/inApp/panier/addElement.dart';
import '../../../page/inApp/shop/shopWebview.dart';
import '../../../state/articleState.dart';
import '../../../widgets/customWidgets.dart';
import 'showActualiteImage.dart';

class Accueil extends StatefulWidget {
  @override
  _AccueilState createState() => _AccueilState();
}

class _AccueilState extends State<Accueil> {
  TextStyle whiteColorTextStyle = TextStyle(color: Colors.white);

  random(min, max) {
    var rn = new Random();
    return min + rn.nextInt(max - min);
  }

  Widget generateThreeImages(Size dSize) {
    List<int> nr = [
      for (var i = 0; i < 50; i++) i + 1,
    ];

    Random r1 = new Random();
    while (nr.length != 3) {
      nr.removeAt(r1.nextInt(nr.length));
    }
    Widget result = Row(
      children: [
        for (var i = 0; i < nr.length; i++)
          Padding(
            padding: EdgeInsets.only(left: 5.0),
            child: Image(
              height: dSize.height * 0.10968660,
              width: dSize.width * 0.179166,
              fit: BoxFit.fill,
              image: AssetImage(
                "assets/images/popular/${nr[i]}.jpg",
              ),
            ),
          ),
      ],
    );
    return result;
  }

  void showCustomModal({
    @required BuildContext context,
    @required List<Widget> jData,
  }) {
    showModalBottomSheet<void>(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32),
            topRight: Radius.circular(32),
          ),
        ),
        backgroundColor: Colors.white,
        builder: (BuildContext context) {
          return SingleChildScrollView(
            padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: jData,
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;

    List moyPaiement = [
      "Mobile Money",
      [
        Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ImageIcon(
                AssetImage(AppAssets.mobileMoney),
                size: 50,
              ),
              Text("MOBILE MONEY"),
            ],
          ),
        ),
        Text(
          "Vous avez la possibilité de payer vos commandes par mobile money sur un canal sécurisé directement dans l'application.\nVous pouvez aussi effectuer une transaction directe sur nos numéros marchants et envoyer la preuve de transaction au service clientèle.",
          /* style: TextStyle(
            fontSize: 16.0,
          ), */
        ),
        Image.asset("assets/flags/tg.png",
            width: dSize.width * 0.11, height: dSize.height * 0.05698),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(width: dSize.width * 0.08),
            Column(
              children: [
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Image.asset(
                      AppAssets.logoFlooz,
                      width: dSize.width * 0.16,
                      height: dSize.height * 0.08547,
                    ),
                    Text(
                      "\t(+228 98969188)",
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Image.asset(
                      AppAssets.logoTMoney,
                      width: dSize.width * 0.16,
                      height: dSize.height * 0.08547,
                    ),
                    Text(
                      "\t(+228 91548361)",
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ],
      AppAssets.mobileMoney,
      "Virement Bancaire",
      [
        Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ImageIcon(AssetImage(AppAssets.bankTransfer), size: 50),
              Text("VIREMENT BANCAIRE"),
            ],
          ),
        ),
        Text(
          "Vous avez la possibilité d'effectuer un virement bancaire et d'envoyer la preuve au service clientèle. Merci de rentrer en contact avec le service clientèle pour tout détail.",
          style: TextStyle(fontSize: 16),
        ),
      ],
      AppAssets.bankTransfer,
      "Espèces",
      [
        Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ImageIcon(AssetImage(AppAssets.especes), size: 50),
              Text("ESPÈCES"),
            ],
          ),
        ),
        Text(
          "Vous avez la possibilité de passer dans nos agences pour effectuer les paiements en espèces.",
          style: TextStyle(fontSize: 16),
        ),
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AgenciesInfos(),
              ),
            );
          },
          child: Text(
            "Voir nos agences >",
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Colors.orange,
            ),
          ),
        ),
      ],
      AppAssets.especes,
      "Chèque Bancaire",
      [
        Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ImageIcon(AssetImage(AppAssets.bankCheck), size: 50),
              Text("CHÈQUE BANCAIRE"),
            ],
          ),
        ),
        Text(
          "Nous reçevons les chèques bancaires.\nMerci de rentrer en contact avec le service clientèle pour tout détail.",
          style: TextStyle(fontSize: 16),
        ),
      ],
      AppAssets.bankCheck,
    ];
    ArticleState articleState =
        Provider.of<ArticleState>(context, listen: false);
    return RefreshIndicator(
      onRefresh: () {
        return Future.delayed(
          Duration(milliseconds: 1),
          () {
            setState(() {});
          },
        );
      },
      child: Builder(
        builder: (context) => SafeArea(
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: dSize.width * 0.03333,
                      vertical: dSize.height * 0.0118),
                  decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                  ),
                  child: Row(
                    children: [
                      FutureBuilder(
                        future: SharedPreferences.getInstance(),
                        builder: (context, d) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: fullWidth(context) * 0.93,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                          child: Text(
                                            "Bienvenue\n" +
                                                ((d.connectionState ==
                                                            ConnectionState
                                                                .done &&
                                                        d.hasData)
                                                    ? d.data
                                                        .getString("prenoms")
                                                    : '..'),
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: false,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            ElevatedButton(
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.black54)),
                                              onPressed: () => Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ServiceClient(
                                                    getQuote: true,
                                                  ),
                                                ),
                                              ),
                                              child: Text(
                                                "Obtenir un devis",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                    ],
                  ),
                ),
                //SizedBox(height: dSize.height * 0.014),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: dSize.width * 0.05,
                      vertical: dSize.height * 0.002),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: Text(
                          "Faites vos courses en cliquant sur vos boutiques préferées ici !",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          for (var i = 0; i < boutiquesImg.length; i++)
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ShopWebview(
                                      brand: boutiquesUrl[i],
                                    ),
                                  ),
                                );
                              },
                              child: Image.asset(
                                boutiquesImg[i],
                                height: dSize.height * 0.0715,
                                width: dSize.width * 0.16,
                                fit: BoxFit.fill,
                              ),
                            ),
                        ],
                      ),
                      Center(
                        child: Text(
                          "ou",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Center(
                        child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.white)),
                          onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AddElement(),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Image.asset(
                                  AppAssets.cartAdd,
                                  height: 25,
                                  width: 25,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  "Ajoutez rapidement au panier",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.brown,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: dSize.height / 40),
                      Text(
                        "Choix populaires",
                        style: TextStyle(
                          fontSize: 13.0,
                          fontWeight: FontWeight.w500,
                          //color: AppColors.indigo,
                        ),
                      ),
                      SizedBox(height: dSize.height / 70),
                      GestureDetector(
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (context) => SimpleDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25)),
                              ),
                              contentPadding:
                                  EdgeInsets.fromLTRB(12, 35, 12, 21),
                              children: <Widget>[
                                Text(
                                  "Cette fonctionnalité sera bientôt disponible !",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 14.0),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                Center(
                                  child: TextButton(
                                    onPressed: () => Navigator.pop(context),
                                    child: Text(
                                      "OK",
                                      style: TextStyle(color: Colors.orange),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            generateThreeImages(dSize),
                            Padding(
                              padding: EdgeInsets.only(left: 5.0),
                              child: Container(
                                color: Colors.grey.shade300,
                                height: dSize.height * 0.10968660,
                                width: dSize.width * 0.179166,
                                child: Center(
                                  child: Text(
                                    "Plus...",
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: dSize.height / 90,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 7),
                        child: Text(
                          "Actualités",
                          style: TextStyle(
                            fontSize: 13.0,
                            fontWeight: FontWeight.w500,
                            //color: AppColors.indigo,
                          ),
                        ),
                      ),
                      FutureBuilder(
                        future: DataConnectionChecker().hasConnection,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.data) {
                              return FutureBuilder(
                                future:
                                    articleState.getArticles(context: context),
                                builder: (context, snapsh) {
                                  if (snapsh.connectionState ==
                                      ConnectionState.done) {
                                    if (articleState.articles != null &&
                                        articleState.articles.isNotEmpty) {
                                      List<ArticleModel> carouselImages = [
                                        for (var i = 0;
                                            i < articleState.articles.length;
                                            i++)
                                          if (![null, 'null'].contains(
                                              articleState
                                                  .articles[i].photoAvantLien))
                                            articleState.articles[i]
                                      ];
                                      if (carouselImages.isNotEmpty) {
                                        return CarouselSlider(
                                          options: CarouselOptions(
                                            height: dSize.height * 0.26,
                                            aspectRatio: 2.0, //16 / 9,
                                            viewportFraction: 1, //0.8,
                                            initialPage: 0,
                                            enableInfiniteScroll: false,
                                            reverse: false,
                                            autoPlay: true,
                                            autoPlayInterval:
                                                Duration(seconds: 10),
                                            autoPlayAnimationDuration:
                                                Duration(milliseconds: 800),
                                            autoPlayCurve: Curves.fastOutSlowIn,
                                            enlargeCenterPage: true,
                                            //onPageChanged: callbackFunction,
                                            scrollDirection: Axis.horizontal,
                                          ),
                                          items: carouselImages
                                              .map((item) => ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                15.0)),
                                                    child: GestureDetector(
                                                      onTap: () =>
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder: (context) =>
                                                                    ShowActualiteImage(
                                                                        article:
                                                                            item),
                                                              )),
                                                      child: Container(
                                                        color: Colors
                                                            .grey.shade300,
                                                        width:
                                                            fullWidth(context),
                                                        height:
                                                            dSize.height * 0.26,
                                                        child: Image.network(
                                                          item.photoAvantLien
                                                                  .startsWith(
                                                                      "http")
                                                              ? item
                                                                  .photoAvantLien
                                                              : "https://" +
                                                                  item.photoAvantLien,
                                                          width: fullWidth(
                                                              context),
                                                          height: dSize.height *
                                                              0.26,
                                                          fit: BoxFit.fill,
                                                          errorBuilder:
                                                              (context, url,
                                                                      error) =>
                                                                  Icon(
                                                            Icons.error,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ))
                                              .toList(),
                                        );
                                      }
                                    }
                                    return Center(
                                      child: Text(
                                        "Aucune actualité pour le moment !",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    );
                                  }
                                  return Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      CircularProgressIndicator(
                                        valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                                AppColors.yellowAppbar),
                                      ),
                                      SizedBox(width: 5),
                                      Text(
                                        "Récupération des données..",
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    ],
                                  );
                                },
                              );
                            }
                            return Center(
                              child: Text(
                                "Connection internet faible ou inexistante !",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 15),
                              ),
                            );
                          }
                          return Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    AppColors.yellowAppbar),
                              ),
                              SizedBox(width: 5),
                              Text(
                                "Récupération des données..",
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          );
                        },
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 14),
                          Text(
                            "Nos modes de transport:",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                /* fontSize: 17.0,  */ fontWeight:
                                    FontWeight.w500),
                          ),
                          SizedBox(height: 14),
                          Row(
                            children: [
                              Expanded(
                                child: Card(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              DetailsLivraison(),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      color: Colors.white,
                                      padding: EdgeInsets.all(4),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.flight_outlined,
                                            color: Colors.teal,
                                          ),
                                          Text(
                                            "Transport Express Avion",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 4),
                              Expanded(
                                child: Card(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              DetailsLivraison(),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      color: Colors.white,
                                      padding: EdgeInsets.all(4),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.directions_ferry_outlined,
                                            color: Colors.blue,
                                          ),
                                          Text(
                                            "Transport Maritime Bateau",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Text(
                        "Nos moyens de paiement:",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            /* fontSize: 17.0, */ fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 15),
                      Wrap(
                        children: [
                          for (var i = 0; i < moyPaiement.length; i = i + 3)
                            GestureDetector(
                              onTap: () {
                                showCustomModal(
                                  context: context,
                                  jData: moyPaiement[i + 1],
                                );
                              },
                              child: Card(
                                //margin: EdgeInsets.all(2),
                                child: Container(
                                  width: dSize.width * 0.42,
                                  height: dSize.height * 0.18,
                                  padding: EdgeInsets.all(4),
                                  child: Center(
                                    child: Wrap(
                                      //mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Column(
                                          children: [
                                            ImageIcon(
                                                AssetImage(moyPaiement[i + 2]),
                                                color: [
                                                  Colors.blue.shade800,
                                                  Colors.brown,
                                                  Colors.pink.shade900,
                                                  Colors.amber,
                                                  Colors.yellow,
                                                  Colors.blueGrey,
                                                  Colors.green,
                                                  Colors.black,
                                                  Colors.orange.shade900,
                                                  Colors.redAccent
                                                ][i]),
                                            Text(
                                              moyPaiement[i],
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                fontSize: 14,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )
                        ],
                      ),
                      SizedBox(height: 25),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
