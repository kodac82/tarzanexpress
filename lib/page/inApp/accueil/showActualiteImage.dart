import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../../model/article.dart';
import '../../../widgets/customWidgets.dart';

class ShowActualiteImage extends StatefulWidget {
  final ArticleModel article;
  ShowActualiteImage({this.article});
  @override
  _ShowActualiteImageState createState() => _ShowActualiteImageState();
}

class _ShowActualiteImageState extends State<ShowActualiteImage> {
  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(widget.article.titre),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
            horizontal: 10, vertical: dSize.height * 0.0277),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ![null, 'null'].contains(widget.article.photoAvantLien)
                ? Container(
                    color: Colors.grey.shade300,
                    width: fullWidth(context),
                    height: dSize.height * 0.34722,
                    child: CachedNetworkImage(
                      height: dSize.height * 0.34722,
                      fit: BoxFit.cover,
                      imageUrl: widget.article.photoAvantLien.startsWith("http")
                          ? widget.article.photoAvantLien
                          : "https://" + widget.article.photoAvantLien,
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) {
                        return Center(
                          child: CircularProgressIndicator(
                            value: downloadProgress.progress,
                          ),
                        );
                      },
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  )
                : Container(),
            SizedBox(
              height: dSize.height * 0.034722,
            ),
            Text(
              widget.article.contenu,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
