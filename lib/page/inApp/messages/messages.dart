import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../model/order.dart';
import '../../../page/inApp/notifications/actualites.dart';
import '../../../state/messageState.dart';
import '../../../state/notificationState.dart';
import '../../../widgets/customWidgets.dart';

import '../../../constants/appAssets.dart';
import '../../../constants/appColors.dart';
import '../../../page/inApp/messages/orderConversation.dart';
import '../../../state/orderState.dart';

enum MenuOptions { Discuter, Details }

List<String> monthString = [
  "JAN",
  "FÉV",
  "MAR",
  "AVR",
  "MAI",
  "JUIN",
  "JUIL",
  "AOÛ",
  "SEP",
  "OCT",
  "NOV",
  "DÉC",
];

class Messages extends StatefulWidget {
  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {
  String day = DateTime.now().day.toString();
  String month = DateTime.now().month.toString();
  String year = DateTime.now().year.toString();
  String filterFirstDate = ((DateTime.now().month - 6 > 0)
          ? "${DateTime.now().year}"
          : "${DateTime.now().year - 1}") +
      '/' +
      ((DateTime.now().month - 6 > 0)
          ? (((DateTime.now().month - 6).toString().length > 1
              ? "${DateTime.now().month - 6}"
              : "0${DateTime.now().month - 6}"))
          : (((DateTime.now().month + 6).toString().length > 1
              ? "${DateTime.now().month + 6}"
              : "0${DateTime.now().month + 6}"))) +
      '/' +
      (DateTime.now().day.toString().length > 1
          ? "${DateTime.now().day}"
          : "0${DateTime.now().day}");
  String filterLastDate = "${DateTime.now().year}" +
      '/' +
      (DateTime.now().month.toString().length > 1
          ? "${DateTime.now().month}"
          : "0${DateTime.now().month}") +
      '/' +
      (DateTime.now().day.toString().length > 1
          ? "${DateTime.now().day}"
          : "0${DateTime.now().day}");
  String reformatDate(String s) {
    String tempDate = s.substring(0, s.indexOf(' '));
    String day = tempDate.substring(0, tempDate.indexOf('/'));
    String month = tempDate.substring(
        tempDate.indexOf('/') + 1, tempDate.lastIndexOf('/'));
    String year = tempDate.substring(tempDate.lastIndexOf('/') + 1);
    return year + '-' + month + '-' + day + s.substring(s.indexOf(' '));
  }

  @override
  Widget build(BuildContext context) {
    Size dSize = MediaQuery.of(context).size;
    Provider.of<NotificationState>(context, listen: false).getUnreadNumber(
      context: context,
    );
    Provider.of<MessageState>(context, listen: false).unreadMessages(
      context: context,
    );
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.scaffoldBackground,
        body: RefreshIndicator(
          onRefresh: () {
            return Future.delayed(
              Duration(seconds: 1),
              () {
                setState(() {});
              },
            );
          },
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(5, 17, 5, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "COMMANDES",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 15.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Actualites(),
                            ));
                      },
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Badge(
                            animationType: BadgeAnimationType.slide,
                            badgeColor: Colors.white,
                            borderSide: BorderSide(color: Colors.blueAccent),
                            elevation: 0,
                            position: BadgePosition.topEnd(end: 0, top: -20),
                            badgeContent: FutureBuilder(
                              future: Provider.of<NotificationState>(context,
                                      listen: false)
                                  .getUnreadNumber(
                                context: context,
                              ),
                              builder: (context, qsnap) {
                                if (qsnap.connectionState ==
                                    ConnectionState.done) {
                                  if (qsnap.data[0]) {
                                    return Padding(
                                      padding: EdgeInsets.all(3.6),
                                      child: Text(
                                        qsnap.data[1].toString(),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    );
                                  }
                                }
                                return Padding(
                                  padding: EdgeInsets.all(7.2),
                                  child: Text(
                                    '..',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                );
                              },
                            ),
                            child: Text(
                              "NOTIFICATIONS",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                //color: Colors.orange,
                                fontSize: 15.0,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              FutureBuilder(
                future: DataConnectionChecker().hasConnection,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.data) {
                      return FutureBuilder(
                        future: Provider.of<OrderState>(context, listen: false)
                            .getOrders(context: context),
                        builder: (context, a) {
                          List<Widget> elements = [];
                          if (a.connectionState == ConnectionState.done) {
                            if (!a.data[0]) {
                              return Expanded(
                                child: Container(
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(Icons.link_off),
                                          Text(
                                            "Une erreur s'est produite. Notre équipe est en train de résoudre le problème . Veuillez réessayer plutard.",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 20),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            } else {
                              if (a.data[1].length != 0) {
                                DateTime start = DateTime.parse(
                                  filterFirstDate.replaceAll('/', '-') +
                                      " 00:00:00",
                                );
                                DateTime end = DateTime.parse(
                                  filterLastDate.replaceAll('/', '-') +
                                      " 23:59:59",
                                );
                                List<Order> firstFiltered = [];
                                List<Order> lastFiltered = [];
                                for (var i = 0; i < a.data[1].length; i++) {
                                  int result = start.compareTo(DateTime.parse(
                                    reformatDate(
                                      a.data[1][i]
                                          .detailsCommande["date_commande"],
                                    ),
                                  ));
                                  if (result < 1 || result == 0) {
                                    firstFiltered.add(a.data[1][i]);
                                  }
                                }
                                for (var i = 0; i < firstFiltered.length; i++) {
                                  int result = end.compareTo(DateTime.parse(
                                    reformatDate(
                                      firstFiltered[i]
                                          .detailsCommande["date_commande"],
                                    ),
                                  ));
                                  if (result > -1 || result == 0) {
                                    lastFiltered.add(firstFiltered[i]);
                                  }
                                }
                                elements = [
                                  for (var i = 0; i < lastFiltered.length; i++)
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 2),
                                      child: GestureDetector(
                                        onTap: () async {
                                          bool reponse = await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  OrderConversation(
                                                calledFromMessages: true,
                                                id: lastFiltered[i].id,
                                                reference:
                                                    lastFiltered[i].reference,
                                              ),
                                            ),
                                          );
                                          if ((reponse != null) && reponse) {
                                            setState(() {});
                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Provider.of<MessageState>(
                                                          context,
                                                          listen: true)
                                                      .unreadMap
                                                      .keys
                                                      .contains(
                                                          "${lastFiltered[i].id}")
                                                  ? Colors.lightBlue.shade100
                                                  : Colors.white,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(7))),
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 20, vertical: 12),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Flexible(
                                                    child: Text(
                                                      lastFiltered[i].reference,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 15,
                                                      ),
                                                    ),
                                                  ),
                                                  Provider.of<MessageState>(
                                                              context,
                                                              listen: true)
                                                          .unreadMap
                                                          .keys
                                                          .contains(
                                                              "${lastFiltered[i].id}")
                                                      ? Container(
                                                          padding:
                                                              EdgeInsets.all(8),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Colors.white,
                                                            shape:
                                                                BoxShape.circle,
                                                          ),
                                                          child: Text(
                                                            Provider.of<MessageState>(
                                                                    context,
                                                                    listen:
                                                                        true)
                                                                .unreadMap[
                                                                    "${lastFiltered[i].id}"]
                                                                .toString(),
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .redAccent,
                                                                fontSize: 17.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                          ),
                                                        )
                                                      : Container()
                                                ],
                                              ),
                                              Text(
                                                "${lastFiltered[i].statut["param1"]}",
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w600,
                                                  fontStyle: FontStyle.italic,
                                                ),
                                              ),
                                              Text(
                                                "${lastFiltered[i].detailsCommande["date_commande"]}",
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontSize: 15,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                ];
                                return Flexible(
                                  child: ListView(
                                    padding: EdgeInsets.fromLTRB(5, 0, 5, 10),
                                    /* crossAxisAlignment:
                                        CrossAxisAlignment.start, */
                                    children: [
                                      Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 0, 0, 5),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            GestureDetector(
                                              child: Text(
                                                (filterFirstDate.substring(
                                                        filterFirstDate.lastIndexOf(
                                                                '/') +
                                                            1)) +
                                                    ' ' +
                                                    monthString[int.parse(
                                                            ((filterFirstDate.substring(filterFirstDate.indexOf('/')))
                                                                    .substring(
                                                                        1,
                                                                        filterFirstDate.indexOf('/') -
                                                                            1))
                                                                .toString()) -
                                                        1] +
                                                    ' ' +
                                                    (filterFirstDate.substring(
                                                        0,
                                                        filterFirstDate
                                                            .indexOf('/'))),
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  color: AppColors.indigo,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                              onTap: () async {
                                                DateTime tempDate;
                                                await showCupertinoModalPopup(
                                                  context: context,
                                                  builder: (context) =>
                                                      CupertinoActionSheet(
                                                    actions: [
                                                      SizedBox(
                                                        height: dSize.height *
                                                            0.256,
                                                        child:
                                                            CupertinoDatePicker(
                                                          minimumYear: 2019,
                                                          maximumDate: DateTime(
                                                              DateTime.now()
                                                                  .year,
                                                              DateTime.now()
                                                                  .month,
                                                              DateTime.now()
                                                                      .day -
                                                                  1),
                                                          initialDateTime: filterFirstDate !=
                                                                  null
                                                              ? (DateTime(
                                                                  int.parse(
                                                                    filterFirstDate
                                                                        .substring(
                                                                      0,
                                                                      filterFirstDate
                                                                          .indexOf(
                                                                              '/'),
                                                                    ),
                                                                  ),
                                                                  int.parse(((filterFirstDate.substring(filterFirstDate.indexOf(
                                                                              '/')))
                                                                          .substring(
                                                                              1,
                                                                              filterFirstDate.indexOf('/') - 1))
                                                                      .toString()),
                                                                  int.parse(
                                                                    filterFirstDate
                                                                        .substring(
                                                                            filterFirstDate.lastIndexOf('/') +
                                                                                1),
                                                                  ),
                                                                ))
                                                              : DateTime(
                                                                  DateTime.now()
                                                                      .year,
                                                                  DateTime.now()
                                                                      .month,
                                                                  DateTime.now()
                                                                          .day -
                                                                      1),
                                                          mode:
                                                              CupertinoDatePickerMode
                                                                  .date,
                                                          onDateTimeChanged:
                                                              (value) =>
                                                                  tempDate =
                                                                      value,
                                                        ),
                                                      ),
                                                    ],
                                                    cancelButton:
                                                        CupertinoActionSheetAction(
                                                      child: Text('OK'),
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                    ),
                                                  ),
                                                );
                                                if (tempDate != null)
                                                  setState(
                                                    () {
                                                      filterFirstDate = tempDate
                                                              .year
                                                              .toString() +
                                                          (tempDate.month
                                                                      .toString()
                                                                      .length >
                                                                  1
                                                              ? "/${tempDate.month}"
                                                              : "/0${tempDate.month}") +
                                                          (tempDate.day
                                                                      .toString()
                                                                      .length >
                                                                  1
                                                              ? "/${tempDate.day}"
                                                              : "/0${tempDate.day}");
                                                    },
                                                  );
                                              },
                                            ),
                                            Text(
                                              "À",
                                              style: TextStyle(fontSize: 15),
                                            ),
                                            GestureDetector(
                                              child: Text(
                                                (filterLastDate.substring(
                                                        filterLastDate.lastIndexOf(
                                                                '/') +
                                                            1)) +
                                                    ' ' +
                                                    monthString[int.parse(
                                                            ((filterLastDate.substring(filterLastDate.indexOf('/')))
                                                                    .substring(
                                                                        1,
                                                                        filterLastDate.indexOf('/') -
                                                                            1))
                                                                .toString()) -
                                                        1] +
                                                    ' ' +
                                                    (filterLastDate.substring(
                                                        0,
                                                        filterLastDate
                                                            .indexOf('/'))),
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  color: AppColors.indigo,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                              onTap: () async {
                                                DateTime tempDate;
                                                await showCupertinoModalPopup(
                                                  context: context,
                                                  builder: (context) =>
                                                      CupertinoActionSheet(
                                                    actions: [
                                                      SizedBox(
                                                        height: dSize.height *
                                                            0.256,
                                                        child:
                                                            CupertinoDatePicker(
                                                          minimumYear: 2019,
                                                          maximumDate: DateTime(
                                                              DateTime.now()
                                                                  .year,
                                                              DateTime.now()
                                                                  .month,
                                                              DateTime.now()
                                                                  .day),
                                                          initialDateTime: filterLastDate !=
                                                                  null
                                                              ? (DateTime(
                                                                  int.parse(
                                                                    filterLastDate
                                                                        .substring(
                                                                      0,
                                                                      filterLastDate
                                                                          .indexOf(
                                                                              '/'),
                                                                    ),
                                                                  ),
                                                                  int.parse(((filterLastDate.substring(filterLastDate.indexOf(
                                                                              '/')))
                                                                          .substring(
                                                                              1,
                                                                              filterLastDate.indexOf('/') - 1))
                                                                      .toString()),
                                                                  int.parse(
                                                                    filterLastDate
                                                                        .substring(
                                                                            filterLastDate.lastIndexOf('/') +
                                                                                1),
                                                                  ),
                                                                ))
                                                              : DateTime(
                                                                  DateTime.now()
                                                                      .year,
                                                                  DateTime.now()
                                                                      .month,
                                                                  DateTime.now()
                                                                      .day),
                                                          mode:
                                                              CupertinoDatePickerMode
                                                                  .date,
                                                          onDateTimeChanged:
                                                              (value) =>
                                                                  tempDate =
                                                                      value,
                                                        ),
                                                      ),
                                                    ],
                                                    cancelButton:
                                                        CupertinoActionSheetAction(
                                                      child: Text('OK'),
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                    ),
                                                  ),
                                                );
                                                if (tempDate != null)
                                                  setState(() {
                                                    filterLastDate = tempDate
                                                            .year
                                                            .toString() +
                                                        (tempDate.month
                                                                    .toString()
                                                                    .length >
                                                                1
                                                            ? "/${tempDate.month}"
                                                            : "/0${tempDate.month}") +
                                                        (tempDate.day
                                                                    .toString()
                                                                    .length >
                                                                1
                                                            ? "/${tempDate.day}"
                                                            : "/0${tempDate.day}");
                                                  });
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      if (elements.isNotEmpty) ...elements,
                                      if (elements.isEmpty)
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: fullHeight(context) / 4),
                                          child: Center(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 35),
                                                  child: Text(
                                                    "AUCUNE COMMANDE TROUVÉE",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 19),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                );
                              } else {
                                return Expanded(
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          width: 60,
                                          height: 60,
                                          image:
                                              AssetImage(AppAssets.orderEmpty),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 35),
                                          child: Text(
                                            "AUCUNE COMMANDE ENREGISTREE",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 19),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }
                            }
                          }
                          return Expanded(
                            child: Center(
                              child: Container(
                                padding: EdgeInsets.all(10),
                                color: Colors.white,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    CircularProgressIndicator(),
                                    SizedBox(width: 10),
                                    Text("Récupération des données ..")
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    }
                    return Expanded(
                      child: Container(
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.wifi_off),
                                Text(
                                  "Connection internet faible ou inexistante.",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 20),
                                ),
                                ElevatedButton(
                                  onPressed: () => setState(() {}),
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      Colors.blue,
                                    ),
                                  ),
                                  child: Text(
                                    "RÉESSAYER",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                  return Expanded(
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        color: Colors.white,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CircularProgressIndicator(),
                            SizedBox(width: 10),
                            Text("Récupération des données ..")
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
