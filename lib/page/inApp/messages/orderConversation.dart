import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../constants/appColors.dart';
import '../../../helper/dataConnectionChecker.dart';
import '../../../state/messageState.dart';
import '../../../widgets/customWidgets.dart';

var orderConversationScaffoldKey = GlobalKey<ScaffoldState>();

class OrderConversation extends StatefulWidget {
  final String id;
  final String reference;
  final bool calledFromMessages;
  OrderConversation({
    @required this.id,
    @required this.reference,
    this.calledFromMessages,
  });
  @override
  _OrderConversationState createState() => _OrderConversationState();
}

class _OrderConversationState extends State<OrderConversation> {
  final _formKey = GlobalKey<FormState>();
  String contenu;

  List<String> monthString = [
    "JANVIER",
    "FÉVRIER",
    "MARS",
    "AVRIL",
    "MAI",
    "JUIN",
    "JUILLET",
    "AOÛT",
    "SEPTEMBRE",
    "OCTOBRE",
    "NOVEMBRE",
    "DÉCEMBRE",
  ];

  Future<File> handleChooseFromGallery() async {
    return File(
        (await ImagePicker().getImage(source: ImageSource.gallery)).path);
  }

  @override
  void initState() {
    SharedPreferences.getInstance().then((mPrefs) {
      mPrefs.setBool("isInside${widget.id}", true);
    });
    super.initState();
  }

  @override
  void dispose() {
    SharedPreferences.getInstance().then((mPrefs) {
      mPrefs.remove("isInside${widget.id}");
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MessageState b = Provider.of<MessageState>(context, listen: false);
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        key: orderConversationScaffoldKey,
        backgroundColor: AppColors.scaffoldBackground,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, true),
          ),
          title: Text(widget.reference),
        ),
        body: FutureBuilder(
          future: (b.commandeWhichFetchingId == widget.id && b.messages != null)
              ? Future.delayed(Duration(milliseconds: 1))
              : b.getMessages(context: context, id: widget.id),
          builder: (context, snap) {
            if (snap.connectionState == ConnectionState.done) {
              return Consumer<MessageState>(
                builder: (context, a, _) {
                  DateTime tempDate = DateTime.parse("1900-01-01");
                  compareDateToPrevious(String tDateString) {
                    tDateString = tDateString.replaceAll('/', '-');
                    DateTime tDate = DateTime.parse(tDateString);
                    tDate = DateTime(tDate.year, tDate.month, tDate.day);
                    if (!(tDate.compareTo(tempDate) < 1)) {
                      tempDate = tDate;
                      return true;
                    }
                    return false;
                  }

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (a.messages != null && a.messages.length != 0)
                        Expanded(
                          child: SingleChildScrollView(
                            reverse: true,
                            physics: ScrollPhysics(),
                            padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
                            child: Column(
                              children: [
                                if (a.messages.length != 0)
                                  for (var i = 0; i < a.messages.length; i++)
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5),
                                      child: Column(
                                        children: [
                                          Center(
                                            child: !compareDateToPrevious(
                                                    a.messages[i].date)
                                                ? Container()
                                                : Padding(
                                                    padding: const EdgeInsets
                                                        .symmetric(vertical: 8),
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              vertical: 5,
                                                              horizontal: 5),
                                                      decoration: BoxDecoration(
                                                        color: AppColors
                                                            .yellowAppbar,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      child: Text(
                                                        tempDate.day
                                                                .toString() +
                                                            ' ' +
                                                            monthString[
                                                                tempDate.month -
                                                                    1] +
                                                            ' ' +
                                                            tempDate.year
                                                                .toString(),
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                                    ),
                                                  ),
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                (a.messages[i].isAdmin == "1")
                                                    ? CrossAxisAlignment.start
                                                    : CrossAxisAlignment.end,
                                            children: [
                                              (a.messages[i].isAdmin == "1")
                                                  ? Row(
                                                      children: [
                                                        Text(
                                                          "TarzanExpress ADMIN",
                                                          style: TextStyle(
                                                            color: AppColors
                                                                .drawerTitle,
                                                            fontSize: 14,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : Container(),
                                              (a.messages[i].image != "null" &&
                                                      a.messages[i].image !=
                                                          null)
                                                  ? Row(
                                                      mainAxisAlignment: (a
                                                                  .messages[i]
                                                                  .isAdmin ==
                                                              "1")
                                                          ? MainAxisAlignment
                                                              .start
                                                          : MainAxisAlignment
                                                              .end,
                                                      children: [
                                                        GestureDetector(
                                                          onTap: () =>
                                                              Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder: (context) =>
                                                                  ViewCachedImageMessage(
                                                                imageUrl: a
                                                                    .messages[i]
                                                                    .image,
                                                                sender: a
                                                                    .messages[i]
                                                                    .isAdmin,
                                                                date: a
                                                                    .messages[i]
                                                                    .date,
                                                              ),
                                                            ),
                                                          ),
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color:
                                                                  Colors.white,
                                                              borderRadius: BorderRadius
                                                                  .all(Radius
                                                                      .circular(
                                                                          12)),
                                                            ),
                                                            child: SizedBox(
                                                              height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height *
                                                                  0.2849,
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width *
                                                                  0.5555,
                                                              child:
                                                                  CachedNetworkImage(
                                                                imageUrl: a
                                                                        .messages[
                                                                            i]
                                                                        .image
                                                                        .startsWith(
                                                                            "http")
                                                                    ? a
                                                                        .messages[
                                                                            i]
                                                                        .image
                                                                    : "https://" +
                                                                        a.messages[i]
                                                                            .image,
                                                                progressIndicatorBuilder:
                                                                    (context,
                                                                        url,
                                                                        downloadProgress) {
                                                                  return Center(
                                                                    child:
                                                                        CircularProgressIndicator(
                                                                      value: downloadProgress
                                                                          .progress,
                                                                    ),
                                                                  );
                                                                },
                                                                errorWidget: (context,
                                                                        url,
                                                                        error) =>
                                                                    Icon(Icons
                                                                        .error),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : Container(
                                                      margin: EdgeInsets.only(
                                                        left: (((a.messages[i]
                                                                    .isAdmin ==
                                                                "1")
                                                            ? 0
                                                            : 40)),
                                                        right: (((a.messages[i]
                                                                    .isAdmin ==
                                                                "1")
                                                            ? 40
                                                            : 0)),
                                                      ),
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  10.0),
                                                          topRight:
                                                              Radius.circular(
                                                                  10.0),
                                                          bottomLeft: (a
                                                                      .messages[
                                                                          i]
                                                                      .isAdmin ==
                                                                  "1")
                                                              ? Radius.circular(
                                                                  0.0)
                                                              : Radius.circular(
                                                                  10.0),
                                                          bottomRight: (a
                                                                      .messages[
                                                                          i]
                                                                      .isAdmin ==
                                                                  "1")
                                                              ? Radius.circular(
                                                                  10.0)
                                                              : Radius.circular(
                                                                  0.0),
                                                        ),
                                                        color: (a.messages[i]
                                                                    .isAdmin ==
                                                                "1")
                                                            ? AppColors
                                                                .imageUploadBackgroundBorder
                                                            : AppColors.indigo,
                                                      ),
                                                      child: Text(
                                                        a.messages[i].contenu,
                                                        style: TextStyle(
                                                          fontSize: 15,
                                                          color: (a.messages[i]
                                                                      .isAdmin ==
                                                                  "1")
                                                              ? Colors.black
                                                              : Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                              SizedBox(height: 5),
                                              Row(
                                                mainAxisAlignment: (a
                                                            .messages[i]
                                                            .isAdmin ==
                                                        "1")
                                                    ? MainAxisAlignment.start
                                                    : MainAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    a.messages[i].date
                                                        .substring(
                                                            a.messages[i].date
                                                                .indexOf(' '),
                                                            a.messages[i].date
                                                                .lastIndexOf(
                                                                    ':')),
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                Visibility(
                                  visible: a.isSending,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(12),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(10.0),
                                                  topRight:
                                                      Radius.circular(10.0),
                                                  bottomLeft:
                                                      Radius.circular(10.0),
                                                  bottomRight:
                                                      Radius.circular(0.0),
                                                ),
                                                color: Colors.white,
                                              ),
                                              child: Text(
                                                "En cours d'envoi..",
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    color: Colors.black),
                                              ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(""),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                              ],
                            ),
                          ),
                        ),
                      if (a.messages != null && a.messages.length == 0)
                        Expanded(
                          child: Container(
                            child: Center(
                              child: Text(
                                "Aucun message trouvé",
                                style: TextStyle(fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      Form(
                        key: _formKey,
                        child: Container(
                          color: Colors.white,
                          padding: EdgeInsets.fromLTRB(8, 10, 8, 10),
                          child: Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.photo_camera),
                                onPressed: () async {
                                  File image = await handleChooseFromGallery();
                                  if (image != null) {
                                    await a.postMessage(
                                      context: context,
                                      id: widget.id,
                                      image: image,
                                      contenu: null,
                                    );
                                  }
                                },
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.newline,
                                  decoration: InputDecoration(
                                    hintText: "Message",
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  ),
                                  validator: (val) {
                                    if (val == null || val.isEmpty) {
                                      return " ";
                                    }
                                    return null;
                                  },
                                  onSaved: (val) {
                                    contenu = val;
                                  },
                                ),
                              ),
                              SizedBox(width: 8),
                              Container(
                                decoration: BoxDecoration(
                                  color: AppColors.green,
                                  shape: BoxShape.circle,
                                ),
                                child: IconButton(
                                  onPressed: () async {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      if (await DataConnectionChecker()
                                          .hasConnection) {
                                        _formKey.currentState.reset();
                                        await a.postMessage(
                                          context: context,
                                          id: widget.id,
                                          contenu: contenu,
                                          image: null,
                                        );
                                      } else {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          SnackBar(
                                            content: Text(
                                              "Assurez vous d'avoir une bonne connection et réessayez",
                                              textAlign: TextAlign.center,
                                            ),
                                            behavior: SnackBarBehavior.floating,
                                            margin: EdgeInsets.fromLTRB(
                                                15, 0, 15, 30),
                                          ),
                                        );
                                      }
                                    }
                                  },
                                  icon: Icon(
                                    Icons.send,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                },
              );
            }
            return Center(
              child: Container(
                padding: EdgeInsets.all(10),
                color: Colors.white,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(width: 10),
                    Text("Récupération des données ..")
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class ViewCachedImageMessage extends StatelessWidget {
  final String imageUrl;
  final String sender;
  final String date;
  ViewCachedImageMessage({
    @required this.imageUrl,
    @required this.sender,
    @required this.date,
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.yellowAppbar,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              sender == "1" ? "TarzanExpress ADMIN" : "Vous",
              style: TextStyle(fontSize: 15),
            ),
            Text(
              date,
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
      ),
      body: Container(
        height: fullHeight(context),
        width: fullWidth(context),
        child: CachedNetworkImage(
          imageUrl:
              imageUrl.startsWith("http") ? imageUrl : "http://" + imageUrl,
          fit: BoxFit.contain,
          progressIndicatorBuilder: (context, url, downloadProgress) {
            return Center(
              child: CircularProgressIndicator(
                value: downloadProgress.progress,
              ),
            );
          },
        ),
      ),
    );
  }
}
