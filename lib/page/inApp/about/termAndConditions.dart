import 'package:flutter/material.dart';
import '../../../constants/termsAndConditionsList.dart';

class TermsAndConditions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "TERMES ET CONDITONS ",
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.orange),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(14, 14, 14, 25),
        child: Container(
          padding: EdgeInsets.fromLTRB(13, 30, 13, 0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Dernières mises à jour 2021, Version 1.1.0",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
              SizedBox(height: 20),
              Card(
                elevation: 6,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Bienvenue sur TarzanExpress\n",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        "Tarzan d’Afrique Express SARL et leurs sociétés affiliées (« TarzanExpress ») vous fournissent  vous fournit des fonctionnalités de site internet et service en ligne lorsque vous visitez ses plateformes https://app .tarzan-express.com et l’application mobile disponible sur PlayStore.\n",
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 25),
              for (var i = 0; i < articlesPart1.length; i = i + 2)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      articlesPart1[i],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w600),
                    ),
                    Text(
                      articlesPart1[i + 1],
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.5,
                        fontFamily: 'Poppins',
                      ),
                    ),
                  ],
                ),
              Text(
                "\nArticle 4 : DELAI DE LIVRAISON ET SUIVI DE LA COMMANDE\n",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                "Nos délais de livraison sont détaillés comme suit :\n",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.5,
                  fontFamily: 'Poppins',
                ),
              ),
              Text(
                "\tÉtape 1 : Processus",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                "\t\t- Exécution de la commande auprès du fournisseur : 1 à 5 jours ouvrés\n\t\t - Livraison du fournisseur à l’agence départ : 1 à 2 jours ouvrés\n\t\t - Préparation du colis et livraison de l’agence départ au Cargo : 1 à 2 jours Ouvrés",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.5,
                  fontFamily: 'Poppins',
                ),
              ),
              Text(
                "\tÉtape 2 : Livraison",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                "\t\t- Transport du colis à destination 5 à 15 jours Ouvrés (Livraison Express) et 45 à 60 jours ouvrés (Livraison par bateau)\n\t\t - Livraison à l’agence destination et disponibilité du colis à être à livrer au client",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.5,
                  fontFamily: 'Poppins',
                ),
              ),
              Text(
                "Vous pouvez avoir plus de détails sur  \"Mon compte\" et cliquez sur \"Mes commandes\". Vous pouvez retrouver l'historique de votre commande et son statut.\n\nCliquez sur le bouton \"Afficher les détails\"\n\nSi votre commande est « expédiée », cliquez sur le bouton « Suivi » pour vérifier l'état actuel de votre colis et trouver la position exacte du colis.\n\nTout retard ou annulation de la commande liée à l’utilisateur sont exclus en cas de retard de livraison.",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.5,
                  fontFamily: 'Poppins',
                ),
              ),
              for (var i = 0; i < articlesPart2.length; i = i + 2)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      articlesPart2[i],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w600),
                    ),
                    Text(
                      articlesPart2[i + 1],
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.5,
                        fontFamily: 'Poppins',
                      ),
                    ),
                  ],
                ),
              Table(
                border: TableBorder(
                  horizontalInside: BorderSide(width: 1),
                  verticalInside: BorderSide(width: 1),
                ),
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                children: [
                  TableRow(
                    children: [
                      Text(
                        "Pourcentage à prélever",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "Intervalle montant de la commande (Montant total des articles)",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "Détails",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          "20%",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          "1 à 50 000 F CFA",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Column(
                          children: [
                            Text(
                              " -- Livraison Fournisseur – Agence 0,05 %\n\n -- Transport Agence – Cargo : 1%\n\n -- Contrôle et Sécurité 1%\n\nDouane 8%\n\nTransport cargo – Agence 0,05%\n\nCommission 9%",
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          "15%",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          "Supérieur à 50 001",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          " -- Livraison Fournisseur – Agence 0,05 %\n\n -- Transport Agence – Cargo : 1%\n\n - Contrôle et Sécurité 1%\n\n -- Douane 5%\n\n -- Transport cargo – Agence 0,05%\n\n -- Commission 7%",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              for (var i = 0; i < articlesPart3.length; i = i + 2)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      articlesPart3[i],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w600),
                    ),
                    Text(
                      articlesPart3[i + 1],
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.5,
                        fontFamily: 'Poppins',
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}
