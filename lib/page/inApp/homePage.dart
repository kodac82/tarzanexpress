import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../constants/appAssets.dart';
import '../../helper/dataConnectionChecker.dart';
import '../../page/inApp/panier/addElement.dart';
import '../../state/messageState.dart';
import '../../state/notificationState.dart';
import '../../state/panierState.dart';

import '../../page/inApp/compte/compte.dart';
import '../../page/inApp/messages/messages.dart';
import 'accueil/accueil.dart';
import 'panier/panier.dart';

class HomePage extends StatefulWidget {
  final int index;
  HomePage({this.index});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    Accueil(),
    Messages(),
    Panier(calledFromHomePage: true),
    Compte(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    if (widget.index != null) {
      if (mounted) {
        setState(() {
          _selectedIndex = widget.index;
        });
      } else {
        _selectedIndex = widget.index;
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      SharedPreferences.getInstance().then((yuPrefs) {
        if (yuPrefs.containsKey("intent")) {
          String dIntent = yuPrefs.getString("intent");
          yuPrefs.remove("intent");
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddElement(
                  intentText: dIntent,
                ),
              ));
        }
      });
    });
    Provider.of<NotificationState>(context, listen: false).getUnreadNumber(
      context: context,
    );
    return WillPopScope(
      onWillPop: () {
        if (_selectedIndex != 0) {
          setState(() {
            _selectedIndex = 0;
          });
          return Future.value(false);
        }
        return Future.value(true);
      },
      child: Scaffold(
        appBar: _selectedIndex == 0
            ? null
            : AppBar(
                elevation: 1,
                automaticallyImplyLeading: false,
                title: Text(
                  ['MESSAGES', 'PANIER', 'MON COMPTE'][_selectedIndex - 1],
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                centerTitle: true,
              ),
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: CupertinoTabBar(
          backgroundColor: Colors.white,
          activeColor: Colors.amber[800],
          iconSize: 30,
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          inactiveColor: Colors.black,
          items: [
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage(AppAssets.homeIcon)),
              label: 'ACCUEIL',
            ),
            BottomNavigationBarItem(
              icon: Badge(
                animationType: BadgeAnimationType.slide,
                badgeColor: Colors.orange,
                elevation: 0,
                position: BadgePosition.topStart(top: -8, start: 20),
                badgeContent: FutureBuilder(
                    future: DataConnectionChecker().hasConnection,
                    builder: (context, snapshot) {
                      if ((snapshot.connectionState == ConnectionState.done) &&
                          snapshot.data) {
                        return FutureBuilder(
                          future:
                              Provider.of<MessageState>(context, listen: false)
                                  .unreadMessages(context: context),
                          builder: (context, snap) => Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Text(
                              ((snap.connectionState == ConnectionState.done)
                                  ? (snap.data[0]
                                      ? (((int.tryParse(snap.data[1].toString()) ?? 0) +
                                                  (int.tryParse(
                                                          Provider.of<NotificationState>(
                                                                  context,
                                                                  listen: true)
                                                              .unread
                                                              .toString()) ??
                                                      0)) <
                                              99
                                          ? (((int.tryParse(snap.data[1].toString()) ?? 0) +
                                                  (int.tryParse(
                                                          Provider.of<NotificationState>(
                                                                  context,
                                                                  listen: true)
                                                              .unread
                                                              .toString()) ??
                                                      0))
                                              .toString())
                                          : "+99")
                                      : '0')
                                  : '..'),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        );
                      }
                      return Text(
                        '..',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w500),
                      );
                    }),
                child: ImageIcon(AssetImage(AppAssets.messageIcon)),
              ),
              label: 'MESSAGES',
            ),
            BottomNavigationBarItem(
              icon: Badge(
                animationType: BadgeAnimationType.slide,
                badgeColor: Colors.orange,
                elevation: 0,
                position: BadgePosition.topStart(top: -8, start: 20),
                badgeContent: Consumer<PanierState>(
                  builder: (context, value, _) => Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Text(
                      (value.contentPanier != null
                          ? value.contentPanier.length.toString()
                          : '0'),
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                child: ImageIcon(
                  AssetImage(AppAssets.cartIcon),
                ),
              ),
              label: 'PANIER',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage(AppAssets.profileIcon)),
              label: 'MOI',
            ),
          ],
        ),
      ),
    );
  }
}
