import 'package:flutter/material.dart';
import 'package:tarzanexpress/constants/appAssets.dart';
import 'package:tarzanexpress/widgets/customWidgets.dart';

class DetailsLivraison extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //TextStyle smallText = TextStyle(fontSize: 13);
    return Scaffold(
      appBar: AppBar(
        title: Text("Détails"),
      ),
      body: SingleChildScrollView(
        //padding: EdgeInsets.all(12),
        child: Column(
          children: [
            /* Text(
              "NB:\nLe délai de livraison estimé commence à partir de la date d'expédition plutôt que la date de la commande et peut prendre plus de temps que prévu en raison de rupture de stock auprès du fournisseur, procédures de dédouanement ou d'autres causes.",
              style: smallText,
            ),
            Text(
              "Délai de réception = Délai de processus + Délai de livraison",
              style: TextStyle(color: Colors.redAccent),
            ),
            Table(
              border: TableBorder(
                horizontalInside: BorderSide(width: 1),
                verticalInside: BorderSide(width: 1),
              ),
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: [
                TableRow(
                  children: [
                    Text(
                      "Délai de Processus",
                      textAlign: TextAlign.center,
                      style: smallText,
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      //color: Colors.redAccent,
                      child: Column(
                        children: [
                          Text(
                            "Éxecution Commande Fournisseur [ 1-5 jours ouvrés ]\n",
                            style: smallText,
                          ),
                          Text(
                            "Du Fournisseur à l'agence Départ [ 1-2 jours ouvrés ]\n",
                            style: smallText,
                          ),
                          Text(
                            "De l'agence Départ au Cargo [ 1-2 jours ouvrés ]",
                            style: smallText,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: [
                    Text(
                      "Délai de Livraison",
                      textAlign: TextAlign.center,
                      style: smallText,
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      //color: Colors.yellowAccent,
                      child: Column(
                        children: [
                          Text(
                            "Transport Colis à Destination [ 5-15 jours ouvrés ] (Avion) et 45-60 jours ouvrés (Bateau)\n",
                            style: smallText,
                          ),
                          Text(
                            "Livraison à l'agence Déstination [ 1 jour ouvré ]",
                            style: smallText,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ), */
            Image.asset(
              AppAssets.detailsLivraison,
              fit: BoxFit.fill,
              //height: fullHeight(context),
              width: fullWidth(context),
            ),
          ],
        ),
      ),
    );
  }
}
