import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:tarzanexpress/page/inApp/commander/detailsLivraison.dart';

import '../../../constants/appColors.dart';
import '../../../constants/infosLivraison.dart';
import '../../../model/adresse.dart';
import '../../../model/agencesLivraison.dart';
import '../../../page/inApp/commandes/orderDetails.dart';
import '../../../page/inApp/compte/userAdresses.dart';
import '../../../services/notificationService.dart';
import '../../../state/newOrderState.dart';
import '../../../state/panierState.dart';

class NewOrder extends StatefulWidget {
  final List<AgenceLivraison> agences;
  final List<int> modesId;
  NewOrder({@required this.agences, @required this.modesId});
  @override
  _NewOrderState createState() => _NewOrderState();
}

class _NewOrderState extends State<NewOrder> {
  final _formKey = GlobalKey<FormState>();
  bool showSpinner = false;
  ModeLivraison modeChoosed;
  AdresseLivraisonType livraisonChoosed;
  String paysChoosed;
  String villeChoosed;
  String agenceChoosed;
  int agenceChoosedId;
  String paysA = "";
  String ville = "";
  String quartier = "";
  String adresse1 = "";
  String adresse2 = "";

  List detailsAdresse = [
    "Ville",
    "Ville de livraison",
    "Quartier",
    "Quartier",
    "Adresse",
    "Détails sur l'adresse",
    "Autres détails",
    "Donnez une autre précision"
  ];

  handleChooseFromGallery({int index, NewOrderState state}) async {
    File tempFile =
        File((await ImagePicker().getImage(source: ImageSource.gallery)).path);
    if (tempFile != null) {
      state.modifyNewProductsElement(
        index: index,
        image: base64Encode(tempFile.readAsBytesSync()),
      );
    }
  }

  List getPaysList() {
    List<String> pays = [];
    for (var i = 0; i < widget.agences.length; i++) {
      if (!pays.contains(widget.agences[i].nomPays))
        pays.add(widget.agences[i].nomPays);
    }
    return pays;
  }

  List getVillesList() {
    List<String> villes = [];
    for (var i = 0; i < widget.agences.length; i++) {
      if (widget.agences[i].nomPays == paysChoosed &&
          (!villes.contains(widget.agences[i].nomVille)))
        villes.add(widget.agences[i].nomVille);
    }
    return villes;
  }

  List getAgences() {
    List<String> agences = [];
    for (var i = 0; i < widget.agences.length; i++) {
      if (widget.agences[i].nomPays == paysChoosed &&
          (widget.agences[i].nomVille == villeChoosed) &&
          (!agences.contains(widget.agences[i].nomAgence))) {
        agences.add(widget.agences[i].nomAgence);
      }
    }
    return agences;
  }

  List getAgencesId() {
    List<int> agencesId = [];
    for (var i = 0; i < widget.agences.length; i++) {
      if (widget.agences[i].nomPays == paysChoosed &&
          (widget.agences[i].nomVille == villeChoosed) &&
          (!agencesId.contains(widget.agences[i].id))) {
        agencesId.add(widget.agences[i].id);
      }
    }
    return agencesId;
  }

  @override
  Widget build(BuildContext context) {
    var panierState = Provider.of<PanierState>(context, listen: false);
    NewOrderState a = Provider.of<NewOrderState>(context, listen: false);
    List pays = getPaysList();
    List villes = getVillesList();
    List agences = getAgences();
    List agencesId = getAgencesId();
    return ModalProgressHUD(
      inAsyncCall: showSpinner,
      color: AppColors.scaffoldBackgroundYellowForWelcomePage,
      progressIndicator: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.yellowAppbar),
      ),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.orange,
          ),
          title: Text(
            "Livraison Adresse",
            style: TextStyle(
              color: Colors.orange,
            ),
          ),
          centerTitle: true,
        ),
        body: Builder(
          builder: (context) => Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(15, 15, 10, 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => DetailsLivraison(),
                              ),
                            );
                            /* showDialog(
                              context: context,
                              builder: (context) {
                                return SimpleDialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                  ),
                                  children: [
                                    SingleChildScrollView(
                                      padding:
                                          EdgeInsets.fromLTRB(18, 12, 18, 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          ...processDetails,
                                          Center(
                                            child: TextButton(
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                              child: Text(
                                                "OK",
                                                style: TextStyle(
                                                    color: Colors.orange),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ); */
                          },
                          child: Text(
                            "Détails Livraison >",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "TRANSPORT:",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.orange,
                          ),
                        ),
                        SizedBox(height: 5),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              modeChoosed = ModeLivraison.AVION;
                            });
                          },
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.white, width: 2),
                                    color: modeChoosed == ModeLivraison.AVION
                                        ? AppColors.green
                                        : Colors.grey[350]),
                                child: modeChoosed == ModeLivraison.AVION
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      )
                                    : Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                              ),
                              SizedBox(width: 5),
                              Flexible(
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Icon(Icons.flight_outlined),
                                    SizedBox(width: 7.2),
                                    Flexible(
                                      child: Text(
                                        "MODE EXPRESS - AVION",
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: AppColors.indigo,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 12),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              modeChoosed = ModeLivraison.BATEAU;
                            });
                          },
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.white, width: 2),
                                    color: modeChoosed == ModeLivraison.BATEAU
                                        ? AppColors.green
                                        : Colors.grey[350]),
                                child: modeChoosed == ModeLivraison.BATEAU
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      )
                                    : Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                              ),
                              SizedBox(width: 5),
                              Flexible(
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Icon(Icons.directions_ferry_outlined),
                                    SizedBox(width: 7.2),
                                    Flexible(
                                      child: Text(
                                        "MODE BATEAU",
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: AppColors.indigo,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.5),
                    child: Divider(thickness: 5),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "TYPE:",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.orange,
                          ),
                        ),
                        SizedBox(height: 10),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              livraisonChoosed =
                                  AdresseLivraisonType.RecupererLeColisEnAgence;
                            });
                          },
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.white, width: 2),
                                    color: livraisonChoosed ==
                                            AdresseLivraisonType
                                                .RecupererLeColisEnAgence
                                        ? AppColors.green
                                        : Colors.grey[350]),
                                child: livraisonChoosed ==
                                        AdresseLivraisonType
                                            .RecupererLeColisEnAgence
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      )
                                    : Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                              ),
                              SizedBox(width: 5),
                              Flexible(
                                child: Text(
                                  "LIVRAISON À UNE AGENCE",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: AppColors.indigo,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 12),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              livraisonChoosed =
                                  AdresseLivraisonType.LivraisonAdomicile;
                            });
                          },
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.white, width: 2),
                                    color: livraisonChoosed ==
                                            AdresseLivraisonType
                                                .LivraisonAdomicile
                                        ? AppColors.green
                                        : Colors.grey[350]),
                                child: livraisonChoosed ==
                                        AdresseLivraisonType.LivraisonAdomicile
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      )
                                    : Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                              ),
                              SizedBox(width: 5),
                              Flexible(
                                child: Text(
                                  "LIVRAISON À DOMICILE",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: AppColors.indigo,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.5),
                    child: Divider(thickness: 5),
                  ),
                  Visibility(
                    visible: livraisonChoosed ==
                        AdresseLivraisonType.RecupererLeColisEnAgence,
                    child: Padding(
                      padding: EdgeInsets.only(left: 14.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "ADRESSE DE LIVRAISON",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.orange,
                            ),
                          ),
                          SizedBox(height: 14),
                          Text(
                            "PAYS",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: AppColors.indigo,
                              fontSize: 13.0,
                            ),
                          ),
                          DropdownButton<String>(
                            value: paysChoosed,
                            isExpanded: true,
                            onChanged: (String newValue) {
                              setState(() {
                                paysChoosed = newValue;
                                villeChoosed = null;
                                agenceChoosed = null;
                              });
                            },
                            items: [
                              for (var i = 0; i < pays.length; i++)
                                DropdownMenuItem<String>(
                                  value: pays[i],
                                  child: Text(
                                    pays[i],
                                  ),
                                )
                            ],
                          ),
                          SizedBox(height: 14),
                          Text(
                            "VILLE",
                            style: TextStyle(
                              color: AppColors.indigo,
                              fontSize: 13.0,
                            ),
                          ),
                          DropdownButton<String>(
                            value: villeChoosed,
                            isExpanded: true,
                            onChanged: (String newValue) {
                              setState(() {
                                agenceChoosed = null;
                                villeChoosed = newValue;
                              });
                            },
                            items: [
                              for (var i = 0; i < villes.length; i++)
                                DropdownMenuItem<String>(
                                  value: villes[i],
                                  child: Text(
                                    villes[i],
                                  ),
                                )
                            ],
                          ),
                          SizedBox(height: 14),
                          Text(
                            "AGENCES",
                            style: TextStyle(
                              color: AppColors.indigo,
                              fontSize: 13.0,
                            ),
                          ),
                          DropdownButton<String>(
                            value: agenceChoosed,
                            isExpanded: true,
                            onChanged: (String newValue) {
                              setState(() {
                                agenceChoosed = newValue;
                                for (var i = 0; i < agences.length; i++) {
                                  if (agences[i] == newValue) {
                                    agenceChoosedId = agencesId[i];
                                    i = agences.length + 1;
                                  }
                                }
                              });
                            },
                            items: [
                              for (var i = 0; i < agences.length; i++)
                                DropdownMenuItem<String>(
                                  value: agences[i],
                                  child: Text(
                                    agences[i],
                                  ),
                                )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: livraisonChoosed ==
                        AdresseLivraisonType.LivraisonAdomicile,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 14.0),
                          child: Text(
                            "ADRESSE DE LIVRAISON:",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.orange,
                            ),
                          ),
                        ),
                        Center(
                          child: TextButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "CHOISISSEZ UNE ADRESSE",
                                  style: TextStyle(
                                      color: AppColors.indigo, fontSize: 16),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: AppColors.indigo,
                                ),
                              ],
                            ),
                            onPressed: () async {
                              Adresse adressResult = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UserAdresses(
                                          calledByOrder: true,
                                        )),
                              );
                              if (adressResult != null) {
                                setState(() {
                                  paysA = adressResult.pays;
                                  ville = adressResult.ville;
                                  quartier = adressResult.quartier;
                                  adresse1 = adressResult.adresse1;
                                  adresse2 = adressResult.adresse2;
                                });
                              }
                            },
                          ),
                        ),
                        Text(
                          (paysA == null &&
                                  ville == null &&
                                  quartier == null &&
                                  adresse1 == null &&
                                  adresse2 == null)
                              ? ''
                              : ((paysA == "" ? '' : '$paysA, ') +
                                  (ville == "" ? '' : '$ville, ') +
                                  (quartier == "" ? '' : '$quartier, ') +
                                  (adresse1 == "" ? '' : '$adresse1, ') +
                                  (adresse2 == "" ? '' : '$adresse2')),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 17,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 21),
                    child: Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                (modeChoosed == null ||
                                        livraisonChoosed == null)
                                    ? Colors.grey
                                    : AppColors.green,
                              ),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                              )),
                            ),
                            onPressed: (modeChoosed == null ||
                                    livraisonChoosed == null)
                                ? null
                                : () async {
                                    if ((![null, ''].contains(quartier)) ||
                                        (agenceChoosedId != null)) {
                                      List finalResponse = [];
                                      setState(() {
                                        showSpinner = true;
                                      });
                                      if (livraisonChoosed ==
                                              AdresseLivraisonType
                                                  .LivraisonAdomicile &&
                                          (_formKey.currentState.validate())) {
                                        _formKey.currentState.save();
                                        finalResponse = await a.createOrder(
                                          agenceId: null,
                                          context: context,
                                          panierState: panierState,
                                          adresseLivraison: "2",
                                          pays: paysA,
                                          ville: ville,
                                          quartier: quartier,
                                          adresse1: adresse1,
                                          adresse2: adresse2,
                                          modeTransport: (modeChoosed ==
                                                  ModeLivraison.AVION)
                                              ? widget.modesId[0]
                                              : widget.modesId[1],
                                        );
                                      }
                                      if (livraisonChoosed ==
                                          AdresseLivraisonType
                                              .RecupererLeColisEnAgence) {
                                        finalResponse = await a.createOrder(
                                          agenceId: agenceChoosedId,
                                          context: context,
                                          panierState: panierState,
                                          adresseLivraison: "1",
                                          pays: null,
                                          ville: null,
                                          quartier: null,
                                          adresse1: null,
                                          adresse2: null,
                                          modeTransport: (modeChoosed ==
                                                  ModeLivraison.AVION)
                                              ? widget.modesId[0]
                                              : widget.modesId[1],
                                        );
                                      }
                                      setState(() {
                                        showSpinner = false;
                                      });
                                      if (finalResponse[0]) {
                                        setState(() {
                                          ville = "";
                                          quartier = "";
                                          adresse1 = "";
                                          adresse2 = "";
                                        });
                                        showNotification(
                                          body:
                                              "Votre commande a bien été créée. Notre équipe débute sa côtation.",
                                          helper: ("NTG"),
                                        );
                                        Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => OrderDetails(
                                              id: "${finalResponse[1]}"
                                                  .substring(
                                                      0,
                                                      "${finalResponse[1]}"
                                                          .indexOf('/')),
                                            ),
                                          ),
                                        );
                                      } else {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          SnackBar(
                                            backgroundColor: AppColors
                                                .scaffoldBackgroundYellowForWelcomePage,
                                            content: Text(
                                              "Une erreur est survenue",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.black),
                                            ),
                                            behavior: SnackBarBehavior.floating,
                                            margin: EdgeInsets.fromLTRB(
                                                15, 0, 15, 30),
                                          ),
                                        );
                                      }
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          backgroundColor: Colors.black,
                                          content: Text(
                                            "Veuillez choisir une adresse de livraison svp !",
                                            textAlign: TextAlign.center,
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          behavior: SnackBarBehavior.floating,
                                          margin: EdgeInsets.fromLTRB(
                                              15, 0, 15, 30),
                                        ),
                                      );
                                    }
                                  },
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 7),
                              child: Text(
                                "VALIDER",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
