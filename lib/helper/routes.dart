import 'package:flutter/material.dart';
import 'package:tarzanexpress/page/inApp/homePage.dart';
import '../page/common/splash.dart';
import '../page/auth/welcomePage.dart';
import '../page/auth/signin.dart';
import '../page/auth/signup.dart';
import '../helper/customRoute.dart';

class Routes {
  static dynamic route() {
    return {
      '/': (BuildContext context) => SplashPage(),
    };
  }

  static Route onGenerateRoute(RouteSettings settings) {
    final List<String> pathElements = settings.name.split('/');
    if (pathElements[0] != '' || pathElements.length == 1) {
      return null;
    }
    switch (pathElements[1]) {
      case "SignIn":
        return CustomRoute<bool>(builder: (BuildContext context) => SignIn());
      case "SignUp":
        return CustomRoute<bool>(builder: (BuildContext context) => SignUp());
      case "WelcomePage":
        return CustomRoute<bool>(
            builder: (BuildContext context) => WelcomePage());
      default:
        return onUnknownRoute(RouteSettings(name: '/Feature'));
    }
  }

  static Route onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (_) =>
          HomePage() /* Scaffold(
        appBar: AppBar(
          title: Text('${settings.name.split('/')[1]} Comming soon..'),
          centerTitle: true,
        ),
        body: Center(
          child: Text('${settings.name.split('/')[1]} Comming soon..'),
        ),
      ) */
      ,
    );
  }
}
